/////////////////////////////////////////////////////////
//  ajax functions for uploading and retreiving clipart
/////////////////////////////////////////////////////////
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

 // used to load image directly to canvas upon upload
var currentClipartUploadSrc;
$('#clipartFileSelect')[0].onchange = function handleImage(e) {
    // alert('onchange');
  var reader = new FileReader();
  reader.onload = function (event) {
    // alert('onload');
    currentClipartUploadSrc = event.target.result;
  }

  reader.readAsDataURL(e.target.files[0]);
}

var currentBackgroundUploadSrc;
$('#backgroundFileSelect')[0].onchange = function handleImage(e) {
    // alert('onchange');
  var reader = new FileReader();
  reader.onload = function (event) {
    // alert('onload');
    currentBackgroundUploadSrc = event.target.result;
  }

  reader.readAsDataURL(e.target.files[0]);
}

var stickerType = 2;
  if ($('#stickertype').length) {
	stickerType = $('#stickertype').val();
  }

// upload button's onclick set to this function
// will upload clipart, then refresh clipart shown
var test1;
function uploadClipart(event) {
   console.log('upload called');

    event.preventDefault();
	if (currentClipartUploadSrc == null)
	{
		return;
	}

    // addClipart(currentClipartUploadSrc);

    // have to make a new form data, copy original form data into it (file, title)
    // then add some extra info: let the server know we're uploading, and that we're
    // requesting a category of clipart to refresh

    console.log("THIS IS THE STICKERTYPE: " + stickerType);
    console.log("This is the token: " + CSRF_TOKEN);

    var formData = new FormData($('#cilpartForm')[0]);
    formData.append('uploadClipart', 1);
    formData.append('category', $('#category').value);
	  formData.append('stickerType', stickerType);
    $.ajax({
        type: 'post',
        url: 'clipartget',
        beforeSend: function (xhr) {
           var token = $('meta[name="csrf-token"]').attr('content');

           if (token) {
                 return xhr.setRequestHeader('X-CSRF-TOKEN', token);
           }
         },
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        // data: {'uploadClipart': 1, 'category': $('#category').value},
        beforeSend: function() {
			showPopup();
            $("#validation-errors").hide().empty();
			// console.log("sending clipart");
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {

			putMessage(data.split("failed")[1]);
		  }
		  else {
			  // $('#clipartShow').html(data.split("success")[1]);
			  // console.log(data);
			  addClipart(data.split("success")[0], null, true);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            console.error('AJAX error Something went to wrong.Please Try again later...');
            console.error(xhr);
            console.error(textStatus);
            console.error(thrownError);
        }
    });
}

//$('#cilpartForm').submit(uploadClipart(clipartType));
$('#cilpartForm').submit(uploadClipart);


function uploadClipartadmin(event) {
  // alert('upload called');

    event.preventDefault();
	if (currentClipartUploadSrc == null)
	{
		return;
	}

    // addClipart(currentClipartUploadSrc);

    // have to make a new form data, copy original form data into it (file, title)
    // then add some extra info: let the server know we're uploading, and that we're
    // requesting a category of clipart to refresh
    var formData = new FormData($('#admincilpartForm')[0]);
    formData.append('uploadClipart', 1);
    formData.append('category', $('#category').value);
	formData.append('stickerType', stickerType);
    $.ajax({
        type: 'post',
        url: '../clipartget',
        headers: {_token: CSRF_TOKEN},
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        // data: {'uploadClipart': 1, 'category': $('#category').value},
        beforeSend: function() {
			showPopup();
            $("#validation-errors").hide().empty();
			// console.log("sending clipart");
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {

			putMessage(data.split("failed")[1]);
		  }
		  else {
			  // $('#clipartShow').html(data.split("success")[1]);
			  // console.log(data);
			  addClipart(data.split("success")[0], null, true);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            console.error('AJAX error Something went to wrong.Please Try again later...');
            console.error(xhr);
            console.error(textStatus);
            console.error(thrownError);
        }
    });
}

//$('#cilpartForm').submit(uploadClipart(clipartType));
$('#admincilpartForm').submit(uploadClipartadmin);
/////////////////////////////////////////////////////////
//  ajax functions for uploading and retreiving backgrounds
/////////////////////////////////////////////////////////

// upload button's onclick set to this function
// will upload background, then refresh background shown
function uploadBackground(event) {
  // alert('upload called');
    event.preventDefault();
    // have to make a new form data, copy original form data into it (file, title)
    // then add some extra info: let the server know we're uploading, and that we're
    // requesting a category of clipart to refresh
    var formData = new FormData($('#backgroundForm')[0]);
    formData.append('uploadBackground', 1);
	formData.append('stickerType', stickerType);
    $.ajax({
        type: 'post',
        url: 'backgroundget',
        headers: {_token: CSRF_TOKEN},
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        // data: {'uploadClipart': 1, 'category': $('#category').value},
        beforeSend: function() {
			showPopup();
            $("#validation-errors").hide().empty();
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {

			putMessage(data.split("failed")[1]);
		  }
		  else {
			//alert('success' + data);
			// $('#backgroundShow').html(data);
			// addBackground(currentBackgroundUploadSrc);
			addBackground(data.split("success")[0], null, true);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            alert('Something went to wrong.Please Try again later...');
        }
    });
}

$('#backgroundForm').submit(uploadBackground);

function uploadBackgroundadmin(event) {
  // alert('upload called');
    event.preventDefault();
    // have to make a new form data, copy original form data into it (file, title)
    // then add some extra info: let the server know we're uploading, and that we're
    // requesting a category of clipart to refresh
    var formData = new FormData($('#adminbackgroundForm')[0]);
    formData.append('uploadBackground', 1);
	formData.append('stickerType', stickerType);
    $.ajax({
        type: 'post',
        url: '../backgroundget',
        headers: {_token: CSRF_TOKEN},
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        // data: {'uploadClipart': 1, 'category': $('#category').value},
        beforeSend: function() {
			showPopup();
            $("#validation-errors").hide().empty();
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {

			putMessage(data.split("failed")[1]);
		  }
		  else {
			//alert('success' + data);
			// $('#backgroundShow').html(data);
			// addBackground(currentBackgroundUploadSrc);
			addBackground(data.split("success")[0], null, true);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            alert('Something went to wrong.Please Try again later...');
        }
    });
}

$('#adminbackgroundForm').submit(uploadBackgroundadmin);
