function toggleTextBold() {
  var theID = '#text-bold';
  var id = 'text-bold';
  if($(theID).hasClass( "designbtnselected" )) {
    document.getElementById(id).classList.remove("designbtnselected");
  } else {
    document.getElementById(id).classList.add("designbtnselected");
  }
}

function selectTextBold() {
  var theID = '#text-bold';
  var id = 'text-bold';
  if($(theID).hasClass( "designbtnselected" )) {

  } else {
    document.getElementById(id).classList.add("designbtnselected");
  }
}

function deselectTextBold() {
  var theID = '#text-bold';
  var id = 'text-bold';
  if($(theID).hasClass( "designbtnselected" )) {
    document.getElementById(id).classList.remove("designbtnselected");
  } else {

  }
}

function toggleTextItalic() {
  var id = 'text-italic';
  if($('#'+id).hasClass( "designbtnselected" )) {
    document.getElementById(id).classList.remove("designbtnselected");
  } else {
    document.getElementById(id).classList.add("designbtnselected");
  }
}

function toggleTextUnderline() {
  var id = 'text-underline';
  if($('#'+id).hasClass( "designbtnselected" )) {
    document.getElementById(id).classList.remove("designbtnselected");
  } else {
    document.getElementById(id).classList.add("designbtnselected");
  }
}

function toggleTextAlign(align) {
  var leftAlignID = 'text-left-align';
  var centerAlignID = 'text-center-align';
  var rightAlignID = 'text-right-align';
  if(align == 'left') {
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(leftAlignID).classList.add("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(centerAlignID).classList.remove("designbtnselected");
    }
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(rightAlignID).classList.remove("designbtnselected");
    }
  } else if (align == 'center') {
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(leftAlignID).classList.remove("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(centerAlignID).classList.add("designbtnselected");
    }
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(rightAlignID).classList.remove("designbtnselected");
    }
  } else {
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(rightAlignID).classList.add("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(centerAlignID).classList.remove("designbtnselected");
    }
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(leftAlignID).classList.remove("designbtnselected");
    }
  }
}

function updateTextProp() {
  var alignment = $('#text-input').css('text-align');
  var weight = $('#text-input').css('font-weight');
  var style = $('#text-input').css('font-style');
  var decoration = $('#text-input').css('text-decoration');
  var leftAlignID = 'text-left-align';
  var centerAlignID = 'text-center-align';
  var rightAlignID = 'text-right-align';
  var boldID = 'text-bold';
  var italicID = 'text-italic';
  var underlineID = 'text-underline';

  var string = decoration;
  string = string.split(" ");
  if(string.length > 1) {
    decoration = string[0];
  }

  if(alignment == 'left') {
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(leftAlignID).classList.add("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(centerAlignID).classList.remove("designbtnselected");
    }
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(rightAlignID).classList.remove("designbtnselected");
    }
  } else if (alignment == 'center' ) {
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(leftAlignID).classList.remove("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(centerAlignID).classList.add("designbtnselected");
    }
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(rightAlignID).classList.remove("designbtnselected");
    }
  } else {
    if($('#'+rightAlignID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(rightAlignID).classList.add("designbtnselected");
    }
    if($('#'+centerAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(centerAlignID).classList.remove("designbtnselected");
    }
    if($('#'+leftAlignID).hasClass( "designbtnselected" )) {
      document.getElementById(leftAlignID).classList.remove("designbtnselected");
    }
  }

  if(weight == '700' || weight == 'bold') { // add bold
    if($('#'+boldID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(boldID).classList.add("designbtnselected");
    }
  } else { // remove bold
    if($('#'+boldID).hasClass( "designbtnselected" )) {
      document.getElementById(boldID).classList.remove("designbtnselected");
    }
  }

  if(style == 'italic') {
    if($('#'+italicID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(italicID).classList.add("designbtnselected");
    }
  } else {
    if($('#'+italicID).hasClass( "designbtnselected" )) {
      document.getElementById(italicID).classList.remove("designbtnselected");
    }
  }

  if(decoration == 'underline') {
    if($('#'+underlineID).hasClass( "designbtnselected" )) {

    } else {
      document.getElementById(underlineID).classList.add("designbtnselected");
    }
  } else {
    if($('#'+underlineID).hasClass( "designbtnselected" )) {
      document.getElementById(underlineID).classList.remove("designbtnselected");
    }
  }
}

function updateFontSelector(theFont){
  var index;
  if (theFont == 'boogaloottf') {
    $('#boogaloottf').trigger('click');
  } else if (theFont == 'crimsontextttf') {
    $('#crimsontextttf').trigger('click');
  } else if (theFont == 'englandttf') {
    $('#englandttf').trigger('click');
  } else if (theFont == 'felipattf') {
    $('#felipattf').trigger('click');
  } else if (theFont == 'gravitasonettf') {
    $('#gravitasonettf').trigger('click');
  } else if (theFont == 'greatvibesttf') {
    $('#greatvibesttf').trigger('click');
  } else if (theFont == 'hammersmithonettf') {
    $('#hammersmithonettf').trigger('click');
  } else if (theFont == 'hennepennyttf') {
    $('#hennepennyttf').trigger('click');
  } else if (theFont == 'kaushanscriptttf') {
    $('#kaushanscriptttf').trigger('click');
  } else if (theFont == 'leaguegothicttf') {
    $('#leaguegothicttf').trigger('click');
  } else if (theFont == 'limelightttf') {
    $('#limelightttf').trigger('click');
  } else if (theFont == 'lobstertwottf') {
    $('#lobstertwottf').trigger('click');
  } else if (theFont == 'maidenoragettf') {
    $('#maidenoragettf').trigger('click');
  } else if (theFont == 'nunitottf') {
    $('#nunitottf').trigger('click');
  } else if (theFont == 'robotottf') {
    $('#robotottf').trigger('click');
  } else {
    $('#robotocondensedttf').trigger('click');
  }

}

function updateFontSelect() {
  var theFonty = document.getElementById('newFontSelector').value;
  $('#newFontSelector').css('font-family', theFonty);
}
