/*
 * Function
 */
function updateTmpBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var textBox = $(".tmpBox");
	textBox.css("width", canvasWidth);
	textBox.css("height", canvasHeight);
	textBox.offset({top: canvasOffset.top+20, left:20});
}

function tmpBoxHide() {
	$('.tmpBox').css("display", "none");
}

function tmpBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.tmpBox').css("display", "inherit");
	$('.can-row').css("background-color", "#ffffff");
	canvas.overlayImage.opacity = 0;
	updateTmpBox();
}

function tmpBoxToggle() {
	canvas.deactivateAll().renderAll();
	document.getElementById('text-input').value = "";
  if(document.getElementById('tmpBox').style.display == "none") {
    tmpBoxShow();
  } else {
    tmpBoxHide();
  }
}

$(document).ready(function() {
	updateTmpBox();
  tmpBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateTmpBox();
}
