var server_path = 'http://markmaster.app/';
//var server_path = 'https://www.markmaster.com/';

var objectArray = new Array();

function putMessage(message, timeout)
{
	// if ($('#messageSpan').children().length == 1)
	var msgSpan = $('#messageSpan');
	msgSpan.append('<p class="alert">'+message+'</p>');
	msg = msgSpan.children().last();
	msg.hide();
	msg.slideDown();
	setTimeout(function(){msg.slideUp();}, 20000);
}


function showPopup() {
    // show the mask
    $("#mask").fadeTo(500, 0.25);

    // show the popup
    $("#popup").show();
}

function hidePopup() {
    // show the mask
    $("#mask").hide();

    // show the popup
    $("#popup").hide();
}

/////////////////////////////////////////////////////////
//  initialize stuff
/////////////////////////////////////////////////////////

// initialize spin
var opts = {
  lines: 13, // The number of lines to draw
  length: 8, // The length of each line
  width: 3, // The line thickness
  radius: 8, // The radius of the inner circle
  corners: 1, // Corner roundness (0..1)
  rotate: 0, // The rotation offset
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: '#000', // #rgb or #rrggbb or array of colors
  speed: 1.2, // Rounds per second
  trail: 60, // Afterglow percentage
  shadow: false, // Whether to render a shadow
  hwaccel: false, // Whether to use hardware acceleration
  className: 'spinner', // The CSS class to assign to the spinner
  zIndex: 2e9, // The z-index (defaults to 2000000000)
  top: '50%', // Top position relative to parent
  left: '50%' // Left position relative to parent
};
var target = document.getElementById('popupSpin');
var spinner = new Spinner(opts).spin(target);

// initialize tabulous
// $(document).ready(function ($) {
  // $('#tabs').tabulous({
    // effect: 'slideLeft'
  // });
// });


// $(function() {
//   $("#tabs").tabs();
// });

 /*
// initialize colorpickers
$('select[name="colorpickerRaise"]')
.simplecolorpicker({
    picker: true
  });

$('select[name="backgroundColorPicker"]').simplecolorpicker({
    picker: false
  });

$('select[name="colorpickerStroke"]')
.simplecolorpicker({
    picker: true
  });

$('select[name="colorpickerClipart"]')
.colorpicker({
        size: 20,
        label: 'Color: ',
        hide: false
    });

// set's background color of sticker when color chosen
// $('select[name="backgroundColorPicker"]').on('change', function() {
  // setBackgroundColor($(this).val());
// });
//*/

 //*
// initialize colorpickers
$('select[name="colorpickerRaise"]')
.simplecolorpicker({
    picker: true
  });

$('select[name="backgroundColorPicker"]').simplecolorpicker({
    picker: false
  });

	$('select[name="backgroundColorPickerSelect"]').simplecolorpicker({
	    picker: false
	  });

$('select[name="colorpickerStroke"]')
.simplecolorpicker({
    picker: true
  });

$('select[name="colorpickerClipart"]')
.simplecolorpicker({
    picker: true
  });

	// set's background color of sticker when color chosen
	 $('select[name="backgroundColorPickerSelect"]').on('change', function() {
	   setBackgroundColor($(this).val());
	 });
	//*/

	$('#backgroundColorPicker').on('change', function() {
	  setBackgroundColor($(this).val());
	});


$('#stickerText').css('font-family', 'boogaloottf');
/*
// initialize colorpickers
$('#fill')
.spectrum({
    showPaletteOnly: true,
	showPalette: true,
	color: '#000',
	palette: [
		['#000000','#ffffff','#E6E6E6','#B1B1B1','#888887','#5C5C5B','#C8112E','#B01D2B','#871630','#EBB5C3'],
		['#E64A00','#E9954A','#E6D5A8','#EFE032','#EFED84','#EAEBBC','#0F2867','#1A35A8','#8ACBE5','#C9D8E7'],
		['#549AA3','#4A7229','#61AE56','#B6DD8E','#A4D426','#9E520F','#BC8F70','#B50970','#E8A3D0','#680E92'],
		['#EACDCF','#9E70C1','#D7CAE3','#cfb53b','#d7d8d8','#000000']
	]
});
 //*/


// updates what background is shown
// whenver a new category for background is selected, it requests all background in that category
// from the server and then puts it in the #clipartShow  box
$(document).ready(function() {
        $('#backCategory').change(function() {

			  var clipartType = 2;
			  if ($('#clipartType').length) {
				clipartType = $('#clipartType').val();
			  }


            $.ajax({
                type: 'get',
                url: 'backgroundget',
                cache: false,
                dataType: 'html',
                data: {'getBackground': 1, 'category': this.value, 'stickerType': clipartType},
                beforeSend: function() {
                    $("#validation-errors").hide().empty();
                },
                success: function(data) {
                  //alert('success' + data);
                  $('#backgroundShow').html(data);
                    // if(data.success == false)
                    // {
                    //     var arr = data.errors;
                    //     $.each(arr, function(index, value)
                    //     {
                    //         if (value.length != 0)
                    //         {
                    //             $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                    //         }
                    //     });
                    //     $("#validation-errors").show();
                    // } else {
                    //      location.reload();
                    // }
                },
                error: function(xhr, textStatus, thrownError) {
                    alert('Something went to wrong.Please Try again later...');
                    // console.log('xhr: ' + xhr);
                    // alert('textStatus: ' + textStatus);
                    // alert('thrownError: ' + thrownError);
                }
            });
    });
});


// updates what clipart is shown
// whenver a new category for clipart is selected, it requests all clipart in that category
// from the server and then puts it in the #clipartShow  box


 function normalizeAngle(angle)
 {
  while(angle > 360)
    angle-=360;
  while(angle < 0)
    angle+= 360;
  return angle;
 }

resizeOutline();
updateOutlinePath();
canvas.renderAll(); // added
setPrice();

//this may require an extra script tag instead of being included using require
//require is a part of Node and not common JS
//require('outline');
//require('price');

function makeRect() {
  // used to select radio button for shape when image is clicked
  document.getElementById('isEllipse').checked = false;
  document.getElementById('isRect').checked = true;
	document.getElementById('isSmallCutline').checked = false;
  document.getElementById('isMediumCutline').checked = false;
	document.getElementById('isLargeCutline').checked = false;
  currentOutline = rectOutlineInfo;
  resizeOutline();
  updateOutlinePath();
  canvas.renderAll();
}

function makeElipse() {
  document.getElementById('isEllipse').checked = true;
  document.getElementById('isRect').checked = false;
	document.getElementById('isSmallCutline').checked = false;
  document.getElementById('isMediumCutline').checked = false;
	document.getElementById('isLargeCutline').checked = false;
  currentOutline = ellipseOutlineInfo;
  resizeOutline();
  updateOutlinePath();
  canvas.renderAll();
}

function makeSmallOutline() {
	showPopup();
	Outliner.options.thickness = 8;
	Outliner.options.textThickness = 20;
  document.getElementById('isEllipse').checked = false;
  document.getElementById('isRect').checked = false;
	document.getElementById('isSmallCutline').checked = true;
  document.getElementById('isMediumCutline').checked = false;
	document.getElementById('isLargeCutline').checked = false;
  Outliner.generateCutline();
}
function makeMediumOutline() {
	showPopup();
	Outliner.options.thickness = 16;
	Outliner.options.textThickness = 40;
  document.getElementById('isEllipse').checked = false;
  document.getElementById('isRect').checked = false;
	document.getElementById('isSmallCutline').checked = false;
  document.getElementById('isMediumCutline').checked = true;
	document.getElementById('isLargeCutline').checked = false;
  Outliner.generateCutline();
}
function makeLargeOutline() {
	showPopup();
	Outliner.options.thickness = 24;
	Outliner.options.textThickness = 60;
  document.getElementById('isEllipse').checked = false;
  document.getElementById('isRect').checked = false;
	document.getElementById('isSmallCutline').checked = false;
  document.getElementById('isMediumCutline').checked = false;
	document.getElementById('isLargeCutline').checked = true;
  Outliner.generateCutline();
}

// function onLineHeightSliderChange() {
  // var lineHeightSlider = $('#lineHeightSlider');
  // $('#lineHeightTextbox').val(lineHeightSlider.val());

  // var currentObj = canvas.getActiveObject();
  // if (!isText(currentObj))
    // return;

  // currentObj.lineHeight = lineHeightSlider.val();
  // canvas.renderAll();
// }

// $('#lineHeightSlider').on('input', onLineHeightSliderChange);
// $('#lineHeightSlider').on('change', onLineHeightSliderChange);

canvas.on('selection:cleared', function(e) {
	shapeBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	clipartBoxHide();
	editBoxHide();
});

//////////////////////////////////////////////////////////////////////////////////////
//  Code to change tab to the corresponding activeobject                            //
//////////////////////////////////////////////////////////////////////////////////////
canvas.on('object:selected', function(e) {
    var object = e.target;

    if(isText(object)) {
			if(document.getElementById('textBox').style.display == "none") {
				editBoxShow();
			}
			$("#text-input").val(canvas.getActiveObject().getText());

	  		$("#text-input").on("keyup", function(e) {
				if(canvas.getActiveObject()) {
					canvas.getActiveObject().set("text", this.value);
					canvas.renderAll();
				}
	  		});

				$('#fontFamily').val(object.fontFamily);
 			 $('#textAngleTextbox').val(object.angle);
 			 $('#fontSizeSlider').val(object.scaleX);
 			 $('#fontSizeTextbox').val(object.scaleX);
 			 $('#lineHeightSlider').val(object.lineHeight);
 			 $('#lineHeightTextbox').val(object.lineHeight);
 			 $('#fill').val(object.fill);
 			 $('#fillcolor').css('background-color', object.fill);
 			 $('#stroke').val(object.stroke);
 			 $('#strokecolor').css('background-color', object.stroke);
 			 $('#strokeWidth').val(object.strokeWidth);
 			 $('#strokeWidthTextbox').val(object.strokeWidth);
 			 $('#text-input').css('font-family', object.fontFamily);
 			 $('#text-input').css('font-weight', object.fontWeight);
 			 $('#text-input').css('font-style', object.fontStyle);
 			 $('#text-input').css('text-align', object.textAlign);
 			 $('#text-input').css('text-decoration', object.textDecoration);
 			 updateTextProp();
 			 updateFontSelector(object.fontFamily);
      object.angle = normalizeAngle(object.angle);
          //$('#textAngleSlider').val(object.angle);
          //onTextAngleSliderChange();

          //MAT: if this is the curved text, set the sliders to match
          if(/curvedText/.test(object.type))
          {
              $('.radius').val(object.get('radius'));
              $('.spacing').val(object.get('spacing'));
          }

    } else if (isSVG(object)){
			editBoxShow();
			$('#clipartScaleTextbox').val(object.scaleX/object.scaleXStart);
			if(object.fill == '') {
				$('#clipcolor').val('#000000');
				$('#clipcolorr').css('background-color', '#000000');
			} else {
				$('#clipcolor').val(object.fill);
				$('#clipcolorr').css('background-color', object.fill);
			}
		} else if (isClipart(object)) {
				editBoxShow();
          // $('#clipartScaleSlider').val(object.scaleX/object.scaleXStart);
          $('#clipartScaleTextbox').val(object.scaleX/object.scaleXStart);


      object.angle = normalizeAngle(object.angle);
			  $('#clipartAngleTextbox').val(object.angle);
          // $('#clipartAngleSlider').val(object.angle);
          //onClipartAngleSliderChange();
    } else if (isGroup(object)) {
			object.setControlsVisibility({
		        mr: false,
		        ml: false,
		        mt: false,
		        mb: false,
		        tl: false,
		        bl: false,
						tr: false
		  });
		object.lockScalingX = false;
		object.lockScalingY = false;
        object.setCoords();
	}
});

//////////////////////////////////////////////////////////////////////////////////////
//  Function to delete the current active object                                    //
//////////////////////////////////////////////////////////////////////////////////////
function deleteActive() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (currentObj != null)
  {
    currentObj.remove();
  }
  else if (isGroup(currentGroup))
  {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].remove();
    }
    canvas.discardActiveGroup();
  }
  //orderLayers();
  canvas.renderAll();
}

//////////////////////////////////////////////////////////////////////////////////////
//  Code for right-click activeObject menu                                          //
//////////////////////////////////////////////////////////////////////////////////////
var textMenu  = [
  {'Delete': function(menuItem,menu) { deleteActive(); } },
  $.contextMenu.separator,
  {'Move Up': function(menuItem,menu) { moveUp(); } },
  {'Move Down': function(menuItem,menu) { moveDown(); } },
  {'Bring to Front': function(menuItem,menu) { bringToFront();} },
  {'Send to Back': function(menuItem,menu) { sendToBack(); } },
  $.contextMenu.separator,
  {'Move to Center': function(menuItem,menu) { centerObject(); }},
  {'Move to Vertical Center': function(menuItem,menu) { centerObjectV(); }},
  {'Move to Horizontal Center': function(menuItem,menu) { centerObjectH(); }},
  $.contextMenu.separator,
  {'Flip Vertical': function(menuItem,menu) { flipXImg(); }},
  {'Flip Horizontal': function(menuItem,menu) { flipYImg(); }},
  $.contextMenu.separator,
  {'Rotate 90 degrees' : function(menuItem,menu) { rotateObject(); }}

];


$('#canvasDiv').contextMenu(textMenu,{theme:'vista'});


//////////////////////////////////////////////////////////////////////////////////////
//  Function for left arrow key controlls                                           //
//////////////////////////////////////////////////////////////////////////////////////
function leftArrowPressed() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (currentObj != null)
  {
    currentObj.left -= 1;
  }
  else if (isGroup(currentGroup))
  {
    currentGroup.left -= 1;
  }
  //orderLayers();
  canvas.renderAll();

};

//////////////////////////////////////////////////////////////////////////////////////
//  Function for right arrow key controlls                                          //
//////////////////////////////////////////////////////////////////////////////////////
function rightArrowPressed() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (currentObj != null)
  {
    currentObj.left += 1;
  }
  else if (isGroup(currentGroup))
  {
    currentGroup.left += 1;
  }
  //orderLayers();
  canvas.renderAll();

};

//////////////////////////////////////////////////////////////////////////////////////
//  Function for up arrow key controlls                                             //
//////////////////////////////////////////////////////////////////////////////////////
function upArrowPressed() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (currentObj != null)
  {
    currentObj.top -= 1;
  }
  else if (isGroup(currentGroup))
  {
    currentGroup.top -= 1;
  }
  //orderLayers();
  canvas.renderAll();

};

//////////////////////////////////////////////////////////////////////////////////////
//  Function for left down key controlls                                            //
//////////////////////////////////////////////////////////////////////////////////////
function downArrowPressed() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (currentObj != null)
  {
    currentObj.top += 1;
  }
  else if (isGroup(currentGroup))
  {
    currentGroup.top += 1;
  }
  //orderLayers();
  canvas.renderAll();
};

//////////////////////////////////////////////////////////////////////////////////////
//  Function to prevent window scrolling when keyboard buttons are pushed           //
//////////////////////////////////////////////////////////////////////////////////////
function preventDefault(e) {
  e = e || window.event;
  if (e.preventDefault)
      e.preventDefault();
  e.returnValue = false;
}

//////////////////////////////////////////////////////////////////////////////////////
//  Function for - button pressed                                                   //
//////////////////////////////////////////////////////////////////////////////////////
function negativeKeypadPressed() {
  // change scale or font size

  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();

  if(!isGroup(currentGroup)) // it's not a group.. do changes
  {
    if(isText(currentObj))
    {
      if (currentObj.scaleX > minFontScale)
      {
        currentObj.scaleX -= 0.1;
		 currentObj.scaleY -= 0.1;
        currentObj.setCoords();
        // document.getElementById('fontSizeSlider').value = currentObj.fontSize;
        document.getElementById('fontSizeTextbox').value = currentObj.scaleX;
        if(currentObj.scaleX < minFontScale)
        {
          currentObj.scaleX = minFontScale;
		  currentObj.scaleY = minFontScale;
          currentObj.setCoords();
          // document.getElementById('fontSizeSlider').value = currentObj.fontSize;
          document.getElementById('fontSizeTextbox').value = currentObj.scaleX;
        }
      }
    } else if (isClipart(currentObj))
    {
      if (currentObj.scaleX > 0)
      {
        currentObj.scaleX -= 0.02;
        currentObj.scaleY -= 0.02;
        currentObj.setCoords();
        // document.getElementById('clipartScaleSlider').value = currentObj.scaleX/currentObj.scaleXStart;
        document.getElementById('clipartScaleTextbox').value = currentObj.scaleX/currentObj.scaleXStart;
        if(currentObj.scaleX < 0.0001)
        {
          currentObj.scaleX = 0.0001;
          currentObj.scaleY = 0.0001;
          currentObj.setCoords();
		  // document.getElementById('clipartScaleSlider').value = currentObj.scaleX/currentObj.scaleXStart;
		  document.getElementById('clipartScaleTextbox').value = currentObj.scaleX/currentObj.scaleXStart;
        }
      }

    }
  }
  //orderLayers();
  canvas.renderAll();

};

//////////////////////////////////////////////////////////////////////////////////////
//  Function for - button pressed                                                   //
//////////////////////////////////////////////////////////////////////////////////////
function positiveKeypadPressed() {
  // change scale or font size

  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();

  if(!isGroup(currentGroup)) // it's not a group.. do changes
  {
    if(isText(currentObj))
    {

        currentObj.scaleX += 0.1;
		currentObj.scaleY += 0.1;
        currentObj.setCoords();
        // document.getElementById('fontSizeSlider').value = currentObj.fontSize;
        document.getElementById('fontSizeTextbox').value = currentObj.scaleX;
      }
    } else if (isClipart(currentObj))
    {
      if (currentObj.scaleX < 5)
      {
        currentObj.scaleX += 0.02;
        currentObj.scaleY += 0.02;
        currentObj.setCoords();
        // document.getElementById('clipartScaleSlider').value = currentObj.scaleX/currentObj.scaleXStart;
        document.getElementById('clipartScaleTextbox').value = currentObj.scaleX/currentObj.scaleXStart;
        if(currentObj.scaleX > 5)
        {
          currentObj.scaleX = 5;
          currentObj.scaleY = 5;
          currentObj.setCoords();
		  // document.getElementById('clipartScaleSlider').value = currentObj.scaleX/currentObj.scaleXStart;
        document.getElementById('clipartScaleTextbox').value = currentObj.scaleX/currentObj.scaleXStart;
        }
      }
    }

  //orderLayers();
  canvas.renderAll();

};

//////////////////////////////////////////////////////////////////////////////////////
//  Function for controlling active canvas objects with keyboard buttons            //
//////////////////////////////////////////////////////////////////////////////////////
document.onkeydown = function(evt) {
  evt = evt || window.event;
  switch (evt.keyCode) {
  case 37:
    preventDefault(evt);
    leftArrowPressed();
    break;
  case 39:
    preventDefault(evt);
    rightArrowPressed();
    break;
  case 38:
    preventDefault(evt);
    upArrowPressed();
    break;
  case 40:
    preventDefault(evt);
    downArrowPressed();
    break;
  case 46:
    preventDefault(evt);
    deleteActive();
  case 107:
    preventDefault(evt);
    positiveKeypadPressed();
    break;
  case 109:
    preventDefault(evt);
    negativeKeypadPressed();
    break;
  }
};

function setBorderColor() {
  var obj = canvas.getObjects();
  var arrayLength = obj.length;
  for (var i = 0; i < arrayLength; i++) {
    applyClipartSettings(obj[i]);
    if(outlineOptions.fill == 'black' || outlineOptions.fill == '#000000')
    {
      obj[i].set({
        borderColor: 'red',
        cornerColor: 'red'
      });
    } else
    {
      obj[i].set({
        borderColor: 'black',
        cornerColor: 'black'
      });
    }
  }
  canvas.renderAll();
}

$('#fontList li').click(
    function(){
        var chosen = $(this).index();
        $('#fontFamily option:selected')
            .removeAttr('selected');
        $('#fontFamily option')
            .eq(chosen)
            .attr('selected',true);
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
		$('#text-input').css('font-family', $(this).css('font-family'));
      var currentObj = canvas.getActiveObject();
      if (isText(currentObj))
      {
      currentObj.fontFamily = $(this).css('font-family');
      canvas.renderAll();
      }
    });

		function jsonify() {
		   var canvasJSON = canvas.toJSON(['selectable']);
			 var shape = 'rect';
			 var outlineInfo;
			  if (currentOutline == rectOutlineInfo)
			  {
			    shape = 'rect';
			    outlineInfo = {
			      fill: outlineOptions.fill,
			      shape: 'rect',
			      width: rectOutlineInfo.width,
			      height: rectOutlineInfo.height,
			      left: rectOutlineInfo.left,
			      top: rectOutlineInfo.top,
			      rx: rectOutlineInfo.rx,
			      ry: rectOutlineInfo.ry
			    }
			  }
			  else if (currentOutline == ellipseOutlineInfo)
			  {
			    shape = 'ellipse';
			    outlineInfo = {
			      fill: outlineOptions.fill,
			      shape: 'ellipse',
			      left: ellipseOutlineInfo.left,
			      top: ellipseOutlineInfo.top,
			      rx: ellipseOutlineInfo.rx,
			      ry: ellipseOutlineInfo.ry
			    }
			  }
			  var width = $('#width').val();
			  var height = $('#height').val();
			  currentOutline.fill = outlineOptions.fill;
			  var jsonData = {
			    canvas: canvasJSON,
			    shape: shape,
			    width: width,
			    height: height,
			    outlineInfo: outlineInfo,
			    backImgInfo: backImgInfo
			  };
			  var dataToSend = JSON.stringify(jsonData);
			  var width = document.getElementById('width').value;
			  var height = document.getElementById('height').value;
			  $('#json').val(dataToSend);
			  $('#stickerjson').val(dataToSend);
				$('#postjson').val(dataToSend);
			  $('#stickerwidth').val(width);
			  $('#stickerheight').val(height);
		}

function rdycheckout(value){
  jsonify();
  setPrice();
  $('#stickertype').val(value);
}

function loadfromJson(jsonstring){

  jsonstring = JSON.stringify(jsonstring);

  if (jsonstring == 'missmatch')
  {
	return;
  }

  jsonstring = jsonstring.replace(/(['"])src(['"]):(['"])http:\/\/.*?\//, '$1src$2:$3' + server_path);
  jsonstring = jsonstring.replace(/\n/g, "\\n");
  var json = JSON.parse(jsonstring);
  // console.log(json);
  var objectsToAdd = [];
  var numReplaced = 0;
  $('#width').val(json.width);
  $('#height').val(json.height);

  //*

  canvas.loadFromJSON(json.canvas, function(){

	outlineOptions.fill = json.outlineInfo.fill;
	$('#backgroundColorPicker').val(canvas.backgroundImage.fill);
	$('#backgrcolor').css('background-color', canvas.backgroundImage.fill);
	var outlinePath;
	resizeOutline();
	if (json.shape == 'rect')
	{
		document.getElementById('isEllipse').checked = false;
		document.getElementById('isRect').checked = true;
		document.getElementById('isSmallCutline').checked = false;
		document.getElementById('isMediumCutline').checked = false;
		document.getElementById('isLargeCutline').checked = false;
		currentOutline = rectOutlineInfo;
		outlinePath = getRectClipPath();
		var overlay = outlinePath.cloneAsImage();
		canvas.setOverlayImage(overlay);
		canvas.renderAll();
	}
	else if (json.shape == 'ellipse')
	{
		document.getElementById('isEllipse').checked = true;
		document.getElementById('isRect').checked = false;
		document.getElementById('isSmallCutline').checked = false;
		document.getElementById('isMediumCutline').checked = false;
		document.getElementById('isLargeCutline').checked = false;
		currentOutline = ellipseOutlineInfo;
		outlinePath = getEllipseClipPath();
		var overlay = outlinePath.cloneAsImage();
		canvas.setOverlayImage(overlay);
		canvas.renderAll();
	} else if(json.shape == 'outline') {
			currentOutline = customOutlineInfo;
			Outliner.initialize(json);
			outlineInfo = json.outlineInfo;
			
			if(json.size == 'smallOutline') {
				//makeSmallOutline();
				document.getElementById('isEllipse').checked = false;
			  document.getElementById('isRect').checked = false;
				document.getElementById('isSmallCutline').checked = true;
			  document.getElementById('isMediumCutline').checked = false;
				document.getElementById('isLargeCutline').checked = false;
			} else if(json.size == 'mediumOutline') {
				//makeMediumOutline();
				document.getElementById('isEllipse').checked = false;
			  document.getElementById('isRect').checked = false;
				document.getElementById('isSmallCutline').checked = false;
			  document.getElementById('isMediumCutline').checked = true;
				document.getElementById('isLargeCutline').checked = false;
			} else {
				//makeLargeOutline();
				document.getElementById('isEllipse').checked = false;
			  document.getElementById('isRect').checked = false;
				document.getElementById('isSmallCutline').checked = false;
			  document.getElementById('isMediumCutline').checked = false;
				document.getElementById('isLargeCutline').checked = true;
			}
	}

	if (json.backImgInfo.beingUsed)
	{
		var backImg = new Image();
		backImg.src = json.backImgInfo.src;

		backImgInfo.isLocal = false;
		addBackgroundFromImgElement(backImg);
	}

	canvas.renderAll.bind(canvas);
  },
  function(o, object){
	if (o.type == 'image')
	{
		// object.src = o.src.replace(/http:\/\/.*?\//, 'http://79.161.166.153/');
		applyClipartSettings(object);
	}
	else if (o.type == 'path' || o.type == 'path-group')
	{
		object.isSVG = true;
		applyClipartSettings(object);
	}
	else if (o.type == 'text')
	{
		applyTextSettings(object);
	}
	else if (o.type == 'i-text')
	{
		applyTextSettings(object);
	}
  });

  setBorderColor();
  setPrice();

  canvas.calcOffset();
  canvas.renderAll();
}


document.onclick = stayActive; // Update to keep the user from going idle
function stayActive() {
	$.ajax({
	  url: server_path + 'no/merkelapper/stayactive',
	  data: null,
	  success: function($data){},
	  dataType: 'html'
	});
}

//crop
/*
var mousePos = [0, 0];

var r = document.getElementById('myCanvas').getBoundingClientRect();
mousePos[0] = r.left;
mousePos[1] = r.top;

var mousex = 0;
var mousey = 0;
var crop = false;
var cropDisabled = true;

//console.log(mousePos);

var cropSelect = new fabric.Rect({
    //left: 100,
    //top: 100,
    fill: 'transparent',
    originX: 'left',
    originY: 'top',
    stroke: '#ccc',
    strokeDashArray: [2, 2],
    opacity: 1,
    width: 1,
    height: 1
  });

cropSelect.visible = false;
canvas.add(cropSelect);

canvas.on("mouse:down", function (event) {
  var currentImg = canvas.getActiveObject();
  if (!isImg(currentImg))
  {
    //alert('mouse down');
    currentImg = null;
    return;
  }
  if (cropDisabled) return;
  cropSelect.left = event.e.pageX - mousePos[0];
  cropSelect.top = event.e.pageY - mousePos[1];
    //cropSelect.selectable = false;
    cropSelect.visible = true;
    mousex = event.e.pageX;
    mousey = event.e.pageY;
    crop = true;
    canvas.bringToFront(cropSelect);
  });

canvas.on("mouse:move", function (event) {
    //console.log(event);
    if (crop && !cropDisabled) {
      cropSelect.width = event.e.pageX - mousex;

      cropSelect.height = event.e.pageY - mousey;
    }
  });

canvas.on("mouse:up", function (event) {
  crop = false;
});

$('#startCropB').on('click', function (event) {
  var currentImg = canvas.getActiveObject();
  if (isImg(currentImg))
  {
    $(this).attr('value', 'cropEnabled');
    cropDisabled = false;
    $('#finishCropB').attr('disabled', false);
    currentImg.selectable = false;
    currentImg.clipTo = null;
    canvas.renderAll();
  }
});


// when the finish crop button is clicked
// apply the crop to the currently selected image
$('#finishCropB').on('click', function (event) {

    // disable finish crop button
    this.disabled = true;

    // change start crop buttons value to 'cropDisabled' to allow for styling
    $('#startCropB').attr('value', 'cropDisabled');


    // if currentImg is null or isn't an image, end the function early
    var currentImg = canvas.getActiveObject();
    if (!isImg(currentImg))
    {
      currentImg = null;
      return;
    }

    var scaleX = currentImg.getScaleX();
    var scaleY = currentImg.getScaleY();

    // var left = cropSelect.left - currentImg.left;// + currentImg.width*currentImg.scaleX/2;
    // var top = cropSelect.top - currentImg.top;// + currentImg.height*currentImg.scaleY/2;

    // left *= 1 / scaleX;
    // top *= 1 / scaleY;

    // var width = cropSelect.width * 1 / scaleX;
    // var height = cropSelect.height * 1 / scaleY;



    var left = cropSelect.left - currentImg.left;// + currentImg.width*currentImg.scaleX/2;
    var top = cropSelect.top - currentImg.top;// + currentImg.height*currentImg.scaleY/2;

    var width = cropSelect.width;
    var height = cropSelect.height;


    // accounts for negative width and height of the crop selection
    if (width < 0)
    {
      left += width;
      width *= -1;
    }
    if (height < 0)
    {
      top += height;
      height *= -1;
    }

    // takes care of flipX and flipY issues
    var rotFlip = 1;
    if (currentImg.flipX)
    {
      left = -(left+width);
      rotFlip *= -1;
    }
    if (currentImg.flipY)
    {
      top = -(top+height);
      rotFlip *= -1;
    }

    var angle = rotFlip * degToRad(currentImg.getAngle());

    currentImg.clipTo = function (ctx) {
      ctx.originX = 'center';
      ctx.originY = 'center';

        // reverse scales and rotations
        ctx.scale(1/scaleX, 1/scaleY);
        ctx.rotate(-angle);

        // clip rect
        ctx.rect(left, top, width, height);

        // apply scales and rotations again
        ctx.rotate(angle);
        ctx.scale(scaleX, scaleY);
      };
      currentImg.selectable = true;
      cropDisabled = true;
      cropSelect.visible = false;
      currentImg.setCoords();
      canvas.renderAll();
    });

function degToRad(degrees) {
  return degrees * (Math.PI / 180);
}
//END CROP
//*/

  // $("#uploadControlsOpen").click(function() {
  //   // $("#uploadControls").toggle();
  //   $("#uploadControls").slideToggle( "slow", function() {
  //     // Animation complete.
  //   });
  // });


// })();



function submitSticker()
{
	var canvasJSON = canvas.toJSON();
	var shape = 'rect';
	var outlineInfo;
	if (currentOutline == rectOutlineInfo)
	{
		shape = 'rect';
		outlineInfo = {
			fill: outlineOptions.fill,
			shape: 'rect',
			width: rectOutlineInfo.width,
			height: rectOutlineInfo.height,
			left: rectOutlineInfo.left,
			top: rectOutlineInfo.top,
			rx: rectOutlineInfo.rx,
			ry: rectOutlineInfo.ry
		}
	}
	else if (currentOutline == ellipseOutlineInfo)
	{
		shape = 'ellipse';
		outlineInfo = {
			fill: outlineOptions.fill,
			shape: 'ellipse',
			left: ellipseOutlineInfo.left,
			top: ellipseOutlineInfo.top,
			rx: ellipseOutlineInfo.rx,
			ry: ellipseOutlineInfo.ry
		}
	}
	else if (currentOutline == customOutlineInfo) {
		shape = 'outline';
		outlineInfo = Outliner.pathInfo;
	}
	var width = $('#width').val();
	var height = $('#height').val();
	currentOutline.fill = outlineOptions.fill;
	var stickerJSON = {
		canvas: canvasJSON,
		shape: shape,
		width: width,
		height: height,
		outlineInfo: outlineInfo,
		backImgInfo: backImgInfo
	};
	var jsonData = {
		stickerData: JSON.stringify(stickerJSON),
		isThumbnail: '0',
		thumbnailSize: 150,
		stickerID: 'test',
		stickerType: stickerType,
		// simon fix. change to 1 if mmdev, 0 if live site
		useDevSite: '0'
	};

	console.log(JSON.stringify(stickerJSON));
	// var dataToSend = JSON.stringify(jsonData);
	$('#stickerPreview').html('<p>sending sticker data... rendering on server...</p>');
	$.ajax({
		type: "POST",
		url: '/node/rendercanvas',
		data: jsonData,
		dataType: 'html',

		success: function(data) {
		data += '<p>success</p>';
		$('#stickerPreview').html(data);
		},

		error: function(jqXHR, textStatus, errorThrown) {
		$('#stickerPreview').html('<p>Something went wrong: </p><p>jqXHR: '+JSON.stringify(jqXHR)+'</p><p>Text Status: '+textStatus+'</p><p>Error Thrown: '+errorThrown+'</p>');
		}
	})
}


/* Drag and Drop code adapted from http://www.html5rocks.com/en/tutorials/dnd/basics/ */

// var canvas = new fabric.Canvas('canvas');

/*
NOTE: the start and end handlers are events for the <img> elements; the rest are bound to
the canvas container.
*/

function handleDragStart(e) {
    [].forEach.call(images, function (img) {
        img.classList.remove('img_dragging');
    });
    this.classList.add('img_dragging');
	// console.log(this);
	e.dataTransfer.setData('text/plain', this.name);
}

function handleDragOver(e) {
    if (e.preventDefault) {
        e.preventDefault(); // Necessary. Allows us to drop.
    }

    e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
    // NOTE: comment above refers to the article (see top) -natchiketa

    return false;
}

function handleDragEnter(e) {
    // this / e.target is the current hover target.
    this.classList.add('over');
}

function handleDragLeave(e) {
    this.classList.remove('over'); // this / e.target is previous target element.
}

function handleDrop(e) {
    // this / e.target is current target element.

    if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
    }

    // var img = document.querySelector('#clipartShow img.img_dragging');
	// e.dataTransfer.getData('text/html');

    // console.log('event: ', e);

    // var newImage = new fabric.Image(img, {
        // width: img.width,
        // height: img.height,
        // // Set the center of the new object based on the event coordinates relative
        // // to the canvas container.
        // left: e.layerX,
        // top: e.layerY
    // });
    // canvas.add(newImage);
	// console.log(img);
	var imgName = e.dataTransfer.getData('text/plain');
	// console.log(imgName);
	addClipart(imgName, {left:e.layerX, top: e.layerY});
    return false;
}

function handleDragEnd(e) {
    // this/e.target is the source node.
    [].forEach.call(images, function (img) {
        img.classList.remove('img_dragging');
    });
}


// if (Modernizr.draganddrop) {
//     // Browser supports HTML5 DnD.
//
//     // Bind the event listeners for the image elements
//     var images = document.querySelectorAll('#clipartShow img');
//     [].forEach.call(images, function (img) {
//         img.addEventListener('dragstart', handleDragStart, false);
//         img.addEventListener('dragend', handleDragEnd, false);
//     });
//     // Bind the event listeners for the canvas
//     var canvasContainer = document.getElementById('canvasDiv');
//     canvasContainer.addEventListener('dragenter', handleDragEnter, false);
//     canvasContainer.addEventListener('dragover', handleDragOver, false);
//     canvasContainer.addEventListener('dragleave', handleDragLeave, false);
//     canvasContainer.addEventListener('drop', handleDrop, false);
// } else {
//     // Replace with a fallback to a library solution.
//     console.log("This browser doesn't support the HTML5 Drag and Drop API.");
// }


// Measurement bar code
// used to scale horizontal and vertical measure bars

// function setBarWidth(widthpx, widthmm)
// {

	// var boxParent = $('#topMeasureBoxParent');
	// var box = $('#topMeasureBox');
	// var longbars = $('#topMeasureBox .centerVert .longbar');
	// var info = $('#topMeasure');
	// var textWidth = parseInt(info.css('width').replace('px', ''));
	// var boxWidth = widthpx+15;

	// boxParent.css('width', boxWidth);
	// var barWidth = (boxWidth - textWidth - 10)/2;
	// info.html(widthmm + ' mm');
	// longbars.css('width', barWidth);
	// box.css('width', boxWidth);
// }

// function setBarHeight(heightpx, heightmm)
// {
	// var boxParent = $('#rightMeasureBoxParent');
	// var box = $('#rightMeasureBox');
	// var longbars = $('#rightMeasureBox .centerHori .longbar');
	// var info = $('#sideMeasure');
	// var textHeight = parseInt(info.css('height').replace('px', ''));
	// var boxHeight = heightpx+5;

	// boxParent.css('height', boxHeight);
	// var barHeight = (boxHeight - textHeight - 10)/2;
	// info.html(heightmm + ' mm');
	// longbars.css('height', barHeight);
	// box.css('height', boxHeight);
// }

// setBarWidth(450, 37);
// setBarHeight(200, 16);


function post(path, params, method) {
    method = method || "post"; // Set method to post by default if not specified.

    // The rest of this code assumes you are not using a library.
    // It can be made less wordy if you use one.
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
}

fabric.Object.prototype.customiseCornerIcons({
    settings: {
        borderColor: 'black',
        cornerSize: 60,
        cornerShape: 'circle',
        cornerBackgroundColor: 'rgb(40, 170, 225)',
        cornerPadding: 20,

    },
    mtr: {
        icon: '/img/icons/rotate.svg'
    },
		tl: {
			icon: '/img/icons/resize-flip.svg'
		},
    tr: {
        icon: '/img/icons/resize.svg'
    },
    bl: {
        icon: '/img/icons/resize.svg'
    },
    br: {
        icon: '/img/icons/remove.svg',
        settings: {
          cornerBackgroundColor: 'red',
        },
    },
		mt: {
			  icon: '/img/icons/up.svg'
		},
    mb: {
        icon: '/img/icons/down.svg'
    },
		mr: {
			icon: '/img/icons/right.svg'
		},
		ml: {
			icon: '/img/icons/left.svg'
		},
}, function() {
    canvas.renderAll();
} );

fabric.Canvas.prototype.customiseControls({
	br: {
		 	action: 'remove',
		  cursor: 'pointer'
	}
}, function() {
    canvas.renderAll();
} );

// fix for background. background will not show up after upload otherwise
setInterval(function(){canvas.renderAll();}, 3000);
