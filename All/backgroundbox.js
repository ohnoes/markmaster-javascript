/*
 * Function
 */
function updateBackgroundBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var backgroundBox = $(".backgroundBox");
	backgroundBox.css("width", canvasWidth);
	backgroundBox.css("height", canvasHeight);
	backgroundBox.offset({top: canvasOffset.top+20, left:20});
}

function backgroundBoxHide() {
	$('.backgroundBox').css("display", "none");
}

function backgroundBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.backgroundBox').css("display", "inherit");
	updateBackgroundBox();
}

function backgroundBoxToggle() {
  if(document.getElementById('backgroundBox').style.display == "none") {
    backgroundBoxShow();
  } else {
    backgroundBoxHide();
  }
}

$(document).ready(function() {
	updateBackgroundBox();
  backgroundBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateBackgroundBox();
}
