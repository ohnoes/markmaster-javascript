/*
 * Function
 */
function updateTextBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var textBox = $(".textBox");
	textBox.css("width", canvasWidth);
	textBox.css("height", canvasHeight);
	textBox.offset({top: canvasOffset.top+20, left:20});
}

function textBoxHide() {
	$('.textBox').css("display", "none");
}

function textBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.textBox').css("display", "inherit");
	updateTextBox();
}

function textBoxToggle() {
	canvas.deactivateAll().renderAll();
	document.getElementById('text-input').value = "";
  if(document.getElementById('textBox').style.display == "none") {
    textBoxShow();
  } else {
    textBoxHide();
  }
}

$(document).ready(function() {
	updateTextBox();
  textBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateTextBox();
}
