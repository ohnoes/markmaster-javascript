function canvasToTmpImg(source){
  //original height and width of the background
  var width = source.backgroundImage.width;
  var height = source.backgroundImage.height;

  source.backgroundImage.strokeWidth = 5;
  source.overlayImage.opacity = 0;
  source.deactivateAll().renderAll();

  var extraPadding = 0;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + ((width+extraPadding) / 2));
  var pathStartY = 450 - (225 + ((height+extraPadding) / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = source.toDataURL({
    format: 'png',
    multiplier: 1,
    width: width+5,
    height: height+5,
    left: pathStartX,
    top: pathStartY
  });
  return imgData;
}

function createTemplate(event) {
    event.preventDefault();
    canvas.deactivateAll();

    if(true) { // set the json
     var canvasJSON = canvas.toJSON(['selectable', 'currentSrc']);
   	 var shape = 'rect';
   	 var outlineInfo;
   	  if (currentOutline == rectOutlineInfo)
   	  {
   	    shape = 'rect';
   	    outlineInfo = {
   	      fill: outlineOptions.fill,
   	      shape: 'rect',
   	      width: rectOutlineInfo.width,
   	      height: rectOutlineInfo.height,
   	      left: rectOutlineInfo.left,
   	      top: rectOutlineInfo.top,
   	      rx: rectOutlineInfo.rx,
   	      ry: rectOutlineInfo.ry
   	    }
   	  }
   	  else if (currentOutline == ellipseOutlineInfo)
   	  {
   	    shape = 'ellipse';
   	    outlineInfo = {
   	      fill: outlineOptions.fill,
   	      shape: 'ellipse',
   	      left: ellipseOutlineInfo.left,
   	      top: ellipseOutlineInfo.top,
   	      rx: ellipseOutlineInfo.rx,
   	      ry: ellipseOutlineInfo.ry
   	    }
   	  }
   	  var width = $('#width').val();
   	  var height = $('#height').val();
   	  currentOutline.fill = outlineOptions.fill;
   	  var jsonData = {
   	    canvas: canvasJSON,
   	    shape: shape,
   	    width: width,
   	    height: height,
   	    outlineInfo: outlineInfo,
   	    backImgInfo: backImgInfo
   	  };
   	  var dataToSend = JSON.stringify(jsonData);
    }

    var postjson = dataToSend;
    var tmpquantity = document.getElementById('tmpquantity').value;
    var tmporder = document.getElementById('tmporder').value;
    var tmptype = document.getElementById('tmptype').value;
    var tmptitle = document.getElementById('tmptitle').value;
    var tmpdescript = document.getElementById('tmpdescript').value;
    var tmpsearch = document.getElementById('tmpsearch').value;
    var postthumbnail = canvasToTmpImg(canvas);
    var output=postthumbnail.replace(/^data:image\/(png|jpg);base64,/, "");
    var test1;

    var THE_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var _TOKEN = $('[name="_token"]').val();

    var formData = new FormData($('#makeTmp')[0]);
    formData.append('thumbnail', output);
	  formData.append('stickerType', tmptype);
    formData.append('json', postjson);
    formData.append('quantity', tmpquantity);
    formData.append('order', tmporder);
    formData.append('title', tmptitle);
    formData.append('descript', tmpdescript);
    formData.append('search', tmpsearch);
    $.ajax({
        type: 'post',
        url: 'makenewtmp',
        beforeSend: function (xhr) {
          var token = $('meta[name="csrf-token"]').attr('content');
          if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
         },
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        beforeSend: function() {
			      showPopup();
            $("#validation-errors").hide().empty();
			// console.log("sending clipart");
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {

			putMessage(data.split("failed")[1]);
		  }
		  else {
			  // SUCCESS MOVE TO ADMIN PAGE
        var redir = document.getElementById('redirect').value;
        window.location.replace(server_path + redir);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            console.error('AJAX error Something went to wrong.Please Try again later...');
            console.error(xhr);
            console.error(textStatus);
            console.error(thrownError);
        }
    });
}

//$('#cilpartForm').submit(uploadClipart(clipartType));
$('#makeTmp').submit(createTemplate);
