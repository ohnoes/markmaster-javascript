function loadfromJsonExp(jsonstring, filename, type ){
    canvas.clear();
    canvas.renderAll();
    sleep(1000);
  jsonstring = JSON.stringify(jsonstring);

  if (jsonstring == 'missmatch')
  {
	return;
  }

  jsonstring = jsonstring.replace(/(['"])src(['"]):(['"])http:\/\/.*?\//, '$1src$2:$3' + server_path);
  jsonstring = jsonstring.replace(/\n/g, "\\n");
  var json = JSON.parse(jsonstring);
  // console.log(json);
  var objectsToAdd = [];
  var numReplaced = 0;


  var mmWidth = parseInt(json.width);
  var mmHeight = parseInt(json.height);

  //*
  //delete json.canvas.backgroundImage;
  //delete json.canvas.overlayImage;
  canvas.loadFromJSON(json.canvas, function() {
  if(type == 3 || type == 4) {
      outlineOptions.fill = '#ffffff';
  } else {
    outlineOptions.fill = json.outlineInfo.fill;
  }
	var outlinePath;
	resizeOutlineExp(mmWidth, mmHeight);
	if (json.shape == 'rect')
	{
		currentOutline = rectOutlineInfo;
		outlinePath = getRectClipPath();
	}
	else if (json.shape == 'ellipse')
	{
		currentOutline = ellipseOutlineInfo;
		outlinePath = getEllipseClipPath();
	}
  if (outlinePath != null){
     var overlay = outlinePath.cloneAsImage();
   }
	canvas.setOverlayImage(overlay);
	canvas.renderAll();

	if (json.backImgInfo.beingUsed)
	{
		var backImg = new Image();
		backImg.src = json.backImgInfo.src;

		backImgInfo.isLocal = false;
		addBackgroundFromImgElement(backImg);
	}

	canvas.renderAll.bind(canvas);
    var destination = new fabric.Canvas('myCanvas');
    var source = "";

    if(type == 6 || type == 7) {
      filename = filename + ".png";
      var postthumbnail = stickerToImage(canvas, mmWidth, mmHeight);
      var img = new Image();
      img.src = postthumbnail;
      var url = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

      var uri = postthumbnail;
      var downloadLink = document.createElement("a");
      downloadLink.href = uri;
      downloadLink.download = filename;

      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);


    } else {
      filename = filename + ".pdf";
      if (json.backImgInfo.beingUsed) {
        if (json.shape == 'rect') {
          imageRectToPDF(source, destination, filename, mmWidth, mmHeight);
        } else if (json.shape == 'ellipse') {
          imageEllipseToPDF(source, destination, filename, mmWidth, mmHeight);
        } else if (json.shape == 'outline'){
          outlineToPDF(filename, json.outlineInfo, mmWidth, mmHeight, true);
        }
      } else {
        if (json.shape == 'rect') {
          rectToPDF(source, destination, filename, mmWidth, mmHeight);
        } else if (json.shape == 'ellipse') {
          ellipseToPDF(source, destination, filename, mmWidth, mmHeight);
        } else if (json.shape == 'outline'){
          outlineToPDF(filename, json.outlineInfo, mmWidth, mmHeight, false);
        }
      }
    }

  },
  function(o, object){
	if (o.type == 'image')
	{
		// object.src = o.src.replace(/http:\/\/.*?\//, 'http://79.161.166.153/');
		applyClipartSettings(object);
	}
	else if (o.type == 'path' || o.type == 'path-group')
	{
		object.isSVG = true;
		applyClipartSettings(object);
	}
	else if (o.type == 'text')
	{
		applyTextSettings(object);
	}
	else if (o.type == 'i-text')
	{
		applyTextSettings(object);
	}
  });

  setBorderColor();
  setPrice();
  //*/

  canvas.calcOffset();
  canvas.renderAll();

}

function stickerToImage(source, mmWidth, mmHeight){
  var mm2pixels = 48;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed

  var padding;
  if(mmWidth == 30) {
    padding = 80;
  } else if(mmWidth == 37) {
    padding = 65;
  } else {
    padding = 38;
  }

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = (parseInt(mmWidth)) / 2
  var marginT = (parseInt(mmHeight)) / 2

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + ((bgWidth-padding) / 2));
  var pathStartY = 450 - (225 + ((bgHeight-padding) / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth - padding,
    height: bgHeight - padding,
    left: pathStartX,
    top: pathStartY
  });

  return imgData;
}

//use to render a sticker that contains an image as a background and is in the form of an ellipse
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function imageEllipseToPDF(source, destination, filename, mmWidth, mmHeight) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //});
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;


  //destination.backgroundImage.scaleToWidth(bgWidth);
  //destination.backgroundImage.scaleToHeight(bgHeight);

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = 0.5 + (parseInt(mmWidth)) / 2;
  var marginT = 0.5 + (parseInt(mmHeight)) / 2;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth) +1, parseInt(mmHeight) +1, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  //pdf.ellipse(margin + marginL, margin + marginT , (parseInt(mmWidth)-1)/2 , (parseInt(mmHeight)-1)/2 ,'S');
  pdf.ellipse(margin + marginL, margin + marginT , (parseInt(mmWidth))/2 , (parseInt(mmHeight))/2 ,'S');
  pdf.save(filename);
}

//use to render a sticker with a background image in the shape of a rectangle
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function imageRectToPDF(source, destination, filename, mmWidth, mmHeight) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //});
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width;
  var bgHeight = height;


  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = 0.5;
  var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth)+1, parseInt(mmHeight)+1, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  //pdf.roundedRect(margin + marginL, margin + marginT , parseInt(mmWidth)-1 , parseInt(mmHeight)-1, 2 , 2 ,'S');
  pdf.roundedRect(margin + marginL, margin + marginT , parseInt(mmWidth) , parseInt(mmHeight), 2 , 2 ,'S');
  pdf.save(filename);
}

//use with solid color background images in the shape of an ellipse
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function ellipseToPDF(source, destination,filename, mmWidth, mmHeight) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //});

  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 1 * mm2pixels;
  var bgScaleMM = 1;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;


  canvas.backgroundImage.scaleX = bgWidth/width;
  canvas.backgroundImage.scaleY = bgHeight/height;

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = 0.5 + (parseInt(mmWidth)) / 2;
  var marginT = 0.5 + (parseInt(mmHeight)) / 2;

  //var marginL = 0.5;
  //var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth) + bgScaleMM, parseInt(mmHeight) + bgScaleMM, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.ellipse(margin + marginL, margin + marginT , parseInt(mmWidth)/2 , parseInt(mmHeight)/2 ,'S');
  pdf.save(filename);
}

//use with stickers with colored backgrounds in the shape of a rectangle
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function rectToPDF(source, destination,filename, mmWidth, mmHeight) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //
  // });
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  console.log("mmWidth: " + mmWidth);
  console.log("mmHeight: " + mmHeight);
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 1 * mm2pixels;
  var bgScaleMM = 1;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  canvas.backgroundImage.scaleX = bgWidth/width;
  canvas.backgroundImage.scaleY = bgHeight/height;

  // this didn't fix anything
  //canvas.backgroundImage.scaleX *= 1.1;
  //canvas.backgroundImage.scaleY *= 1.1;

  var marginL = 0.5;
  var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + mmWidth;
  var pdfHeight = (margin * 2) + mmHeight;

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //(imageData, format, x, y, w, h, alias, compression, rotation)
  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, mmWidth + bgScaleMM, mmHeight + bgScaleMM, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.roundedRect(margin + marginL, margin + marginT , mmWidth, mmHeight, 2 , 2 ,'S');
  pdf.save(filename);
}

function outlineToPDF(filename, outlineInfo, mmWidth, mmHeight, usingBackgroundImage) {
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  console.log("mmWidth: " + mmWidth);
  console.log("mmHeight: " + mmHeight);
  var mm2pixels = 24;

  // force background image to match canvas size
  if (usingBackgroundImage && canvas.backgroundImage){
    canvas.backgroundImage.width = canvas.width;
    canvas.backgroundImage.height = canvas.height;
  }

  canvas.setOverlayImage(null);
  canvas.renderAll();

  var bounds = outlineInfo.bounds;
  var padding = outlineInfo.padding || 10; // default padding to 0 if none found
  var margin = 36;
  var outlineWidth = bounds.width;
  var outlineHeight = bounds.height;
  var width2Height = outlineHeight / outlineWidth;
  mmHeight = mmWidth * width2Height; // height dependent on width

  var sticker2mm = mmWidth / outlineWidth;

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + mmWidth;
  var pdfHeight = (margin * 2) + mmHeight;

  // have to choose landscape or portrait for the layout type
  var layoutType = outlineWidth > outlineHeight ? 'l' : 'p';
  var pdf = new jsPDF(layoutType, 'mm', [pdfWidth, pdfHeight]);

  // crop sticker
  var imgX = margin;
  var imgY = margin;
  var imgWidth = mmWidth;
  var imgHeight = mmHeight;
  var mult = mmWidth * mm2pixels / outlineWidth; // render sticker image with dpi taken into account
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: outlineWidth+(2*padding),
    height: outlineHeight+(2*padding),
    left: bounds.left-padding,
    top: bounds.top-padding
  });

  var sizeMod = 2*padding*sticker2mm;

  //(imageData, format, x, y, w, h, alias, compression, rotation)
  pdf.addImage(imgData, 'png', imgX, imgY, imgWidth+sizeMod, imgHeight+sizeMod, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  // crop outline
  var outlineX = outlineInfo.x - bounds.left;
  var outlineY = outlineInfo.y - bounds.top;

  outlineX += 10;
  outlineY += 10;

  var outlineScale = sticker2mm;
  outlineX = outlineX * outlineScale + margin;
  outlineY = outlineY * outlineScale + margin;


  pdf.lines(outlineInfo.curves, outlineX, outlineY, [outlineScale, outlineScale])
  pdf.save(filename);
}


function stickerRender(id) {
  var jsonToLoad = JSON.parse(document.getElementById(id).value);
  var sticktype = document.getElementById("type-"+id).value;
  // console.log("sticktype: " + sticktype);
  // console.log("SHAPE: " + jsonToLoad.shape);
  // console.log("Background used: " + jsonToLoad.backImgInfo.beingUsed);
  var filename = "" + id;

  loadfromJsonExp(jsonToLoad, filename, sticktype);
  canvas.forEachObject(function(o) {
    o.selectable = false;
  });
}

function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}

function resizeOutlineExp(width, height) {

  var ratio = width / height;

  // canvas width, height, center
  var cWidth = canvas.getWidth();
  var cHeight = canvas.getHeight();
  var centerX = cWidth / 2;
  var centerY = cHeight / 2;

  // aspect width/height
  var aWidth;
  var aHeight;

  // pads dimension with higher value, then calculates other
  // ex: if width is more than height, then pad width and calculate height, else vice versa
  var cornerRadius = 2;
  if ((width/2) > height)
  {
    aWidth = cWidth - 2*outlinePadding;
    aHeight = aWidth / ratio;
    mm2px = aWidth / width;
  }
  else
  {
    aHeight = cHeight - 2*outlinePadding;
    aWidth = ratio * aHeight;
    mm2px = aHeight/height;
  }

  cornerRadius *= mm2px;

  // set width and height of outlines and center them
  rectOutlineInfo.width = aWidth;
  rectOutlineInfo.height = aHeight;
  rectOutlineInfo.left = centerX;
  rectOutlineInfo.top = centerY;
  rectOutlineInfo.rx = cornerRadius;
  rectOutlineInfo.ry = cornerRadius;

  ellipseOutlineInfo.rx = aWidth/2;
  ellipseOutlineInfo.ry = aHeight/2;
  ellipseOutlineInfo.left = centerX;
  ellipseOutlineInfo.top = centerY;

  //orderLayers();

  //canvas.setBackgroundImage(currentOutline); // added
  //updateTextSize();
  canvas.renderAll();
}
