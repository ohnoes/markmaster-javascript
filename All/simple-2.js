// Add simple text (check how much text is input, and if there's a clipart)
function addSimpleText() {
    // Clear all text objects before adding new, also check for clipart
    var clipartAdded = false;
    canvas.getObjects();
    for(var i = canvas.getObjects().length-1; i >= 0; i--){
      if(canvas.item(i).isType('i-text')) {
        canvas.remove(canvas.item(i));
      } else if (canvas.item(i).isType('image') || canvas.item(i)['isSVG'] == true) {
        clipartAdded = true;
      }
    }

  var cWidth = canvas.getWidth();
	var cHeight = canvas.getHeight();

  var oneTextTop1 = cHeight/1.95;
  var twoTextTop1 = cHeight/2.7272;
  var twoTextTop2 = cHeight/1.5789;
  var threeTextTop1 = cHeight/3.4285;
  var threeTextTop2 = cHeight/2;
  var threeTextTop3 = cHeight/1.4117;

  OutlineInput.sanitizeInputs();
  var width = OutlineInput.getWidth();
  var height = OutlineInput.getHeight();

	var mmLimit;

	if (width > height)
	  {
		mmLimit = width/cWidth;
	  }
	  else
	  {
		mmLimit = height/cHeight;
	  }

	var minFontScale = 2.47 / (26 * mmLimit);

    // get the number of lines being added (objects)
    var textOne = document.getElementById('text1').value;
    var textTwo = document.getElementById('text2').value;
    var textThree = document.getElementById('text3').value;

    // REMOVE EMOJI
    textOne = validatedText(textOne);
    if(textOne != document.getElementById('text1').value) {
      alert('Illegal character removed!');
    }
    document.getElementById('text1').value = textOne;

    // REMOVE EMOJI
    textTwo = validatedText(textTwo);
    if(textTwo != document.getElementById('text2').value) {
      alert('Illegal character removed!');
    }
    document.getElementById('text2').value = textTwo;

    // REMOVE EMOJI
    textThree = validatedText(textThree);
    if(textThree != document.getElementById('text3').value) {
      alert('Illegal character removed!');
    }
    document.getElementById('text3').value = textThree;

    var firstFont = $('#fontSelect').css('font-family');
    if (firstFont.indexOf(", ") == -1) {
        var testFont = firstFont.split(",");
    } else {
        var testFont = firstFont.split(", ");
    }
    var theFont = testFont[1];

    var numLines = 0;
    if(textOne.length > 0) {
        numLines++;
    }
    if(textTwo.length > 0) {
        numLines++;
    }
    if(textThree.length > 0) {
        numLines++;
    }

    // We got clipart, and need to make the text smaller to make room for the clipart
    if(clipartAdded) {
      var leftPos = cWidth/1.6216;
      var maxWidth = cWidth/1.9354;
      if($('#stickertype').val() == 7) {
          leftPos = cWidth/1.6666;
      }

      /*
      var leftPos = 740; // cWidth/1.6216
      var maxWidth = 620; // cWidth/1.9354
      if($('#stickertype').val() == 7) {
          leftPos = 720; // cWidth/1.6666
      }

    // There is no clipart, make text take majority of the space

      var leftPos = 600; // cWidth/2
      var maxWidth = 800; // cWidth/1.5
*/

    // There is no clipart, make text take majority of the space
    } else {
      var leftPos = cWidth/2;
      var maxWidth = cWidth/1.3;
    }
    var fontSize = 20;
    var firstText, secondText;
    if(numLines == 1) {
        var oneScale = cWidth/80; // 20
        if($('#stickertype').val() == 7) {
            oneScale = cWidth/120; // 13
        }

      if(textOne.length > 0) {
        firstText = textOne;
      } else if (textTwo.length > 0) {
        firstText = textTwo;
      } else {
        firstText = textThree;
      }
        var theFill = document.getElementById('fill').value;
        var currentText = new fabric.IText(firstText,
          { left: leftPos,
            top: oneTextTop1,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: oneScale,
            scaleY: oneScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);
        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        var moveTOP = 1;
        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*oneScale;
            currentText.scaleY = (maxWidth/objectWidth)*oneScale;
            moveTOP = 1 - (currentText.scaleX / oneScale);
            objectScale = currentText.scaleX;
            if(objectScale < minFontScale) {
                // remove last letter from firstText
                if(textOne.length > 0) {
                   str = document.getElementById('text1').value;
                   var newStr = str.substring(0, str.length-1);
                   document.getElementById('text1').value = newStr;
                } else if (textTwo.length > 0) {
                    str = document.getElementById('text2').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text2').value = newStr;
                } else {
                    str = document.getElementById('text3').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text3').value = newStr;
                }
                // recursive call
                addSimpleText();
                return;
            }

        }
        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);
    } else if (numLines == 2) {
        var textScale = cWidth/170; // 10
        var firstTextTop = twoTextTop1;
        var secondTextTop = twoTextTop2;
        if($('#stickertype').val() == 7) {
            textScale = cWidth/240; // 7
            firstTextTop = twoTextTop1+20;
            secondTextTop = twoTextTop2-10;
        }

      if(textOne.length > 0) {
        firstText = textOne;
        if(textTwo.length > 0) {
          secondText = textTwo;
        } else {
          secondText = textThree;
        }
      } else {
        firstText = textTwo;
        secondText = textThree;
      }
        var currentText = new fabric.IText(firstText,
          { left: leftPos,
            top: firstTextTop,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: textScale,
            scaleY: textScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);
        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*textScale;
            currentText.scaleY = (maxWidth/objectWidth)*textScale;
            objectScale = currentText.scaleX;
            var firstScale = objectScale;

            if(firstScale < minFontScale) {
                // remove last letter from firstText
                if(textOne.length > 0) {
                    str = document.getElementById('text1').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text1').value = newStr;
                } else {
                    str = document.getElementById('text2').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text2').value = newStr;
                }
                // recursive call
                addSimpleText();
                return;
            }
        }

        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);

        var currentText = new fabric.IText(secondText,
          { left: leftPos,
            top: secondTextTop,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: textScale,
            scaleY: textScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);

        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        var moveTOP = 1;

        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*textScale;
            currentText.scaleY = (maxWidth/objectWidth)*textScale;
            moveTOP = 1 - (currentText.scaleX / textScale);
            objectScale = currentText.scaleX;
            currentText.top = secondTextTop - (moveTOP * 10);
            if(objectScale < minFontScale) {
                if(textOne.length > 0) {
                  if(textTwo.length > 0) {
                      str = document.getElementById('text2').value;
                      var newStr = str.substring(0, str.length-1);
                      document.getElementById('text2').value = newStr;
                  } else {
                      str = document.getElementById('text3').value;
                      var newStr = str.substring(0, str.length-1);
                      document.getElementById('text3').value = newStr;
                  }
                } else {
                    str = document.getElementById('text3').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text3').value = newStr;
                }
                // recursive call
                addSimpleText();
                return;
            }
        }
        if (firstScale < objectScale) {
            currentText.scaleX = firstScale;
            currentText.scaleY = firstScale;
            moveTOP = 1 - (currentText.scaleX / textScale);
            objectScale = currentText.scaleX;
            currentText.top = secondTextTop - (moveTOP * 10);
        }
        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);

    } else if (numLines == 3) {
      var fontScale = cWidth/220; // 8

      var firstTextLine = new fabric.IText(textOne,
        { left: leftPos,
          top: threeTextTop1,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          textAlign: 'center',
          fontSize: 20,
          scaleX: fontScale,
          scaleY: fontScale,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });

      // Use width and x scale to make sure it fits (if the width extends the size)
      //canvas.setActiveObject(currentText);
      var firstWidth = firstTextLine.width;
      firstTextLine.fill = document.getElementById('fill').value;

      var secondTextLine = new fabric.IText(textTwo,
        { left: leftPos,
          top: threeTextTop2,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          textAlign: 'center',
          fontSize: 20,
          scaleX: fontScale,
          scaleY: fontScale,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });
      //canvas.setActiveObject(currentText);
      var secondWidth = secondTextLine.width;
      secondTextLine.fill = document.getElementById('fill').value;

      var thirdTextLine = new fabric.IText(textThree,
        { left: leftPos,
          top: threeTextTop3,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          scaleX: fontScale,
          scaleY: fontScale,
          textAlign: 'center',
          fontSize: 20,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });

      // Use width and x scale to make sure it fits (if the width extends the size)
      //canvas.setActiveObject(currentText);
      var thirdWidth = thirdTextLine.width;
      thirdTextLine.fill = document.getElementById('fill').value;


    var largestWidth = firstWidth;
    // find the greatest width text of the three
    if(firstWidth >= secondWidth) {
        if(firstWidth >= thirdWidth) {
            largestWidth = firstWidth;
        } else {
            largestWidth = thirdWidth;
        }
    } else {
        if (secondWidth >= thirdWidth) {
            largestWidth = secondWidth;
        } else {
            largestWidth = thirdWidth;
        }
    }
    if ((largestWidth*4) > maxWidth) {
      console.log("MAKE SMALLER!");
        if((maxWidth/largestWidth) < minFontScale) {
            if(firstWidth >= secondWidth) {
                if(firstWidth >= thirdWidth) {
                    str = document.getElementById('text1').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text1').value = newStr;
                } else {
                    str = document.getElementById('text3').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text3').value = newStr;
                }
            } else {
                if (secondWidth >= thirdWidth) {
                    str = document.getElementById('text2').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text2').value = newStr;
                } else {
                    str = document.getElementById('text3').value;
                    var newStr = str.substring(0, str.length-1);
                    document.getElementById('text3').value = newStr;
                }
            }
            // recursive call
            addSimpleText();
            return;
        }

        firstTextLine.scaleX    = (maxWidth/largestWidth);
        firstTextLine.scaleY    = (maxWidth/largestWidth);
        secondTextLine.scaleX   = (maxWidth/largestWidth);
        secondTextLine.scaleY   = (maxWidth/largestWidth);
        thirdTextLine.scaleX    = (maxWidth/largestWidth);
        thirdTextLine.scaleY    = (maxWidth/largestWidth);
    }

    canvas.add(firstTextLine);
    canvas.add(secondTextLine);
    canvas.add(thirdTextLine);
    // we have width, height, left and top
    // Center: left 150, top 55
}

}

function orderSticker(){
    var value = $('#stickertype').val();
    rdycheckout(value);
    document.getElementById("cartForm").submit();
}

function resizeTopMargin()
{
    //get the canvas div
    if($(window).width() < 768)
        $('#textBox').css({'padding-top':$("#canvasDiv").height() + 10});
    else {
        $('#textBox').css({'padding-top': 10});
    }
}

function clearTheSticker(){
    canvas.clear();
    if($('#stickertype').val() == 3) {
      var bgColor = '#cfb53b';
      setBackgroundColor(bgColor);
    } else if ($('#stickertype').val() == 4){
      var bgColor = '#d7d8d8';
      setBackgroundColor(bgColor);
    } else {
      var bgColor = '#ffffff';
      setBackgroundColor(bgColor);
    }
}
