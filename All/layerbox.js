/*
 * Function
 */
function updateLayerBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var layerBox = $(".layerBox");
	layerBox.css("width", canvasWidth);
	layerBox.css("height", canvasHeight);
	layerBox.offset({top: canvasOffset.top+20, left:20});
}

function layerBoxHide() {
	$('.layerBox').css("display", "none");
}

function layerBoxShow() {
	shapeBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	clipartBoxHide();
  backgroundBoxHide();
	editBoxHide();
	function1();
	$('.layerBox').css("display", "inherit");
	updateLayerBox();
}

function layerBoxToggle() {
  if(document.getElementById('layerBox').style.display == "none") {
    layerBoxShow();
  } else {
    layerBoxHide();
  }
}

$(document).ready(function() {
	updateLayerBox();
  layerBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateLayerBox();
}

function function1() {
	var ul = document.getElementById("sortable");
	// clear the old list
	$(ul).empty();

	canvas.deactivateAll();
	canvas.renderAll();
	var object;
	var counter = 0;

  var objects = canvas.getObjects();
	for(var i = objects.length-1; i >= 0; i--) {
		// text
		if (isText(objects[i])) {
			if (objects[i].text.length > 10) {
				var layerText = objects[i].text.substr(0,9) + "...";
			} else {
				var layerText = objects[i].text;
			}
			var li = document.createElement("li");
			var barrow = document.createElement("b");
			var block = document.createElement("b");
			barrow.setAttribute("class", "fa fa-fw fa-lg fa-arrows-v");
			barrow.setAttribute("style", "float: left;");
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(barrow);
			li.appendChild(document.createTextNode(layerText));
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		} else if (isImg(objects[i])) { // clipart
			var layerImg = objects[i]._element.currentSrc;
			var oImg = document.createElement("img");
			oImg.setAttribute('src', layerImg);
			oImg.setAttribute('alt', 'na');
			oImg.setAttribute('height', '35px');
			var li = document.createElement("li");
			var barrow = document.createElement("b");
			var block = document.createElement("b");
			barrow.setAttribute("class", "fa fa-fw fa-lg fa-arrows-v");
			barrow.setAttribute("style", "float: left;");
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(barrow);
			li.appendChild(oImg);
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		} else if(isSVG(objects[i])) {
			var layerText = 'SVG';
			var li = document.createElement("li");
			var barrow = document.createElement("b");
			var block = document.createElement("b");
			barrow.setAttribute("class", "fa fa-fw fa-lg fa-arrows-v");
			barrow.setAttribute("style", "float: left;");
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(barrow);
			li.appendChild(document.createTextNode(layerText));
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		}
		counter++;
	}

}
