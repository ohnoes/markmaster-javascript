/*
 * Function
 */
function updatePopBox() {
  var height = $(document).height();
  var width = $(document).width();
	//then we'll set its properties
	var popBox = $(".popBox");
	popBox.css("width", width/2);
	popBox.css("height", height/2);
	popBox.offset({top: height/10, left:width/10});
}

function popBoxHide() {
	$('.popBox').css("display", "none");
}

function popBoxShow() {

	$('.popBox').css("display", "inherit");

}

function popBoxToggle() {
  if(document.getElementById('popBox').style.display == "none") {
    popBoxShow();
  } else {
    popBoxHide();
  }
}

$(document).ready(function() {
	updatePopBox();
  popBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updatePopBox();
}
