/////////////////////////////////////////////////////////
//        tab   4  -  Clip Art
/////////////////////////////////////////////////////////


function clipartWithColor()
{
  alert('test');
  return false;
}

//////////////////////////////////////////////////////////////////////////////////////
//  Function to add clipart from the database to the canvas
//  returns image object added to canvas
//////////////////////////////////////////////////////////////////////////////////////
var xs;
var xstr;
var ctest;
function addClipart(name, clipartOptions, loadFromURL)
{

  if (name.substring(name.length-4, name.length) == '.svg')
  {
      fabric.loadSVGFromURL(name, function(objects, options){
        var object = new fabric.util.groupSVGElements(objects, options);
        object.set({
            left: canvas.getWidth()/2,
            top: canvas.getHeight()/2,
            originX: 'center',
            originY: 'center',
            //scale object to 200x200
            scaleX: 200 / options.width,
            scaleY: 200 / options.height
        });
		      object.set(clipartOptions);
        // ensure object is uniformly scaled
        var scale = object.scaleX;
        if (scale < object.scaleY) {
          scale = object.scaleY;
        }
        object.scale(scale, scale);

        object['isSVG'] = true;
        object['currentSrc'] = name;
		    applyClipartSettings(object);
        object.rotatingPointOffset = 80;
        canvas.add(object);
        canvas.setActiveObject(object);
        canvas.renderAll();
    });
  }
  else {
    // UPLOADED
  	var currentImg;
  	if (loadFromURL){
  		// simon fix later - mmdev
      //console.log("IT WAS LOADED FROM URL");
  		var newName = server_path + name;
  		currentImg = new fabric.Image.fromURL(newName, function(currentImg) {

        var oldWidth = currentImg.width;
        var oldHeight = currentImg.height;
        var newHeight = oldHeight/(oldWidth/200);

        currentImg.left = canvas.getWidth()/2;
        currentImg.top = canvas.getHeight()/2;
        currentImg.width = 200;
        currentImg.height = newHeight;
  			currentImg.originX = 'center';
  			currentImg.originY = 'center';

  			canvas.setActiveObject(currentImg);

  			currentImg.set(clipartOptions);
  			applyClipartSettings(currentImg);

  			canvas.add(currentImg);
  			canvas.setActiveObject(currentImg);
  			//orderLayers();
  			canvas.renderAll();
  		});
  	}
  	else {
      // NON SVG INSTORE LOAD
  		var imgElement;
  		if (name.length > 10 && name.substring(0,10) == 'data:image')
  		{
  		  imgElement = new Image();
  		  imgElement.src = name;
  		}
  		else
  		{
  		  imgElement = document.getElementsByName(name)[0];
  		}

  		currentImg = new fabric.Image(imgElement, {
  		  left: canvas.getWidth()/2,
  		  top: canvas.getHeight()/2,
  		  originX: 'center',
  		  originY: 'center',
        scaleX: 6,
        scaleY: 6

  		});
  		// ctest = currentImg;
  		if (clipartOptions != null)
  			currentImg.set(clipartOptions);
  		applyClipartSettings(currentImg);

  		canvas.add(currentImg);
  		canvas.setActiveObject(currentImg);
  		//orderLayers();
  		canvas.renderAll();
  	}
    currentImg.rotatingPointOffset = 80;
  }
  //currentImg.rotatingPointOffset = 80;
  canvas.renderAll();
  clipartBoxHide();
}

function applyClipartSettings(obj)
{
  obj.setControlsVisibility({
        tl: false,
        ml: false,
        mt: false,
        bl: false,
  });

    if(outlineOptions.fill == 'black' || outlineOptions.fill == '#000000')
    {
      obj.set({
        borderColor: 'red',
        cornerColor: 'red',
        transparentCorners: true,
        cornerSize: 60,

      });
    } else {
      obj.set({
        borderColor: 'black',
        cornerColor: 'black',
        transparentCorners: true,
        cornerSize: 60

      });
    }
    obj.centeredScaling = true;
    obj['scaleXStart'] = obj.scaleX;
    obj['scaleYStart'] = obj.scaleY;
}

function deleteClipart() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (isClipart(currentObj))
  {
    var yes = confirm("Are you sure you want to delete the selected clipart?");
    if (yes)
    {
      currentObj.remove();
    }
  }
  else if (isGroup(currentGroup))
  {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].remove();
    }
    canvas.discardActiveGroup();
  }
  //orderLayers();
  canvas.renderAll();
}

//////////////////////////////////////////////////////////////////////////////////////
//  Function to flip the current object over the X axis                             //
//////////////////////////////////////////////////////////////////////////////////////
function flipXImg()
{
  var currentImg = canvas.getActiveObject();

  if (isClipart(currentImg))
  {
    currentImg.flipX = !currentImg.flipX;
    canvas.renderAll();
  }
}


//////////////////////////////////////////////////////////////////////////////////////
//  Function to flip the current object over the Y axis                             //
//////////////////////////////////////////////////////////////////////////////////////
function flipYImg()
{
  var currentImg = canvas.getActiveObject();

  if (isClipart(currentImg))
  {
    currentImg.flipY = !currentImg.flipY;
    canvas.renderAll();
  }
}

//////////////////////////////////////////////////////////////////////////////////////
//  Function to center the active object                                            //
//////////////////////////////////////////////////////////////////////////////////////
function centerObject()
{
  var currentObj = canvas.getActiveObject();
  var currGroup = canvas.getActiveGroup();
  if(currGroup) {
    currGroup.center();
    currGroup.setCoords();
    canvas.renderAll();
  } else
  {
    if (currentObj == null)
    return;

    currentObj.center();
    currentObj.setCoords();
    canvas.renderAll();
  }
}

function centerObjectH()
{
  var currentObj = canvas.getActiveObject();
  var currGroup = canvas.getActiveGroup();
  if(currGroup) {
    currGroup.centerH();
    currGroup.setCoords();
    canvas.renderAll();
  } else
  {
    if (currentObj == null)
    return;

    currentObj.centerH();
    currentObj.setCoords();
    canvas.renderAll();
  }
}

function centerObjectV()
{
  var currentObj = canvas.getActiveObject();
  var currGroup = canvas.getActiveGroup();
  if(currGroup) {
    currGroup.centerV();
    currGroup.setCoords();
    canvas.renderAll();
  } else
  {
    if (currentObj == null)
    return;

    currentObj.centerV();
    currentObj.setCoords();
    canvas.renderAll();
  }
}

var s;
function clipColor() {
    var currentObj = canvas.getActiveObject();
    if (!isSVG(currentObj))
      return;

    var colorSet = $('#clipcolor').val();
    currentObj.set('fill', colorSet);
    canvas.renderAll();

}



// slider for rotation
// function onTextAngleSliderChange()
// {
  // var angle = $('#textAngleSlider').val();
  // angle *= 1;
  // angle = Math.round((angle + 0.00001) * 100) / 100
  // $('#textAngleTextbox').val(angle);

  // var currentObj = canvas.getActiveObject();
  // if ( !(isText(currentObj) || isImg(currentObj) || isSVG(currentObj)) )
    // return;

  // currentObj.rotate(angle);
  // canvas.renderAll();
// }

//$('#textAngleSlider').on('input', onTextAngleSliderChange);
//$('#textAngleSlider').on('change', onTextAngleSliderChange);

var minAngle = 0;
var maxAngle = 359.9;

 function onTextAngleTextInput()
 {
   var angle = $('#textAngleTextbox').val();
   if (isNumber(angle) && minAngle <= angle && angle <= maxAngle)
   {
    //$('#textAngleSlider').val(angle);


     var currentObj = canvas.getActiveObject();
     if ( !(isText(currentObj) || isImg(currentObj) || isSVG(currentObj)) )
         return;

    currentObj.rotate(angle);
     canvas.renderAll();
   }
 }

function onTextAngleTextChange()
{

  var angleText = $('#textAngleTextbox');
  var angle = angleText.val();
  if (!isNumber(angle))
  {
    angleText.val(12);
  }
  else if (angle < minAngle)
  {
    angleText.val(minAngle);
  }
  else if (angle > maxAngle)
  {
    angleText.val(maxAngle);
  }
  onTextAngleTextInput();
}

$('#textAngleTextbox').on('input', onTextAngleTextInput);
$('#textAngleTextbox').on('change', onTextAngleTextChange);




//clipart
// function onClipartAngleSliderChange()
// {
  // var angle = $('#clipartAngleSlider').val();
  // angle *= 1;
  // angle = Math.round((angle + 0.00001) * 100) / 100
  // $('#clipartAngleTextbox').val(angle);

  // var currentObj = canvas.getActiveObject();
  // if ( !(isText(currentObj) || isImg(currentObj) || isSVG(currentObj)) )
    // return;

  // currentObj.rotate(angle);
  // canvas.renderAll();
// }

// $('#clipartAngleSlider').on('input', onClipartAngleSliderChange);
// $('#clipartAngleSlider').on('change', onClipartAngleSliderChange);

// var minAngle = 0;
// var maxAngle = 360;

function onClipartAngleTextInput()
{
  var angle = $('#clipartAngleTextbox').val();
  if (isNumber(angle) && minAngle <= angle && angle <= maxAngle)
  {
    //$('#clipartAngleSlider').val(angle);


    var currentObj = canvas.getActiveObject();
    if ( !(isText(currentObj) || isImg(currentObj) || isSVG(currentObj)) )
        return;

    currentObj.rotate(angle);
    canvas.renderAll();
  }
}

function onClipartAngleTextChange()
{

  var angleText = $('#clipartAngleTextbox');
  var angle = angleText.val();
  if (!isNumber(angle))
  {
    angleText.val(12);
  }
  else if (angle < minAngle)
  {
    angleText.val(minAngle);
  }
  else if (angle > maxAngle)
  {
    angleText.val(maxAngle);
  }
  onClipartAngleTextInput();
}

$('#clipartAngleTextbox').on('input', onClipartAngleTextInput);
$('#clipartAngleTextbox').on('change', onClipartAngleTextChange);


//////////////////////////////////////////////////////////////////////////////////////
//  Code for alignment lines (object-to-object alignment)                           //
//////////////////////////////////////////////////////////////////////////////////////

    initCenteringGuidelines(canvas);
    initAligningGuidelines(canvas);

// Object alignment code
function initAligningGuidelines(canvas) {

  var ctx = canvas.getSelectionContext(),
      aligningLineOffset = 5,
      aligningLineMargin = 4,
      aligningLineWidth = 1,
      aligningLineColor = 'rgb(0,255,0)';

  function drawVerticalLine(coords) {
    drawLine(
      coords.x + 0.5,
      coords.y1 > coords.y2 ? coords.y2 : coords.y1,
      coords.x + 0.5,
      coords.y2 > coords.y1 ? coords.y2 : coords.y1);
  }

  function drawHorizontalLine(coords) {
    drawLine(
      coords.x1 > coords.x2 ? coords.x2 : coords.x1,
      coords.y + 0.5,
      coords.x2 > coords.x1 ? coords.x2 : coords.x1,
      coords.y + 0.5);
  }

  function drawLine(x1, y1, x2, y2) {
    ctx.save();
    ctx.lineWidth = aligningLineWidth;
    ctx.strokeStyle = aligningLineColor;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.restore();
  }

  function isInRange(value1, value2) {
    value1 = Math.round(value1);
    value2 = Math.round(value2);
    for (var i = value1 - aligningLineMargin, len = value1 + aligningLineMargin; i <= len; i++) {
      if (i === value2) {
        return true;
      }
    }
    return false;
  }

  var verticalLines = [ ],
      horizontalLines = [ ];

  canvas.on('object:moving', function(e) {

    var activeObject = e.target,
        canvasObjects = canvas.getObjects(),
        activeObjectCenter = activeObject.getCenterPoint(),
        activeObjectLeft = activeObjectCenter.x,
        activeObjectTop = activeObjectCenter.y,
        activeObjectHeight = activeObject.getBoundingRectHeight(),
        activeObjectWidth = activeObject.getBoundingRectWidth(),
        horizontalInTheRange = false,
        verticalInTheRange = false,
        transform = canvas._currentTransform;

    if (!transform) return;

    // It should be trivial to DRY this up by encapsulating (repeating) creation of x1, x2, y1, and y2 into functions,
    // but we're not doing it here for perf. reasons -- as this a function that's invoked on every mouse move

    for (var i = canvasObjects.length; i--; ) {

      if (canvasObjects[i] === activeObject) continue;

      var objectCenter = canvasObjects[i].getCenterPoint(),
          objectLeft = objectCenter.x,
          objectTop = objectCenter.y,
          objectHeight = canvasObjects[i].getBoundingRectHeight(),
          objectWidth = canvasObjects[i].getBoundingRectWidth();

      // snap by the horizontal center line
      if (isInRange(objectLeft, activeObjectLeft)) {
        verticalInTheRange = true;
        verticalLines.push({
          x: objectLeft,
          y1: (objectTop < activeObjectTop)
            ? (objectTop - objectHeight / 2 - aligningLineOffset)
            : (objectTop + objectHeight / 2 + aligningLineOffset),
          y2: (activeObjectTop > objectTop)
            ? (activeObjectTop + activeObjectHeight / 2 + aligningLineOffset)
            : (activeObjectTop - activeObjectHeight / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(objectLeft, activeObjectTop), transform.originX, transform.originY);
      }

      // snap by the left edge
      if (isInRange(objectLeft - objectWidth / 2, activeObjectLeft - activeObjectWidth / 2)) {
        verticalInTheRange = true;
        verticalLines.push({
          x: objectLeft - objectWidth / 2,
          y1: (objectTop < activeObjectTop)
            ? (objectTop - objectHeight / 2 - aligningLineOffset)
            : (objectTop + objectHeight / 2 + aligningLineOffset),
          y2: (activeObjectTop > objectTop)
            ? (activeObjectTop + activeObjectHeight / 2 + aligningLineOffset)
            : (activeObjectTop - activeObjectHeight / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(objectLeft - objectWidth / 2 + activeObjectWidth / 2, activeObjectTop), transform.originX, transform.originY);
      }

      // snap by the right edge
      if (isInRange(objectLeft + objectWidth / 2, activeObjectLeft + activeObjectWidth / 2)) {
        verticalInTheRange = true;
        verticalLines.push({
          x: objectLeft + objectWidth / 2,
          y1: (objectTop < activeObjectTop)
            ? (objectTop - objectHeight / 2 - aligningLineOffset)
            : (objectTop + objectHeight / 2 + aligningLineOffset),
          y2: (activeObjectTop > objectTop)
            ? (activeObjectTop + activeObjectHeight / 2 + aligningLineOffset)
            : (activeObjectTop - activeObjectHeight / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(objectLeft + objectWidth / 2 - activeObjectWidth / 2, activeObjectTop), transform.originX, transform.originY);
      }

      // snap by the vertical center line
      if (isInRange(objectTop, activeObjectTop)) {
        horizontalInTheRange = true;
        horizontalLines.push({
          y: objectTop,
          x1: (objectLeft < activeObjectLeft)
            ? (objectLeft - objectWidth / 2 - aligningLineOffset)
            : (objectLeft + objectWidth / 2 + aligningLineOffset),
          x2: (activeObjectLeft > objectLeft)
            ? (activeObjectLeft + activeObjectWidth / 2 + aligningLineOffset)
            : (activeObjectLeft - activeObjectWidth / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(activeObjectLeft, objectTop), transform.originX, transform.originY);
      }

      // snap by the top edge
      if (isInRange(objectTop - objectHeight / 2, activeObjectTop - activeObjectHeight / 2)) {
        horizontalInTheRange = true;
        horizontalLines.push({
          y: objectTop - objectHeight / 2,
          x1: (objectLeft < activeObjectLeft)
            ? (objectLeft - objectWidth / 2 - aligningLineOffset)
            : (objectLeft + objectWidth / 2 + aligningLineOffset),
          x2: (activeObjectLeft > objectLeft)
            ? (activeObjectLeft + activeObjectWidth / 2 + aligningLineOffset)
            : (activeObjectLeft - activeObjectWidth / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(activeObjectLeft, objectTop - objectHeight / 2 + activeObjectHeight / 2), transform.originX, transform.originY);
      }

      // snap by the bottom edge
      if (isInRange(objectTop + objectHeight / 2, activeObjectTop + activeObjectHeight / 2)) {
        horizontalInTheRange = true;
        horizontalLines.push({
          y: objectTop + objectHeight / 2,
          x1: (objectLeft < activeObjectLeft)
            ? (objectLeft - objectWidth / 2 - aligningLineOffset)
            : (objectLeft + objectWidth / 2 + aligningLineOffset),
          x2: (activeObjectLeft > objectLeft)
            ? (activeObjectLeft + activeObjectWidth / 2 + aligningLineOffset)
            : (activeObjectLeft - activeObjectWidth / 2 - aligningLineOffset)
        });
        activeObject.setPositionByOrigin(new fabric.Point(activeObjectLeft, objectTop + objectHeight / 2 - activeObjectHeight / 2), transform.originX, transform.originY);
      }
    }

    if (!horizontalInTheRange) {
      horizontalLines.length = 0;
    }

    if (!verticalInTheRange) {
      verticalLines.length = 0;
    }
  });

  canvas.on('before:render', function() {
    canvas.clearContext(canvas.contextTop);
  });

  canvas.on('after:render', function() {
    for (var i = verticalLines.length; i--; ) {
      drawVerticalLine(verticalLines[i]);
    }
    for (var i = horizontalLines.length; i--; ) {
      drawHorizontalLine(horizontalLines[i]);
    }
  });

  canvas.on('mouse:up', function() {
    verticalLines.length = horizontalLines.length = 0;
    canvas.renderAll();
  });
}


//////////////////////////////////////////////////////////////////////////////////////
//  Code for object alignment (Object-to-center alignment)                          //
//////////////////////////////////////////////////////////////////////////////////////
function initCenteringGuidelines(canvas) {

  var canvasWidth = canvas.getWidth(),
      canvasHeight = canvas.getHeight(),
      canvasWidthCenter = canvasWidth / 2,
      canvasHeightCenter = canvasHeight / 2,
      canvasWidthCenterMap = { },
      canvasHeightCenterMap = { },
      centerLineMargin = 4,
      centerLineColor = 'rgba(255,0,241,0.5)',
      centerLineWidth = 1,
      ctx = canvas.getSelectionContext();

  for (var i = canvasWidthCenter - centerLineMargin, len = canvasWidthCenter + centerLineMargin; i <= len; i++) {
    canvasWidthCenterMap[i] = true;
  }
  for (var i = canvasHeightCenter - centerLineMargin, len = canvasHeightCenter + centerLineMargin; i <= len; i++) {
    canvasHeightCenterMap[i] = true;
  }

  function showVerticalCenterLine() {
    showCenterLine(canvasWidthCenter + 0.5, 0, canvasWidthCenter + 0.5, canvasHeight);
  }

  function showHorizontalCenterLine() {
    showCenterLine(0, canvasHeightCenter + 0.5, canvasWidth, canvasHeightCenter + 0.5);
  }

  function showCenterLine(x1, y1, x2, y2) {
    ctx.save();
    ctx.strokeStyle = centerLineColor;
    ctx.lineWidth = centerLineWidth;
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
    ctx.restore();
  }

  var afterRenderActions = [ ],
      isInVerticalCenter,
      isInHorizontalCenter;

  canvas.on('object:moving', function(e) {
    var object = e.target,
        objectCenter = object.getCenterPoint(),
        transform = canvas._currentTransform;

    var currentGroup = canvas.getActiveGroup();

    if(isGroup(currentGroup)){
      object = currentGroup;
      objectCenter = object.getCenterPoint();
      transform = canvas._currentTransform;
    }

    if (!transform) return;

    isInVerticalCenter = objectCenter.x in canvasWidthCenterMap,
    isInHorizontalCenter = objectCenter.y in canvasHeightCenterMap;

    if (isInHorizontalCenter || isInVerticalCenter) {
      object.setPositionByOrigin(new fabric.Point((isInVerticalCenter ? canvasWidthCenter : objectCenter.x), (isInHorizontalCenter ? canvasHeightCenter : objectCenter.y)), transform.originX, transform.originY);
    }
  });

  canvas.on('before:render', function() {
    canvas.clearContext(canvas.contextTop);
  });

  canvas.on('after:render', function() {
    if (isInVerticalCenter) {
      showVerticalCenterLine();
    }
    if (isInHorizontalCenter) {
      showHorizontalCenterLine();
    }
  });

  canvas.on('mouse:up', function() {
    // clear these values, to stop drawing guidelines once mouse is up
    isInVerticalCenter = isInHorizontalCenter = null;
    canvas.renderAll();
  });
}

function rotateObject() {
  var object = canvas.getActiveObject();
  if(object.getAngle() >= 0 && object.getAngle() < 90)
  {
    object.setAngle(90);
  } else if (object.getAngle() >= 90 && object.getAngle() < 180)
  {
    object.setAngle(180);
  } else if (object.getAngle() >= 180 && object.getAngle() < 270)
  {
    object.setAngle(270);
  } else
  {
    object.setAngle(0);
  }
  canvas.renderAll();
}

canvas.on('object:scaling', function(e) {
	var object = e.target;
	if (isClipart(object))
	{
		// $('#clipartScaleSlider').val(object.scaleX/object.scaleXStart);
		$('#clipartScaleTextbox').val(object.scaleX/object.scaleXStart);
	}
});

canvas.on('object:rotating', function(e) {
	var object = e.target;
	if (isClipart(object))
	{
		// $('#clipartScaleSlider').val(object.scaleX/object.scaleXStart);
		$('#clipartAngleTextbox').val(object.angle);
	}
});
