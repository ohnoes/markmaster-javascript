/////////////////////////////////////////////////////////
//        Setup canvas stuff
/////////////////////////////////////////////////////////
var stickerType = document.getElementById('stickertype').value;

var minStrokeWidth = 0.01;


var minFontSize = 1.5;
var maxFontSize = 140;

var minLineHeight = 0;
var maxLineHeight = 2;

var minClipartScale = 0.0001;
var maxClipartScale = 6;

var backImgInfo = {
  src: '',
  beingUsed: false,
  isLocal: true,
  isOutline: false,
  width: 0,
  height: 0
}
function setBackgroundImageForOutline(img){
  backImgInfo.isOutline = true;
  canvas.setBackgroundImage(img);
}

// added
// conversion ratio from pt. for font size to mm
// 1 pt = 1/72 inch.  1/72 inch = ~0.352mm
// exact after 3rd decimal apparently varies from program to program http://en.wikipedia.org/wiki/Point_(typography)
var pt2mm = 0.352;
var mm2px = 1;

var canvas = this.__canvas = new fabric.Canvas('myCanvas', {width: $('#myCanvas').prop('width'), height: $('#myCanvas').prop('height')});
setInterval(function(){canvas.calcOffset();}, 5000);
var currentOutline;
var outlinePath;
// outlinePath style and other options



var outlinePathOptions;
if (stickerType == 6 || stickerType == 7) {
    outlinePathOptions = {
      visible: true,
      selectable: false,
      fill: "#f1f1f1",
      originX: 'center',
      originY: 'center',
      opacity: 0.8,
    };
} else {
    outlinePathOptions = {
      visible: true,
      selectable: false,
      fill: "#f1f1f1",
      originX: 'center',
      originY: 'center',
      opacity: 0.8,
    };
}
var outlinePathWhiteOptions = {
  visible: true,
  selectable: false,
  fill: "#f1f1f1",
  originX: 'center',
  originY: 'center',
  opacity: 1,

};


var rectOutlineInfo = new fabric.Rect({
  left: 150,
  top: 120,
  originX: 'center',
  originY: 'center',
  width: 150,
  height: 120,
  angle: 0,
  fill: 'rgba(255,255,255,1)',
  transparentCorners: false,
  selectable: false
});

var customOutlineInfo = {
left: 0,
top: 0,
width: canvas.width,
height: canvas.height,
originX: 'left',
originY: 'top',
};

var outlineOptions = {
  visible: true,
  selectable: false,
  fill: "#FFFFFF",
  originX: 'center',
  originY: 'center',
  opacity: 1,
  stroke: true,
  strokeStyle: 'black'
}

// set all variables for a rectangle path

var ellipseOutlineInfo = new fabric.Ellipse({
  left: 0,
  top: 0,
  originX: 'center',
  originY: 'center',
  rx: 150,
  ry: 120,
  fill: 'rgba(255,255,255,1)',
  transparentCorners: false,
  selectable: false
});

currentOutline = rectOutlineInfo;

/////////////////////////////////////////////////////////
//        tab   1 - size and shape
/////////////////////////////////////////////////////////

var outlinePadding = 40; // how much to pad largest dimension: width/height

if (stickerType == 9) {
  maxHeight = 50;
  maxWidth = 50;
}

function resizeOutlineWithRatio(inputElement){
  if (currentOutline == customOutlineInfo){
    if (inputElement == OutlineInput.getWidthInput()){
      OutlineInput.matchRatioForWidth(Outliner.getRatio());
    }
    else if (inputElement == OutlineInput.getHeightInput()){
      OutlineInput.matchRatioForHeight(Outliner.getRatio());
    }
  }

  resizeOutline();
}

// gets width and height from textbox, then resizes all outlines with padding using width/height ratio
function resizeOutline() {
  // get width and height from textbox
  OutlineInput.sanitizeInputs();
  setPrice();

  var width = OutlineInput.getWidth();
  var height = OutlineInput.getHeight();


  var ratio = width / height;

  // canvas width, height, center
  var cWidth = canvas.getWidth();
  var cHeight = canvas.getHeight();
  var centerX = cWidth / 2;
  var centerY = cHeight / 2;

  // aspect width/height
  var aWidth;
  var aHeight;

  // pads dimension with higher value, then calculates other
  // ex: if width is more than height, then pad width and calculate height, else vice versa
  var cornerRadius = 2;
  if ((width/2) > height)
  {
    aWidth = cWidth - 2*outlinePadding;
    aHeight = aWidth / ratio;
    mm2px = aWidth / width;
  }
  else
  {
    aHeight = cHeight - 2*outlinePadding;
    aWidth = ratio * aHeight;
    mm2px = aHeight/height;
  }

  cornerRadius *= mm2px;

  // set width and height of outlines and center them
  rectOutlineInfo.width = aWidth;
  rectOutlineInfo.height = aHeight;
  rectOutlineInfo.left = centerX;
  rectOutlineInfo.top = centerY;
  rectOutlineInfo.rx = cornerRadius;
  rectOutlineInfo.ry = cornerRadius;



  ellipseOutlineInfo.rx = aWidth/2;
  ellipseOutlineInfo.ry = aHeight/2;
  ellipseOutlineInfo.left = centerX;
  ellipseOutlineInfo.top = centerY;

  updateTextSize();
  canvas.renderAll();
}


// creates and returns non-sticker area based on rect outline
// gray area
function getRectClipPath() {
  var rWidth = rectOutlineInfo.width;
  var rHeight = rectOutlineInfo.height;

  // setBarWidth(rWidth, $('#width').val());
  // setBarHeight(rHeight, $('#height').val());


  var width = canvas.getWidth();
  var height = canvas.getHeight();

  var cRadius = rectOutlineInfo.rx;

  var top = 0;
  var left = 0;
  var bottom = top+height;
  var right = left+width;

  var centerX = (left+right)/2;
  var centerY = (top+bottom)/2;

  var rTop = centerY -rHeight/2;
  var rLeft = centerX - rWidth/2;
  var rBottom = centerY +rHeight/2;
  var rRight = centerX + rWidth/2;

  var cTop = rTop + cRadius;
  var cLeft = rLeft + cRadius;
  var cBottom = rBottom - cRadius;
  var cRight = rRight - cRadius;

  var ePath, rectOutline;
  if (stickerType == 7) {
	 	var borderWidth = mm2px * 1.5;
		cTop = rTop + borderWidth;
		cLeft = rLeft + borderWidth;
		cBottom = rBottom - borderWidth;
		cRight = rRight - borderWidth;
		//in rectCutoff change cLeft, cRight, cTop, and cBottom
		setBackgroundImageForOutline(rectOutline);
		var rectCutoff = new fabric.Path(
		'M' + rLeft    + ' ' + rTop     +
		'L' + centerX + ' ' + rTop     +
		'L' + centerX + ' ' + cTop    +
		'L' + cLeft   + ' ' + cTop    +
		'L' + cLeft   + ' ' + cBottom +
		'L' + cRight  + ' ' + cBottom +
		'L' + cRight  + ' ' + cTop    +
		'L' + centerX + ' ' + cTop    +
		'L' + centerX + ' ' + rTop     +
		'L' + rRight   + ' ' + rTop     +
		'L' + rRight   + ' ' + rBottom  +
		'L' + rLeft + ' ' + rBottom  +
		'Z', outlinePathWhiteOptions);
		rectCutoff.setFill('#FFFFFF');

		ePath = new fabric.Path(
		'M' + left    + ' ' + top     +
		'L' + centerX + ' ' + top     +
		'L' + centerX + ' ' + rTop    +
		'L' + rLeft   + ' ' + rTop    +
		'L' + rLeft   + ' ' + rBottom +
		'L' + rRight  + ' ' + rBottom +
		'L' + rRight  + ' ' + rTop    +
		'L' + centerX + ' ' + rTop    +
		'L' + centerX + ' ' + top     +
		'L' + right   + ' ' + top     +
		'L' + right   + ' ' + bottom  +
		'L' + left + ' ' + bottom  +
		'Z', outlinePathOptions);


	  rectOutline = new fabric.Path(
		'M' + centerX + ' ' + rTop    +
		'L' + rLeft   + ' ' + rTop    +
		'L' + rLeft   + ' ' + rBottom +
		'L' + rRight  + ' ' + rBottom +
		'L' + rRight  + ' ' + rTop    +
		'L' + centerX + ' ' + rTop    +
		'Z', outlineOptions);
    setBackgroundImageForOutline(rectOutline);

		var group = new fabric.Group([ rectCutoff, ePath ], { left: 0, top: 0 });
		return group;
  } else {

		if(stickerType == 6) {
			var borderWidth = mm2px * 1.5;
			cTop = rTop + borderWidth;
			cLeft = rLeft + borderWidth;
			cBottom = rBottom - borderWidth;
			cRight = rRight - borderWidth;
			//in rectCutoff change cLeft, cRight, cTop, and cBottom
			setBackgroundImageForOutline(rectOutline);
			var rectCutoff = new fabric.Path(
			'M' + rLeft    + ' ' + rTop     +
			'L' + centerX + ' ' + rTop     +
			'L' + centerX + ' ' + cTop    +
			'L' + cLeft   + ' ' + cTop    +
			'L' + cLeft   + ' ' + cBottom +
			'L' + cRight  + ' ' + cBottom +
			'L' + cRight  + ' ' + cTop    +
			'L' + centerX + ' ' + cTop    +
			'L' + centerX + ' ' + rTop     +
			'L' + rRight   + ' ' + rTop     +
			'L' + rRight   + ' ' + rBottom  +
			'L' + rLeft + ' ' + rBottom  +
			'Z', outlinePathWhiteOptions);
			rectCutoff.setFill('#FFFFFF');

		}

	  ePath = new fabric.Path(
		'M' + left    + ' ' + top     +
		'L' + centerX + ' ' + top     +
		'L' + centerX + ' ' + rTop    +
		'L' + cLeft   + ' ' + rTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rLeft   + ' ' + cTop +
		'L' + rLeft   + ' ' + cBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cLeft   + ' ' + rBottom +
		'L' + cRight  + ' ' + rBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rRight   + ' ' + cBottom +
		'L' + rRight  + ' ' + cTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cRight   + ' ' + rTop +
		'L' + centerX + ' ' + rTop    +
		'L' + centerX + ' ' + top     +
		'L' + right   + ' ' + top     +
		'L' + right   + ' ' + bottom  +
		'L' + left + ' ' + bottom  +
		'Z', outlinePathOptions);


	  rectOutline = new fabric.Path(
		'M' + centerX + ' ' + rTop    +
		'L' + cLeft   + ' ' + rTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rLeft   + ' ' + cTop +
		'L' + rLeft   + ' ' + cBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cLeft   + ' ' + rBottom +
		'L' + cRight  + ' ' + rBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rRight   + ' ' + cBottom +
		'L' + rRight  + ' ' + cTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cRight   + ' ' + rTop +
		'L' + centerX + ' ' + rTop    +
		'Z', outlineOptions);
  }
	setBackgroundImageForOutline(rectOutline);

	if(stickerType == 6) {
		var group = new fabric.Group([ rectCutoff, ePath ], { left: 0, top: 0 });
		return group;
	}

  return ePath;
}

// creates and returns non-sticker area based on ellipse outline
// gray outline
function getEllipseClipPath() {
  var rx = ellipseOutlineInfo.rx;
  var ry = ellipseOutlineInfo.ry;

  // setBarWidth(rx*2, $('#width').val());
  // setBarHeight(ry*2, $('#height').val());

  var width = canvas.getWidth();
  var height = canvas.getHeight();

  var top = 0;
  var left = 0;
  var bottom = top+height;
  var right = left+width;

  var centerX = (left+right)/2;
  var centerY = (top+bottom)/2;

  var eTop = centerY - ry;
  var eLeft = centerX - rx;
  var eBottom = centerY + ry;
  var eRight = centerX + rx;

  var ePath = new fabric.Path(
    'M' + left    + ' ' + top     +
    'L' + centerX + ' ' + '0'     +
    'L' + centerX + ' ' + eTop    +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eLeft   + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eBottom +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eRight  + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eTop +
    'L' + centerX + ' ' + '0'     +
    'L' + right   + ' ' + '0'     +
    'L' + right   + ' ' + bottom  +
    'L' + left + ' ' + bottom  +
    'Z', outlinePathOptions);

  var ellipseOutline = new fabric.Path(
    'M' + centerX + ' ' + eTop    +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eLeft   + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eBottom +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eRight  + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eTop +
    'Z', outlineOptions);
  setBackgroundImageForOutline(ellipseOutline);

  return ePath;
}

// updates greyed-out non-sticker area to match outline
function updateOutlinePath() {
    if (currentOutline == rectOutlineInfo)
    {
      outlinePath = getRectClipPath();
    }
    else if (currentOutline == ellipseOutlineInfo)
    {
      outlinePath = getEllipseClipPath();
    }
    else if (currentOutline == customOutlineInfo) // custom outline overlay is drawn elsewhere. no need to draw an overlay here also
    {
      Outliner.setBackgroundColor(outlineOptions.fill);
      return;
    }

    var overlay = outlinePath.cloneAsImage();
    canvas.setOverlayImage(overlay);
    canvas.renderAll();
}

function PixelsDecorator(pixels){
  var container = {};

  container.pixels = pixels;
  Object.defineProperty(container, 'width', { get: function() { return this.pixels.width; } });
  Object.defineProperty(container, 'height', { get: function() { return this.pixels.height; } });
  Object.defineProperty(container, 'data', { get: function() { return this.pixels.data; } });

  const valuesPerPixel = 4;
  const alphaOffset = 3;

  container.getAlphaIndex = function(x, y){
    return (this.width * y + x) * valuesPerPixel + alphaOffset;
  }

  container.getAlpha = function(x, y){
    var alphaIndex = this.getAlphaIndex(x, y);
    return this.data[alphaIndex];
  }

  container.setAlpha = function(x, y, alpha){
    var alphaIndex = this.getAlphaIndex(x, y);
    this.data[alphaIndex] = alpha;
  }

  container.hasAlpha = function(x, y){
    return this.getAlpha(x,y) > Outliner.options.alphaThreshold;
  }

  return container;
}

function ClusterFinder(pixelsDecorator) {
  const hasAlpha = -1;
  const empty = 0;
  const found = 1;

  var connector = {};
  connector.pixelsDecorator = pixelsDecorator;
  var width = pixelsDecorator.width;
  var height = pixelsDecorator.height;

  connector.getClusters = function(){
    var clusters = [];
    var grid = this.createArray(width, height);

    for (var x = 0; x < width; x++) {
      for (var y = 0; y < height; y++) {
        if (this.pixelsDecorator.hasAlpha(x, y)) {
          grid[x][y] = hasAlpha;
        }
        else {
          grid[x][y] = empty;
        }
      }
    }

    var clusterCount = 0;
    for (var x = 0; x < width; x++){
      for (var y = 0; y < height; y++) {
        if (grid[x][y] == empty || grid[x][y] == found){
          continue;
        }

        if (grid[x][y] == hasAlpha) {
          clusters.push(this.getCluster(grid, x, y));
        }
      }
    }

    return clusters;
  };

  connector.getCluster = function(grid, x, y){
    var points = [];
    var nextPoints = [[x,y]];
    while (nextPoints.length != 0){
      var point = nextPoints.pop();
      x = point[0];
      y = point[1];

      // out of bounds
      if (x < 0 || x >= width || y < 0 || y >= height){
        continue;
      }

      if (grid[x][y] == hasAlpha){
        grid[x][y] = found;
        points.push(point);
        nextPoints.push([x-1,y-1]);
        nextPoints.push([x-1,y]);
        nextPoints.push([x-1,y+1]);
        nextPoints.push([x,y-1]);
        nextPoints.push([x,y+1]);
        nextPoints.push([x+1,y-1]);
        nextPoints.push([x+1,y]);
        nextPoints.push([x+1,y+1]);
      }
    }

    var bounds = this.getBounds(points);
    return {
      bounds: bounds,
      center: bounds.centerPoint,
      points: points
    };
  };

  connector.getBounds = function(points){
    var bounds = {
      minX : 10000000,
      maxX : -10000000,
      minY : 10000000,
      maxY : -10000000,
    }
    for (var point of points){
      bounds.minX = Math.min(point[0], bounds.minX);
      bounds.maxX = Math.max(point[0], bounds.maxX);
      bounds.minY = Math.min(point[1], bounds.minY);
      bounds.maxY = Math.max(point[1], bounds.maxY);
    }
    var centerX = (bounds.minX + bounds.maxX) / 2;
    var centerY = (bounds.minY + bounds.maxY) / 2;
    bounds.centerPoint = [centerX, centerY];
    return bounds;
  };

  connector.createArray = function(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
      var args = Array.prototype.slice.call(arguments, 1);
      while(i--) arr[length-1 - i] = this.createArray.apply(this, args);
    }

    return arr;
  };

  return connector;
}

var OutlineInput = {
  minWidth: 6,
  maxWidth: 300,
  minHeight: 6,
  maxHeight: 300,

  getWidthInput: function() {
    return document.getElementById('width');
  },
  getHeightInput: function() {
    return document.getElementById('height');
  },

  getWidth: function() {
    return this.validateWidth(this.getWidthInput().value);
  },
  getHeight: function() {
    return this.validateHeight(this.getHeightInput().value);
  },

  setWidth: function(width) {
    this.getWidthInput().value = this.validateWidth(width);
  },
  setHeight: function(height) {
    this.getHeightInput().value = this.validateWidth(height);
  },

  matchRatioForWidth: function(ratio) {
    var mmWidth = OutlineInput.getWidth();
    var mmHeight = mmWidth / ratio;

    // TODO - consider issues from rounding

    if (mmHeight < this.minHeight) {
      this.setHeight(this.minHeight);
      this.matchRatioForHeightWithoutCorrection(ratio);
    }
    else if (mmHeight > this.maxHeight) {
      this.setHeight(this.maxHeight);
      this.matchRatioForHeightWithoutCorrection(ratio);
    }
    else{
      OutlineInput.setHeight(mmHeight);
    }
  },

  matchRatioForWidthWithoutCorrection: function(ratio) {
    var mmWidth = OutlineInput.getWidth();
    var mmHeight = mmWidth / ratio;
    OutlineInput.setHeight(mmHeight);
  },

  matchRatioForHeight: function(ratio) {
    var mmHeight = OutlineInput.getHeight();
    var mmWidth = mmHeight * ratio;

    if (mmWidth < this.minWidth) {
      this.setWidth(this.minWidth);
      this.matchRatioForWidthWithoutCorrection(ratio);
    }
    else if (mmWidth > this.maxWidth) {
      this.setWidth(this.maxWidth);
      this.matchRatioForWidthWithoutCorrection(ratio);
    }
    else {
      OutlineInput.setWidth(mmWidth);
    }
  },

  matchRatioForHeightWithoutCorrection: function(ratio) {
    var mmHeight = OutlineInput.getHeight();
    var mmWidth = mmHeight * ratio;
    OutlineInput.setWidth(mmWidth);
  },

  sanitizeInputs: function(){
    this.setWidth(this.getWidth());
    this.setHeight(this.getHeight());
  },

  // clamps number between min/max values
  // takes number, checks that it is within min/max range
  // if it is, return the number
  // if not, return closest boundary
  validateRange: function(numValue, min, max) {
    if (numValue == '')
      return min;
    numValue = parseInt(numValue);
    if (numValue < min)
      return min;
    if (numValue > max)
      return max;
    return numValue;
  },

  validateWidth: function(numValue, min, max){
    return this.validateRange(numValue, this.minWidth, this.maxWidth);
  },

  validateHeight: function(numValue, min, max){
    return this.validateRange(numValue, this.minHeight, this.maxHeight);
  }
};

var Outliner = {
  boundingBoxScaleImg: document.createElement("img"),
  cleanedCanvasImg: document.createElement("img"),
  clusteredImg: document.createElement("img"),
  stickerEffectImg: document.createElement("img"),
  paperImg: document.createElement("img"),
  outlinePathImg: document.createElement("img"),
  paperOutlineOverlayImg: document.createElement("img"),

  // TODO - find appropriate values
  options: {
    thickness: 8, // TODO - consider scaling thickness depending on the physical size of the sticker
    textThickness: 20,
    blurAmount: 4, // anything less than 3 tends to make the outline blocky when thickness is set to high values
    overlayRGBA: [241, 241, 241, 200],
    pathTolerance: 1,
    alphaThreshold: 10,
    smoothIterations: 3,
    clusterIterations: 3,
    scalingThreshold: 0.05,
    bgStrokeWidth: 10, // used to help ensure the cutline is covered by the bg color and that the image is cropped correctly anything less than 4 is not recommended
  },

  // state used to fit canvas to largest group of pixels
  largestCluster: null,
  finishedScaling: false,

  isText: function(obj) {
    return obj.type == "i-text" || obj.type == "text";
  },

  initialize: function(loadedJson) {
    //this.path = new paper.Path(loadedJson.outlineInfo.path._segments);
    this.newPaper();
    this.path = new paper.Path({
    segments:loadedJson.outlineInfo.path[1].segments,
    fillColor: loadedJson.outlineInfo.path[1].fillColor,
    closed: true
    })
    this.setBackgroundColor(loadedJson.outlineInfo.fill);

  },

  getMaxThickness: function(){
    var textCount = 0;
    var otherCount = 0;
    var self = this;
    canvas.forEachObject(function(obj){
      if (self.isText(obj)){
        textCount++;
      }
      else {
        otherCount++;
      }
    });

    if (textCount == 0){
      return this.options.thickness;
    }
    else if (otherCount == 0) {
      return this.options.textThickness;
    }
    else {
      return Math.max(this.options.thickness, this.options.textThickness);
    }
  },

  debug: {
    getInfo: function(){
      canvas.overlayImage=null;
      var info = ""
      +"var imgData = \"" + this.getImageData() + "\";\n"
      +"var outlineInfo = JSON.parse('" + this.getPathInfoJson() + "');";
      return info;
    },

    getImageData: function(){
      var padding = Outliner.options.bgStrokeWidth;
      var imgData = canvas.toDataURL({
        format: 'png',
        multiplier: 1,
        width: Outliner.pathInfo.bounds.width + 2 * padding,
        height: Outliner.pathInfo.bounds.height + 2 * padding,
        left: Outliner.pathInfo.bounds.left - padding,
        top: Outliner.pathInfo.bounds.top - padding
      });
      return imgData;
    },

    getPathInfoJson: function(){
      var json = JSON.stringify(Outliner.pathInfo)
      return json;
    }
  },

  drawPaper: function(stickerEffectCanvas) {
    var points = Tracer.getPoints(stickerEffectCanvas, this.largestCluster);
    var paperCanvas = this.newPaper();

    var path = new paper.Path();
    path.closed = true;

    path.moveTo(new paper.Point(points[0]));
    for (var point of points) {
      var paperPoint = new paper.Point(point);
      path.lineTo(paperPoint);
    }

    path.lineTo(points[0]);

    path.simplify(this.options.pathTolerance);
    this.path = path;
    this.pathInfo = this.getCutlinePaths(path);
    this.pathInfo.path = path;

    var self = this;
    this.setBackgroundColor(outlineOptions.fill);
    drawOutlinePath();

    function drawOutlinePath() {
      path.strokeColor = 'black';
      path.fillColor = null;
      paper.view.draw();
      fixScale(function(scaledCanvas){
        self.outlinePathImg.onload = function() {
          drawFilledOutline();
        }
        self.outlinePathImg.src = scaledCanvas.toDataURL();
      });
    }

    function drawFilledOutline() {
      path.strokeColor = 'black';
      path.fillColor = 'red';
      paper.view.draw();
      fixScale(function(scaledCanvas){
        self.paperImg.src = scaledCanvas.toDataURL();
      });
    }

    function fixScale(callback) {
      var paperImg = document.createElement("img");
      paperImg.onload = function() {
        var zoomCanvas = document.createElement("canvas");
        var zoomCtx = zoomCanvas.getContext("2d");
        zoomCanvas.width = stickerEffectCanvas.width;
        zoomCanvas.height = stickerEffectCanvas.height;

        zoomCtx.drawImage(paperImg, 0, 0, zoomCanvas.width, zoomCanvas.height);
        callback(zoomCanvas);
      }
      paperImg.src = paperCanvas.toDataURL();
    }
  },

  newPaper: function() {
    var paperCanvas = document.createElement("canvas");
    paperCanvas.width = canvas.width;
    paperCanvas.height = canvas.height;
    paper.setup(paperCanvas);
    return paperCanvas;
  },

  setBackgroundColor: function(bgColor){
    // guard clause - path not generated yet
    if (!this.path){
      return;
    }

    var svgElement = this.path.exportSVG();
    var fabricPath = new fabric.Path(svgElement.getAttribute('d'));
    var strokeWidth = this.options.bgStrokeWidth;
    fabricPath.set({fill: bgColor, stroke: bgColor, strokeWidth: strokeWidth});
    fabricPath.left -= strokeWidth / 2;
    fabricPath.top -= strokeWidth / 2;
    canvas.setBackgroundImage(fabricPath);
  },

  // usage: pdf.lines(result.lines, result.x, result.y, [1,1]) // where [1,1] is the x,y scale
  getCutlinePaths: function createCurves(path){
    var self = this;
    var segments = path._segments,
    length = segments.length,
    coords = new Array(6),
    first = true,
    curX, curY,
    prevX, prevY,
    inX, inY,
    outX, outY,
    parts = [];

    function addSegment(segment) {
      segment._transformCoordinates(null, coords);
      curX = coords[0];
      curY = coords[1];
      if (first) {
        parts.push([curX, curY]);
        first = false;
      } else {
        inX = coords[2];
        inY = coords[3];
        parts.push([outX - prevX, outY - prevY,
          inX - prevX,  inY - prevY,
          curX - prevX, curY - prevY]);
        }
        prevX = curX;
        prevY = curY;
        outX = coords[4];
        outY = coords[5];
      }

      if (!length)
      return [];

      for (var i = 0; i < length; i++)
      addSegment(segments[i]);
      if (path._closed && length > 0) {
        addSegment(segments[0]);
      }
      return {padding: self.options.bgStrokeWidth,
              x: parts[0][0],
              y: parts[0][1],
              curves: parts.slice(1),
              bounds: {left: path.bounds.left,
                       right: path.bounds.right,
                       top: path.bounds.top,
                       bottom: path.bounds.bottom,
                       width: path.bounds.width,
                       height: path.bounds.height}
             }
  },

  getFillStyle: function(){
    var r = this.options.overlayRGBA[0];
    var g = this.options.overlayRGBA[1];
    var b = this.options.overlayRGBA[2];
    var a = this.options.overlayRGBA[3]/255;
    return "rgba(" + r + "," + g + "," + b + "," + a + ")";
  },

  generateCutline: function(){
    // do nothing if canvas is empty
    if (canvas.getObjects().length == 0) {
      hidePopup();
      return;
    }

    currentOutline = customOutlineInfo;
    this.finishedScaling = false;
    this.largestCluster = null;

    if (backImgInfo.isOutline){
      setBackgroundImageForOutline(null);
    }

    // centering is inconsistent, though repeating the center function a few times it seems to settle
    for (var i = 0; i < 8; i++){
      this.centerCanvasObjectsByBoundingBox();
    }
    this.scaleCanvasAndGenerateOutline();
  },

  scaleCanvasAndGenerateOutline: function(){
    var self = this;
    self.onCleanCanvas(function(cleanCanvas)
    {
      // center canvas objects
      var cleanedImage = new Image();
      cleanedImage.onload = function(e){
        var scale = self.centerCanvasObjectsByAlpha(cleanedImage);
        if (1 - self.options.scalingThreshold < scale && scale < 1 + self.options.scalingThreshold) {
          self.finishedScaling = true;
        }
        canvas.renderAll();
        self.cleanCanvasAndGenerateOutline();
      };
      self.getCanvasImage(cleanCanvas, function(canvasImg){
        cleanedImage.src = canvasImg;
      });
    });
  },

    cleanCanvasAndGenerateOutline: function(){
      var self = this;
      this.onCleanCanvas(function(cleanCanvas){
        var cleanedAndCenteredImage = new Image();
        cleanedAndCenteredImage.onload = function(e){
          self.createAndApplyOutline();
        };
        self.getCanvasImage(cleanCanvas, function(canvasImg){
          cleanedAndCenteredImage.src = canvasImg;
        });
      });
    },

    centerCanvasObjectsByAlpha: function(cleanImage){
      var canvasContext = this.createCanvasContext(cleanImage);
      var c = canvasContext.canvas;
      var ctx = canvasContext.context;
      var pixelsDecorator = new PixelsDecorator(ctx.getImageData(0, 0, c.width, c.height));

      var bounds = {
        minX: c.width,
        maxX: 0,
        minY: c.height,
        maxY: 0,
      };

      if (!this.largestCluster){
        for (var x = 0; x < pixelsDecorator.width; x++){
          for (var y = 0; y < pixelsDecorator.height; y++){
            if (pixelsDecorator.hasAlpha(x, y)){
              bounds.minX = Math.min(x, bounds.minX);
              bounds.maxX = Math.max(x, bounds.maxX);
              bounds.minY = Math.min(y, bounds.minY);
              bounds.maxY = Math.max(y, bounds.maxY);
            }
          }
        }
      }
      else {
        bounds = this.largestCluster.bounds;
      }

      this.centerCanvasObjectsWithBounds(bounds);

      var margin = 10;
      var thickness = this.getMaxThickness();
      var canvasWidth = canvas.width - 2 * (thickness + margin);
      var canvasHeight = canvas.height - 2 * (thickness + margin);
      return this.scaleObjectsAroundCenter(bounds, canvasWidth, canvasHeight, 1);
    },

    centerCanvasObjectsByBoundingBox: function(){
      var bounds = {
        minX: 1000000,
        maxX: -1000000,
        minY: 1000000,
        maxY: -1000000,
      };
      canvas.forEachObject(function(obj){
        var rect = obj.getBoundingRect();
        bounds.minX = Math.min(bounds.minX, rect.left);
        bounds.maxX = Math.max(bounds.maxX, rect.left + rect.width);
        bounds.minY = Math.min(bounds.minY, rect.top);
        bounds.maxY = Math.max(bounds.maxY, rect.top + rect.height);
      });

      this.centerCanvasObjectsWithBounds(bounds);

      // scale to fit in canvas
      var margin = 10;
      var canvasWidth = canvas.width - 2 * (margin + this.getMaxThickness());
      var canvasHeight = canvas.height - 2 * (margin + this.getMaxThickness());
      this.scaleObjectsAroundCenter(bounds, canvasWidth, canvasHeight, 0.5);

      // ------ for debugging
          var self = this;
          this.onCleanCanvas(function(cleanCanvas){
            self.boundingBoxScaleImg.src = cleanCanvas.toDataURL();
          });
      // ------ for debugging
    },

    centerCanvasObjectsWithBounds: function(bounds){
      var centerX = (bounds.minX + bounds.maxX) / 2;
      var centerY = (bounds.minY + bounds.maxY) / 2;

      var canvasCenterX = canvas.width / 2;
      var canvasCenterY = canvas.height / 2;

      var offsetX = canvasCenterX - centerX;
      var offsetY = canvasCenterY - centerY;

      canvas.forEachObject(function(obj){
        obj.left += offsetX;
        obj.top += offsetY;
      })
    },

    scaleObjectsAroundCenter: function(bounds, fitToWidth, fitToHeight, scaleFactor){
      bounds.width = bounds.maxX - bounds.minX;
      bounds.height = bounds.maxY - bounds.minY;

      var scaleX = fitToWidth / bounds.width;
      var scaleY = fitToHeight / bounds.height;
      var ratio = bounds.width / bounds.height;
      var canvasRatio = fitToWidth / fitToHeight;
      var scale = ratio > canvasRatio ? scaleX : scaleY;
      scale *= scaleFactor;

      var canvasCenterX = canvas.width / 2;
      var canvasCenterY = canvas.height / 2;

      canvas.forEachObject(function(obj){
        var offsetX = canvasCenterX - obj.left;
        var offsetY = canvasCenterY - obj.top;

        obj.left = canvasCenterX - scale * offsetX;
        obj.top = canvasCenterY - scale * offsetY;

        obj.setScaleX(obj.scaleX * scale);
        obj.setScaleY(obj.scaleY * scale);
        obj.setCoords();
      });

      return scale;
    },

    createAndApplyOutline: function(){
      var self = this;

      self.prepareImageElementsForOutline();

      self.onCleanCanvas(function(cleanCanvas)
      {
        // TODO - remove for production - for testing only
        //$('.row.bottom-row').before(Outliner.paperOutlineOverlayImg);
        //$('.row.bottom-row').before(Outliner.outlinePathImg);
        //$('.row.bottom-row').before(Outliner.paperImg);
        //$('.row.bottom-row').before(Outliner.stickerEffectImg);
        //$('.row.bottom-row').before(Outliner.clusteredImg);
        //$('.row.bottom-row').before(Outliner.cleanedCanvasImg);
        //$('.row.bottom-row').before(Outliner.boundingBoxScaleImg);

        self.getCanvasImage(cleanCanvas, function(canvasImg){
          self.cleanedCanvasImg.src = canvasImg;
        });
      });
    },

    onCleanCanvas: function(callback){
      canvas.clone(function(canvasClone)
      {
        canvasClone.setBackgroundColor(null);
        canvasClone.setBackgroundImage(null);
        canvasClone.setOverlayColor(null);
        canvasClone.setOverlayImage(null);
        callback(canvasClone);
      });
    },

    getCanvasImage: function(canvasClone, callback){
      // TODO: consider https://developer.apple.com/library/content/documentation/AudioVideo/Conceptual/HTML-canvas-guide/SettingUptheCanvas/SettingUptheCanvas.html
      var dataUrlOptions = {format: 'png', multiplier: 1};

      // fabric automatically scales images of the canvas based on page zoom, device dpi, etc.
      // the below attempts to force the image to a certain side to fix issues encountered when the page is zoomed in/out

      var img = document.createElement("img");
      img.onload = function() {
        var zoomCanvas = document.createElement("canvas");
        var zoomCtx = zoomCanvas.getContext("2d");
        zoomCanvas.width = canvasClone.width;
        zoomCanvas.height = canvasClone.height;

        var scale = 1 ;// paper.view.pixelRatio;
        zoomCtx.scale(scale, scale);
        zoomCtx.drawImage(img, 0, 0, zoomCanvas.width, zoomCanvas.height);
        callback(zoomCanvas.toDataURL());
      }
      img.src = canvasClone.toDataURL();
    },

    prepareImageElementsForOutline: function(){
      var self = this;

      self.cleanedCanvasImg.onload = function() {
        self.applyStickerEffectForTextAndOther(function(canvas){
          self.stickerEffectImg.src = canvas.toDataURL();
        });
      };

      self.stickerEffectImg.onload = function() {
        var clusteredCanvas = self.connectClusters(self.stickerEffectImg);
        self.clusteredImg.src = clusteredCanvas.toDataURL();
      };

      self.clusteredImg.onload = function() {
        if (self.finishedScaling) {
          var stickerEffectCanvas = self.createCanvasContext(self.clusteredImg).canvas;
          self.drawPaper(stickerEffectCanvas);
        }
        else {
          self.scaleCanvasAndGenerateOutline();
        }
      };

      self.paperImg.onload = function() {
        var outlineOverlayCanvas = self.createOutlineOverlay(self.paperImg, self.outlinePathImg);
        self.paperOutlineOverlayImg.src = outlineOverlayCanvas.toDataURL();
      };

      self.paperOutlineOverlayImg.onload = function() {
        var outlineOverlayImg = new fabric.Image(self.paperOutlineOverlayImg);
        canvas.setOverlayImage(outlineOverlayImg);
        canvas.renderAll();
        hidePopup();
        OutlineInput.matchRatioForWidth(self.getRatio());
        resizeOutline();
      };
    },

    getRatio: function(){
      return this.pathInfo.bounds.width / this.pathInfo.bounds.height;
    },

    createCanvasContext: function(img){
      var c = document.createElement('canvas');
      c.width = img.width;
      c.height = img.height;
      var ctx = c.getContext('2d');
      ctx.drawImage(img, 0, 0);
      return {canvas: c, context: ctx};
    },

    createOutlineOverlay: function(outlineImage, outlinePathImg) {
      var canvasContext = this.createCanvasContext(outlineImage);
      var c = canvasContext.canvas;
      var ctx = canvasContext.context;

      var pixels = ctx.getImageData(0, 0, c.width, c.height);
      var filteredImgData = this.getOutlineImage(pixels);

      ctx.fillStyle = this.getFillStyle();
      ctx.fillRect(0, 0, c.width, c.height);
      ctx.putImageData(filteredImgData, 0, 0);
      ctx.drawImage(outlinePathImg, 0, 0);

      return c;
    },

    connectClusters: function(cleanedCanvasImg){
      var canvasContext = this.createCanvasContext(cleanedCanvasImg);
      var c = canvasContext.canvas;
      var ctx = canvasContext.context;


      var pixelsDecorator = new PixelsDecorator(ctx.getImageData(0, 0, c.width, c.height));
      var clusterFinder = new ClusterFinder(pixelsDecorator);
      var clusters = clusterFinder.getClusters();

      // get biggest cluster
      this.largestCluster = null;
      for (var cluster of clusters) {
        if (!this.largestCluster){
          this.largestCluster = cluster;
          continue;
        }

        if (cluster.points.length > this.largestCluster.points.length) {
          this.largestCluster = cluster;
        }
      }

      // TODO - commented out for initial release. will eventually put back in
      // for (var cluster1 of clusters){
      //   for (var cluster2 of clusters){
      //     var pair1 = this.getClusterPair(ctx, cluster1, cluster2, true);
      //     var pair2 = this.getClusterPair(ctx, cluster1, cluster2, false);
      //     this.drawLine(ctx, pair1[0], pair1[1]);
      //     this.drawLine(ctx, pair2[0], pair2[1]);
      //   }
      // }

      return c;
    },

    getClusterPair: function(ctx, cluster1, cluster2, above){
      var center1 = cluster1.center;
      var center2 = cluster2.center;

      var pair = [center1, center2];
      // the more iterations here, the nicer clusters will look joined.
      // 1 = ok, 2 = much better than 1, 3+ = unlikely to noticably improve upon 2
      for (var i = 0; i < this.options.clusterIterations; i++){
        pair = this.joinClusters(ctx, pair[0], pair[1], cluster1, cluster2, above);
      }
      // TODO - once a pair is found, move each point in the pair a little toward its respective center point
      return pair;
    },

    joinClusters: function(ctx, center1, center2, cluster1, cluster2, above){
      if (center1 == center2){
        return [center1, center2];
      }

      var dy = center2[1] - center1[1];
      var dx = center2[0] - center1[0];

      // guard clause - avoid division by 0
      if (dx == 0){
        // TODO - handle this case better. this would happen if it's a vertical line between center1 and center2
        return [center1, center2];
      }
      var slope = dy / dx;

      function isAboveLine(x, y) {
        return y > slope * (x - center1[0]) + center1[1];
      }

      function partition(cluster){
        if (above){
          return cluster.points.filter(function(point){
            return isAboveLine(point[0], point[1]);
          });
        }
        else{
          return cluster.points.filter(function(point){
            return !isAboveLine(point[0], point[1]);
          });
        }
      }
      var c1 = partition(cluster1);
      var c2 = partition(cluster2);

      var ax = center1[0];
      var ay = center1[1];
      var bx = center2[0];
      var by = center2[1];
      function getDistance(point){
        var cx = point[0];
        var cy = point[1];
        return ax * (by - cy) + bx * (cy - ay) + cx * (ay - by);
      }
      function getFurthestPoint(points, defaultPoint){
        var maxDistance = 0;
        var maxPoint = defaultPoint;
        for (var point of points){
          var distance = getDistance(point);
          if (distance > maxDistance){
            maxDistance = distance;
            maxPoint = point;
          }
        }
        return maxPoint;
      }

      return [getFurthestPoint(c1, center1), getFurthestPoint(c2, center2)];
    },

    drawLine: function(ctx, point1, point2) {
        if (!point1 || !point2){
          return;
        }

        ctx.strokeStyle = 'green';
        ctx.beginPath();
        ctx.moveTo(point1[0], point1[1]);
        ctx.lineTo(point2[0], point2[1]);
        ctx.stroke();
    },

    getOutlineImage: function(pixels) {
      var d = pixels.data;
      for (var i = 0; i < d.length; i += 4) {
        var r = d[i];
        var g = d[i+1];
        var b = d[i+2];
        var a = d[i+3];
        if (a <= this.options.alphaThreshold) {
          d[i+0] = this.options.overlayRGBA[0];
          d[i+1] = this.options.overlayRGBA[1];
          d[i+2] = this.options.overlayRGBA[2];
          d[i+3] = this.options.overlayRGBA[3];
        }
        else {
          d[i] = d[i+1] = d[i+2] = d[i+3] = 0;
        }
      }
      return pixels;
    },

    applyStickerEffectForTextAndOther: function(callback) {
      // TODO - refactor. perhaps with promises
      var self = this;

      // outline text
      self.onCleanCanvas(function(cleanCanvas) {
        cleanCanvas.forEachObject(function(obj) {
          obj.visible = self.isText(obj);
        });

        var textImg = document.createElement("img");
        textImg.onload = function(){
          var textOutlineCanvas = self.stickerEffect(textImg, self.options.textThickness);
          createStickerEffectForNonTextObjects(textOutlineCanvas);
        };
        self.getCanvasImage(cleanCanvas, function(canvasImg){
          textImg.src = canvasImg;
        });
      });

      // outline non-text
      function createStickerEffectForNonTextObjects(textOutlineCanvas) {
        self.onCleanCanvas(function(cleanCanvas) {
          cleanCanvas.forEachObject(function(obj) {
            obj.visible = !self.isText(obj);
          });

          var nonTextImg = document.createElement("img");
          nonTextImg.onload = function(){
            var nonTextOutlineCanvas = self.stickerEffect(nonTextImg, self.options.thickness);
            combineTextAndNonTextOutlines(textOutlineCanvas, nonTextOutlineCanvas);
          };
          self.getCanvasImage(cleanCanvas, function(canvasImg){
            nonTextImg.src = canvasImg;
          });
        });
      }

      // combine outlines
      function combineTextAndNonTextOutlines(textOutlineCanvas, nonTextOutlineCanvas) {
        var combinedCanvas = textOutlineCanvas;
        var ctx = combinedCanvas.getContext("2d");
        ctx.drawImage(nonTextOutlineCanvas, 0, 0);

        callback(combinedCanvas);
      }
    },

    stickerEffect: function(img, thickness) {
      // https://stackoverflow.com/questions/34374695/fabricjs-add-a-custom-path-around-the-objects
      var canvas1 = document.createElement("canvas");
      var ctx1 = canvas1.getContext("2d");
      var canvas2 = document.createElement("canvas");
      var ctx2 = canvas2.getContext("2d");

      canvas1.width = canvas2.width = img.width;
      canvas1.height = canvas2.height = img.height;

      ctx1.drawImage(img, 0, 0);
      ctx2.shadowColor = 'red';
      ctx2.shadowBlur = this.options.blurAmount;

      for(var i = 0; i < thickness; i++){
        ctx2.drawImage(canvas1, 0, 0);
        ctx1.drawImage(canvas2, 0, 0);
      }

      return canvas2;
    },
  };

  var Tracer = {
    getPoints: function(outlineCanvas, cluster){
      var points = this.getMarchingSquaresPoints(outlineCanvas, cluster);
      points = this.trimCornerPoints(points);

      for (var i = 0; i < Outliner.options.smoothIterations; i++){
        points = this.smoothPoints(points);
      }

      return points;
    },

    trimCornerPoints: function(points){
      var newPoints = [];
      for (var i = 1; i < points.length - 1; i++){
        if (!this.isCorner(points, i)){
          newPoints.push(points[i]);
        }
      }
      return newPoints;
    },

    isCorner: function(points, index) {
      if (index < 1 || index >= points.length-2){
        return false;
      }

      var before = points[index - 1];
      var current = points[index];
      var after = points[index + 1];

      var x = current[0];
      var y = current[1];

      function checkIsCorner(before, after){
        var isAbove = x == before[0] && y > before[1];
        var isBelow = x == before[0] && y < before[1];
        var isLeft = x > after[0] && y == after[1];
        var isRight = x < after[0] && y == after[1];

        return isLeft && isAbove ||
               isAbove && isRight ||
               isRight && isBelow ||
               isBelow && isLeft;
      }

      return checkIsCorner(before, after) || checkIsCorner(after, before);
    },

    getMarchingSquaresPoints: function(outlineCanvas, cluster){
      var firstPoint = null;
      if (cluster) {
        var topPoint = cluster.points[0];
        for (var point of cluster.points) {
          if (point[1] < topPoint[1]) {
            topPoint = point;
          }
        }
        firstPoint = {x: topPoint[0] + 1, y: topPoint[1] + 1};
      }

      var marchingPoints = MarchingSquares.getBlobOutlinePoints(outlineCanvas, firstPoint);
      var points = [];
      for (var i = 0; i+1 < marchingPoints.length; i+=2){
        points.push([marchingPoints[i], marchingPoints[i+1]]);
      }
      return points;
    },

    smoothPoints: function(points){
      if (points.length <= 2){
        return points;
      }

      function averagePoints(point1, point2){
        const x1 = point1[0];
        const y1 = point1[1];
        const x2 = point2[0];
        const y2 = point2[1];
        return [(x1+x2)/2, (y1+y2)/2];
      }

      var firstPoint = points[0];
      var lastPoint = points[points.length-1];
      for (var i = 0; i+1 < points.length; i++){
        var currentPoint = points[i];
        var nextPoint = points[i+1];
        points[i] = averagePoints(points[i], points[i+1]);
      }

      points[points.length-1] = averagePoints(firstPoint, lastPoint);

      return points;
    },
  };

  //
  function observeShape(property) {
    document.getElementById(property).onclick = function() {
      if (property == 'isRect')
      {
        currentOutline = rectOutlineInfo;
      }
      else if (property == 'isEllipse')
      {
        currentOutline = ellipseOutlineInfo;
      }
      else if (property == 'isSmallCutline')
      {
        Outliner.generateCutline();
        return;
      }
      else if (property == 'isMediumCutline')
      {
        Outliner.generateCutline();
        return;
      }
      else if (property == 'isLargeCutline')
      {
        Outliner.generateCutline();
        return;
      }

      resizeOutline();
      updateOutlinePath();
      canvas.renderAll();
    };
  }

  function observeNumeric(property) {
    var element = document.getElementById(property);

    element.onchange = function() {
      resizeOutlineWithRatio(this);
      updateOutlinePath();
      canvas.renderAll();
    };

    element.onkeypress = function() {
      var x = window.event.which;
      if (x == 13)
      {
        element.onchange();
      }
    }

    updateOutlinePath();
  }

  function observeSize(property) {
    document.getElementById(property).onclick = function() {
      if (property == '3716')
      {
        OutlineInput.setWidth(37);
        OutlineInput.setHeight(16);
      }
      else if (property == '6026')
      {
        OutlineInput.setWidth(60);
        OutlineInput.setHeight(26);
      }
      else if (property == '3010')
      {
        OutlineInput.setWidth(30);
        OutlineInput.setHeight(10);
      }

      resizeOutline();
      updateOutlinePath();
      canvas.renderAll();
    };
  }

  observeShape('isRect');
  observeShape('isEllipse');
  observeShape('isSmallCutline');
  observeShape('isMediumCutline');
  observeShape('isLargeCutline');

  observeNumeric('width');
  observeNumeric('height');

  observeSize('3010');
  observeSize('3716');
  observeSize('6026');
