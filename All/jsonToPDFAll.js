function loadfromJsonExp(jsonstring, filename, type){

  jsonstring = JSON.stringify(jsonstring);

  jsonstring = jsonstring.replace(/(['"])src(['"]):(['"])http:\/\/.*?\//, '$1src$2:$3' + server_path);
  jsonstring = jsonstring.replace(/\n/g, "\\n");
  var json = JSON.parse(jsonstring);
  // console.log(json);
  var objectsToAdd = [];
  var numReplaced = 0;
  $('#width').val(json.width);
  $('#height').val(json.height);

  //*
  //delete json.canvas.backgroundImage;
  //delete json.canvas.overlayImage;
  canvas.loadFromJSON(json.canvas, function(){

	outlineOptions.fill = json.outlineInfo.fill;
	var outlinePath;
	resizeOutline();
	if (json.shape == 'rect')
	{
		currentOutline = rectOutlineInfo;
		outlinePath = getRectClipPath();
	}
	else if (json.shape == 'ellipse')
	{
		currentOutline = ellipseOutlineInfo;
		outlinePath = getEllipseClipPath();
	}
	var overlay = outlinePath.cloneAsImage();
	canvas.setOverlayImage(overlay);
	canvas.renderAll();

	if (json.backImgInfo.beingUsed)
	{
		var backImg = new Image();
		backImg.src = json.backImgInfo.src;

		backImgInfo.isLocal = false;
		addBackgroundFromImgElement(backImg);
	}

	canvas.renderAll.bind(canvas);
    var destination = new fabric.Canvas('myCanvas');
    var source = "";
    if(type == 6 || type == 7) {
      filename = filename + ".png";
      var postthumbnail = stickerToImage(canvas);
      return postthumbnail;
      var img = new Image();
      img.src = postthumbnail;
      var url = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');

      var uri = postthumbnail;
      var downloadLink = document.createElement("a");
      downloadLink.href = uri;
      downloadLink.download = filename;

      document.body.appendChild(downloadLink);
      //downloadLink.click();
      document.body.removeChild(downloadLink);


    } else {
      filename = filename + ".pdf";
      if(json.backImgInfo.beingUsed) {
        if(json.shape == 'rect') {
          return imageRectToPDF(source, destination,filename);
        } else {
          return imageEllipseToPDF(source, destination,filename);
        }
      } else {
        if(json.shape == 'rect') {
          return rectToPDF(source, destination,filename);
        } else {
          return ellipseToPDF(source, destination,filename);
        }
      }
    }

  },
  function(o, object){
	if (o.type == 'image')
	{
		// object.src = o.src.replace(/http:\/\/.*?\//, 'http://79.161.166.153/');
		applyClipartSettings(object);
	}
	else if (o.type == 'path' || o.type == 'path-group')
	{
		object.isSVG = true;
		applyClipartSettings(object);
	}
	else if (o.type == 'text')
	{
		applyTextSettings(object);
	}
	else if (o.type == 'i-text')
	{
		applyTextSettings(object);
	}
  });

  setBorderColor();
  setPrice();
  //*/

  canvas.calcOffset();
  canvas.renderAll();

}

function stickerToImage(source){
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed
  var mmWidth = document.getElementById("width").value;
  var mmHeight = document.getElementById("height").value;
  var padding;
  if(mmWidth == 30) {
    padding = 80;
  } else if(mmWidth == 37) {
    padding = 65;
  } else {
    padding = 38;
  }

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = (parseInt(mmWidth)) / 2
  var marginT = (parseInt(mmHeight)) / 2

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + ((bgWidth-padding) / 2));
  var pathStartY = 450 - (225 + ((bgHeight-padding) / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth - padding,
    height: bgHeight - padding,
    left: pathStartX,
    top: pathStartY
  });
  return imgData;
}

//use to render a sticker that contains an image as a background and is in the form of an ellipse
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function imageEllipseToPDF(source, destination, filename) {
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed
  var mmWidth = document.getElementById("width").value;
  var mmHeight = document.getElementById("height").value;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  canvas.renderAll();

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = (parseInt(mmWidth)) / 2;
  var marginT = (parseInt(mmHeight)) / 2;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth), parseInt(mmHeight), '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.ellipse(margin + marginL, margin + marginT , (parseInt(mmWidth)-1)/2 , (parseInt(mmHeight)-1)/2 ,'S');
  return pdf;
  pdf.save(filename);
}

//use to render a sticker with a background image in the shape of a rectangle
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function imageRectToPDF(source, destination, filename) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //});
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed
  var mmWidth = document.getElementById("width").value;
  var mmHeight = document.getElementById("height").value;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 0 * mm2pixels;
  var bgScaleMM = 0;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width;
  var bgHeight = height;

  canvas.renderAll();

  //destination.backgroundImage.scaleToWidth(bgWidth);
  //destination.backgroundImage.scaleToHeight(bgHeight);

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = 0.5;
  var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth), parseInt(mmHeight), '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.roundedRect(margin + marginL, margin + marginT , parseInt(mmWidth)-1 , parseInt(mmHeight)-1, 2 , 2 ,'S');
  return pdf;
  pdf.save(filename);
}

//use with solid color background images in the shape of an ellipse
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function ellipseToPDF(source, destination,filename) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //});

  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed
  var mmWidth = document.getElementById("width").value;
  var mmHeight = document.getElementById("height").value;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 1 * mm2pixels;
  var bgScaleMM = 1;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  canvas.backgroundImage.scaleX = bgWidth/width;
  canvas.backgroundImage.scaleY = bgHeight/height;

  canvas.renderAll();

  //get the margin for the cutline.
  //(bgWidth - width)/2 is the difference on each side
  var marginL = 0.5 + (parseInt(mmWidth)) / 2;
  var marginT = 0.5 + (parseInt(mmHeight)) / 2;

  //var marginL = 0.5;
  //var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth) + bgScaleMM, parseInt(mmHeight) + bgScaleMM, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.ellipse(margin + marginL, margin + marginT , parseInt(mmWidth)/2 , parseInt(mmHeight)/2 ,'S');
  return pdf;
  pdf.save(filename);
}

//use with stickers with colored backgrounds in the shape of a rectangle
/* Params:
 * source: a JSON produced by fabric.canvas.toJSON() or JSON.stringify(fabric.canvas)
 * destination: the canvas to which this sticker will be temporarily drawn, clears any previous elements
 * filename: the filename for the generated PDF file. i.e. "sticker-1234.pdf"
*/
function rectToPDF(source, destination,filename) {
  // destination.loadFromJSON(source, function() {
  //   destination.renderAll.bind(destination);
  //
  // });
  //according to the web, on a 600 dpi pdf, there are 24 pixels per millimeter
  var mm2pixels = 24;

  //original height and width of the background
  var width = canvas.backgroundImage.width;
  var height = canvas.backgroundImage.height;

  //the height and width of the sticker when it will be printed
  var mmWidth = document.getElementById("width").value;
  var mmHeight = document.getElementById("height").value;

  //the stickers width and height in pixels based on the print size
  var stickerWidth = mmWidth * mm2pixels;
  var stickerHeight = mmHeight * mm2pixels;

  //the factor by which the sticker must be multiplied to obtain the correct DPI
  var mult = stickerWidth/width;

  //the scale by which we want to multiply the background to pad the cutline
  var bgScalePx = 1 * mm2pixels;
  var bgScaleMM = 1;
  bgScalePx /= mult;

  //remove the stroke on the background (we're going to add our own path) and scale it to add some padding
  canvas.backgroundImage.strokeWidth = 0;

  var bgWidth = width + bgScalePx;
  var bgHeight = height + bgScalePx;

  canvas.backgroundImage.scaleX = bgWidth/width;
  canvas.backgroundImage.scaleY = bgHeight/height;

  // this didn't fix anything
  //canvas.backgroundImage.scaleX *= 1.1;
  //canvas.backgroundImage.scaleY *= 1.1;

  canvas.renderAll();

  var marginL = 0.5;
  var marginT = 0.5;

  //remove the old cutline and render the canvas
  canvas.overlayImage = "";
  canvas.renderAll();

  var margin = 36;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + (bgWidth / 2));
  var pathStartY = 450 - (225 + (bgHeight / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = canvas.toDataURL({
    format: 'png',
    multiplier: mult,
    width: bgWidth,
    height: bgHeight,
    left: pathStartX,
    top: pathStartY
  });

  //the pdf width and height with a margin around them
  var pdfWidth = (margin * 2) + parseInt(mmWidth);
  var pdfHeight = (margin * 2) + parseInt(mmHeight);

  //create a new pdf
  var pdf = new jsPDF('l', 'mm', [pdfWidth, pdfHeight]);

  //(imageData, format, x, y, w, h, alias, compression, rotation)
  //add the image in the left most corner (we can change the 0s as they are the margin)
  pdf.addImage(imgData, 'png', margin, margin, parseInt(mmWidth) + bgScaleMM, parseInt(mmHeight) + bgScaleMM, '', 'FAST');

  //set the stroke line width to somethign small
  pdf.setLineWidth(0.1);

  //create the cutline
  pdf.roundedRect(margin + marginL, margin + marginT , parseInt(mmWidth), parseInt(mmHeight), 2 , 2 ,'S');
  return pdf;
  pdf.save(filename);
}

function stickerRender(id) {
  var jsonToLoad = JSON.parse(document.getElementById(id).value);
  var sticktype = document.getElementById("type-"+id).value;
  // console.log("sticktype: " + sticktype);
  // console.log("SHAPE: " + jsonToLoad.shape);
  // console.log("Background used: " + jsonToLoad.backImgInfo.beingUsed);
  var filename = "" + id;

  loadfromJsonExp(jsonToLoad, filename, sticktype);
  canvas.forEachObject(function(o) {
    o.selectable = false;
  });
}
