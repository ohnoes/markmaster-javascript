/*
 * Function
 */
function updatenBackgroundBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var nbackgroundBox = $(".nbackgroundBox");
	nbackgroundBox.offset({top: canvasOffset.top + 20, left:20});
}

function backgroundCategorySelect(category){
	$(".backgroundImageBox").html('');
	var stickerType = $('#stickertype').val();
	var clipartImageBox = $(".backgroundImageBox");
	var clipartLink = "../../img/add_clipart.png";

	//calculate the size of the images based on the number of columns and the width of the div
	var imageSize = (clipartImageBox.width() - 20) / 3;

	//create style with width and height definitions
	var imageStyle = $('<style>.clipartImage {id: "myStyleTag"; width:' + imageSize + 'px; height:' + imageSize + 'px; padding: 10px; cursor: pointer; cursor: hand; display: inline-block; text-align: center; vertical-align: middle; line-height: ' + imageSize +'px;}</style>');
	$('#myStyleTag').remove();
	imageStyle.appendTo("head");

	var numAdded = 0;
	for(var i = 0; i < background.length; i++)
	{
		if(category != 0) {
			if((background[i].public == category) && canColor(stickerType, background[i].can_color)) {
				clipartLink = background[i].name;
				//clipartLink = background[i].name;
				onclickTag = 'addBackground(\'' + background[i].name + '\')';
				if(numAdded % 3 == 0 && numAdded != 0)
					clipartImageBox.append('<br />');

				//once we have images we'll dynamically change clipartLink to the appropriate image
				//we'll also create an onclick to add it to the canvas.
				var img = '<div class="clipartImage"> <img src="' + clipartLink + '" name="' + clipartLink + '"  onclick="' + onclickTag + '" style="max-height: 100%; max-width:100%"/> </div>';
				clipartImageBox.append(img);

				numAdded++;
			}
		} else {
			if((background[i].public == category) && canColor(stickerType, background[i].can_color)) {
				clipartLink = background[i].name;
				//clipartLink = background[i].name;
				onclickTag = 'addBackground(\'' + background[i].name + '\')';
				if(numAdded % 3 == 0 && numAdded != 0)
					clipartImageBox.append('<br />');

				//once we have images we'll dynamically change clipartLink to the appropriate image
				//we'll also create an onclick to add it to the canvas.
				var img = '<div class="clipartImage"> <img src="' + clipartLink + '" name="' + clipartLink + '"  onclick="' + onclickTag + '" style="max-height: 100%; max-width:100%"/> </div>';
				clipartImageBox.append(img);

				numAdded++;
			}
		}
	}
}

function canColor(stickerType, colorValue){
	console.log("stickertype: " + stickerType + " colorValue: " + colorValue);
	if(colorValue == 1 && (stickerType == 6 || stickerType == 7))
		return false;
	else if(colorValue == 2 && (stickerType >= 3 && stickerType <= 7))
		return false;
	return true;
}

function searchBackground(){
	$(".backgroundImageBox").html('');
	var stickerType = $('#stickertype').val();
	var clipartImageBox = $(".backgroundImageBox");
	var clipartLink = "../../img/add_clipart.png";

	console.log("the stickertype: " + stickerType);

	//calculate the size of the images based on the number of columns and the width of the div
	var imageSize = (clipartImageBox.width() - 20) / 3;
	//create style with width and height definitions
	var imageStyle = $('<style>.clipartImage {id: "myStyleTag"; width:' + imageSize + 'px; height:' + imageSize + 'px; padding: 10px; cursor: pointer; cursor: hand;}</style>');
	$('#myStyleTag').remove();
	imageStyle.appendTo("head");

	var numAdded = 0;
	for(var i = 0; i < background.length; i++)
	{

		if(background[i].searchwords.indexOf(searchWord) != -1 && canColor(stickerType, background[i].can_color))
		{
			clipartLink = background[i].name;
			//clipartLink = background[i].name;
			onclickTag = 'addClipart(\'' + background[i].name + '\')';
			if(numAdded % 3 == 0 && numAdded != 0)
				clipartImageBox.append('<br />');

			//once we have images we'll dynamically change clipartLink to the appropriate image
			//we'll also create an onclick to add it to the canvas.
			var img = '<div class="clipartImage"> <img src="' + clipartLink + '" name="' + clipartLink + '"  onclick="' + onclickTag + '" style="max-height: 100%; max-width:100%"/> </div>';
			clipartImageBox.append(img);

			numAdded++;
		}
	}
}

function nbackgroundBoxHide() {
	$('.backgroundImageBox').html('');
	$('.nbackgroundBox').css("display", "none");
}

function nbackgroundBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.backgroundImageBox').html('');
	$('.nbackgroundBox').css("display", "inherit");
	backgroundCategorySelect(1);
	updatenBackgroundBox();
}


$(document).ready(function() {
	updatenBackgroundBox();
	//backgroundCategorySelect(1);
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updatenBackgroundBox();
}
