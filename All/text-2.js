/////////////////////////////////////////////////////////
//        tab   3 - add text
/////////////////////////////////////////////////////////

var minFontScale = 1.5;

canvas.on('object:scaling', function(event){

    // lower limit = 6 * 5 = 30;
    // setting fontSize to 20, lower scale limit is now 1.5
    var obj = event.target;
    if (isText(obj)) {
        if(obj.scaleX < minFontScale || obj.scaleY < minFontScale) {
            obj.scaleX = minFontScale;
            obj.scaleY = minFontScale;
        }
        $('#fontSizeTextbox').val(obj.scaleX);
    }
});

// scales text to match proportions of sticker
function updateTextSize()
{
	var cWidth = canvas.getWidth();
	var cHeight = canvas.getHeight();

  OutlineInput.sanitizeInputs();
  var width = OutlineInput.getWidth();
  var height = OutlineInput.getHeight();

	var mmLimit;

	if (width > height)
	  {
		mmLimit = width/cWidth;
	  }
	  else
	  {
		mmLimit = height/cHeight;
	  }

	minFontScale = 2.47 / (26 * mmLimit);
    //minFontScale = 2.10 / (26 * mmLimit);

  canvas.forEachObject(function(obj){
      if (isText(obj)) {

        var fontSize = obj.scaleX;
        if (fontSize < minFontScale) {
          fontSize = minFontScale;
          // alert('too small');
        }
        obj.scaleX = fontSize;
        obj.scaleY = fontSize;

        if (canvas.getActiveObject() == obj)
        {
          // update text controls
         // $('#fontSizeSlider').val(obj.fontSize);
          $('#fontSizeTextbox').html(obj.scaleX);
        }
        // obj.fontSize

        // obj.scale(pt2mm * mm2px, pt2mm * mm2px);
      }
  });

  canvas.renderAll();
  canvas.calcOffset();
}


function setText() {

  if(canvas.getActiveObject() == null) {
    setTextInput();
  }

  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  var text = document.getElementById('text-input').value;

  // REMOVE EMOJI
  text = validatedText(text);
  if(text != document.getElementById('text-input').value) {
    alert('Illegal character removed!');
  }
  document.getElementById('text-input').value = text;
  if (isText(currentObj))
  {
    currentObj.setText(text);
  }
  else if (isGroup(currentGroup)) {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      if (isText(groupArr[i])) {
        groupArr[i].text = text;
      }
    }
  }
  canvas.renderAll();
}

// remove emoji etc
function validatedText(input) {
    var result = '';
    var re = /xxx[\x00-\xFF]+xxx/;
    // if(re.test('xxx'+input+'xxx')) {
    //   //alert('all good');
    // } else {
    //   alert('Illegal character removed!');
    // }
    if (input.length == 0)
            return input;
            for (var indexOfInput = 0, lengthOfInput = input.length; indexOfInput < lengthOfInput; indexOfInput++) {
                if (re.test('xxx'+input[indexOfInput]+'xxx')) {
                  result += input[indexOfInput];
                } else {

                }
            }
            return result;

  };


// add text
function setTextInput() {
  var text = document.getElementById('text-input').value;
  text = validatedText(text);
  if(text != document.getElementById('text-input').value) {
    alert('Illegal character removed!');
  }
  document.getElementById('text-input').value = text;
  var alignment = $('#text-input').css('text-align');
  var weight = $('#text-input').css('font-weight');
  var style = $('#text-input').css('font-style');
  var decoration = $('#text-input').css('text-decoration');

  var string = decoration;
  string = string.split(" ");
  if(string.length > 1) {
    decoration = string[0];
  }

  var currentText = new fabric.IText(text,
    { left: 100,
      top: 100,
      originX: 'center',
      originY: 'center',
      textAlign: alignment,
      textDecoration: decoration,
      fontWeight: weight,
      fontSize: 20,
      fontStyle: style
    });

  // if(document.getElementById('strokeWidth').value == 0) { // to remove the outline if the slider is at 0
  //   currentText.stroke = document.getElementById('fill').value;
  // }
  canvas.add(currentText);
  canvas.renderAll();
  canvas.calcOffset();
  updateTextInput(currentText);
  updateTextSize();
  //orderLayers();
}

// add text
function updateTextInput(textObj) {
  var currentText = textObj;

  currentText.padding = 10;

  applyTextSettings(currentText);

  var firstFont = $('#fontSelect').css('font-family');
  if (firstFont.indexOf(", ") == -1) {
      var testFont = firstFont.split(",");
  } else {
      var testFont = firstFont.split(", ");
  }
  var theFont = testFont[1];
  currentText.fontFamily = theFont;
  currentText.scaleX = document.getElementById('fontSizeTextbox').value;
  currentText.scaleY = document.getElementById('fontSizeTextbox').value;
  currentText.lineHeight = document.getElementById('lineHeightTextbox').value;
  currentText.fill = document.getElementById('fill').value;
  currentText.stroke = document.getElementById('stroke').value;
  currentText.strokeWidth = parseFloat(document.getElementById('strokeWidthTextbox').value);
  // if(document.getElementById('strokeWidth').value == 0) { // to remove the outline if the slider is at 0
  //   currentText.stroke = document.getElementById('fill').value;
  // }

  currentText.center();
  currentText.setCoords();
  // canvas.on('object:selected', objectSelected);
  //updateTextSize();
  canvas.setActiveObject(currentText);
  canvas.renderAll();
  canvas.calcOffset();

  //orderLayers();
}

function changeTextFont(selectedFont) {
    if (selectedFont.indexOf(", ") == -1) {
        var testFont = selectedFont.split(",");
    } else {
        var testFont = selectedFont.split(", ");
    }
    var theFont = testFont[1];
    var obj = canvas.getActiveObject();

    //MAT: checking for curvedText as setting the fontFamily is done using a set(name, parameter)
    if(obj != null && /curvedText/.test(obj.type))
    {
        obj.set('fontFamily', theFont);
        // canvas.on('object:selected', objectSelected);
        //updateTextSize();
        canvas.setActiveObject(obj);
        canvas.renderAll();
        canvas.calcOffset();
    }else if(isText(obj)) {
        obj.fontFamily = theFont;
        // canvas.on('object:selected', objectSelected);
        //updateTextSize();
        canvas.setActiveObject(obj);
        canvas.renderAll();
        canvas.calcOffset();
    }
}

function applyTextSettings(textObj)
{

	textObj.setControlsVisibility({
        mr: false,
        ml: false,
        mt: false,
        mb: false,
        tl: false,
        bl: false
  });

	textObj.set({
	  borderColor: 'black',
	  cornerColor: 'black',
    transparentCorners: true,
    cornerSize: 60
	});

	if(outlineOptions.fill == 'black' || outlineOptions.fill == '#000000')
	{
		textObj.set({
		  borderColor: 'red',
		  cornerColor: 'red',
      transparentCorners: true,
      cornerSize: 60
		});
	}
  textObj.centeredScaling = true;
  textObj['scaleXStart'] = textObj.scaleX;
  textObj['scaleYStart'] = textObj.scaleY;
}

function setTextColor(textColor) {
  var currentObj = canvas.getActiveObject();
  if (isText(currentObj))
  {
    canvas.getActiveObject().set({fill: textColor});
    canvas.renderAll();
  }
}


function setTextColorInput() {
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS
  var textColor = document.getElementById('textColorInput').value;
  if (textColor[0] != '#')
    textColor = '#' + textColor;
  var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(textColor);
  if (!isOk)
    return;
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS

  var styleString = "background:" + textColor;
  var customColor = document.getElementById('customTextColor');
  customColor.style.cssText = 'background:'+ textColor;
  setTextColor(textColor);
  canvas.renderAll();
}

function setStrokeColor(strokeColor) {

  var currentObj = canvas.getActiveObject();

  if (isText(currentObj))
  {
    currentObj.stroke = strokeColor;
    canvas.renderAll();
  }
}


function setStrokeColorInput() {

  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS
  var strokeColor = document.getElementById('strokeColorInput').value;
  if (strokeColor[0] != '#')
    strokeColor = '#' + strokeColor;
  var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(strokeColor);
  if (!isOk)
    return;
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS


  var styleString = "background:" + strokeColor;
  var customColor = document.getElementById('customStrokeColor');
  customColor.style.cssText = 'background:'+ strokeColor;
  setStrokeColor(strokeColor);
  canvas.renderAll();
}


function deleteText() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (isText(currentObj))
  {
    var yes = confirm("Are you sure you want to delete the selected text?");
    if (yes)
    {
      currentObj.remove();
    }
  }
  else if (isGroup(currentGroup))
  {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].remove();
    }
    canvas.discardActiveGroup();
  }
  //orderLayers();
  canvas.renderAll();
  canvas.calcOffset();
}

function isText(obj)
{
  return obj != null && obj.text != null;
}

function isGroup(obj)
{
  return obj != null && obj._objects != null;
}

function isImg(obj)
{
  return obj != null && obj._element != null;
}

function isSVG(obj)
{
  return obj != null && obj['isSVG'] == true;
}

function isClipart(obj)
{
	return isImg(obj) || isSVG(obj);
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// function onFontSizeChange()
// {
  // var fontSizeSlider = $('#fontSizeSlider');
  // $('#fontSizeTextbox').val(fontSizeSlider.val());

  // var currentObj = canvas.getActiveObject();
  // if (!isText(currentObj))
    // return;

  // currentObj.fontSize = fontSizeSlider.val();
  // updateTextSize();
  // canvas.renderAll();
// }

// $('#fontSizeSlider').on('input', onFontSizeChange);
// $('#fontSizeSlider').on('change', onFontSizeChange);

var inter = null;

function increaseFont()
{
	var currentFont = $('#fontSizeTextbox').val();
	var newFont = parseFloat(currentFont) + 0.1;
	$('#fontSizeTextbox').val(newFont.toFixed(1));

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.scaleX = newFont;
    currentObj.scaley = newFont;
	updateTextSize();
	canvas.renderAll();
}

function decreaseFont()
{
	var currentFont = $('#fontSizeTextbox').val();
	var newFont = parseFloat(currentFont) - 0.1;
	if (currentFont <= minFontScale){
		newFont = currentFont;
	} else {
		$('#fontSizeTextbox').val(newFont.toFixed(1));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

    currentObj.scaleX = newFont;
    currentObj.scaley = newFont;
	updateTextSize();
	canvas.renderAll();
}

function increaseTextStroke()
{
	var currentFont = $('#strokeWidthTextbox').val();
	var newFont = parseFloat(currentFont) + 0.01;
	if (newFont > 1.0) {
		newFont = 1.0;
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	} else {
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

    if(/curvedText/.test(currentObj.type))
        currentObj.set('strokeWidth', newFont);
    else
        currentObj.strokeWidth = newFont;

	updateTextSize();
	canvas.renderAll();
}

function decreaseTextStroke()
{
	var currentFont = $('#strokeWidthTextbox').val();
	var newFont = parseFloat(currentFont) - 0.01;
	if (newFont < 0.01) {
		newFont = 0.01;
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	} else {
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	if(/curvedText/.test(currentObj.type))
        currentObj.set('strokeWidth', newFont);
    else
        currentObj.strokeWidth = newFont;

	updateTextSize();
	canvas.renderAll();
}

function increaseLineHeight() // max value 2
{
	var currentFont = $('#lineHeightTextbox').val();
	var newFont = parseFloat(currentFont) + 0.01;
	if (newFont > 2){
		newFont = currentFont;
	} else {
		$('#lineHeightTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.lineHeight = newFont;
	updateTextSize();
	canvas.renderAll();
}

function decreaseLineHeight() // min value 0
{
	var currentFont = $('#lineHeightTextbox').val();
	var newFont = parseFloat(currentFont) - 0.01;
	if (newFont < 0){
		newFont = currentFont;
	} else {
		$('#lineHeightTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.lineHeight = newFont;
	updateTextSize();
	canvas.renderAll();
}

function increaseTextRotation() { // max value 359.9, at 360 we set it to 0
	var currentFont = $('#textAngleTextbox').val();
	var newFont = parseFloat(currentFont) + 0.10;

	if (newFont > 359.90) {
		newFont = 0;
		$('#textAngleTextbox').val(newFont.toFixed(2));
	} else {
		$('#textAngleTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.rotate(newFont);
	updateTextSize();
	canvas.renderAll();
}

function decreaseTextRotation() { // min value 0, if we go any lower, we set it to 359.9
	var currentFont = $('#textAngleTextbox').val();
	var newFont = parseFloat(currentFont) - 0.10;

	if (newFont < 0.00) {
		newFont = 359.9;
		$('#textAngleTextbox').val(newFont.toFixed(2));
	} else {
		$('#textAngleTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.rotate(newFont);
	updateTextSize();
	canvas.renderAll();
}

function increaseObjectScale() {
	var currentArt =  $('#clipartScaleTextbox').val();
	var newArt = parseFloat(currentArt) + 0.01;

	$('#clipartScaleTextbox').val(newArt.toFixed(2));

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.scale(currentObj.scaleXStart*newArt, currentObj.scaleYStart*newArt);
	canvas.renderAll();
}

function decreaseObjectScale() {
	var currentArt =  $('#clipartScaleTextbox').val();
	var newArt = parseFloat(currentArt) - 0.01;
	if (newArt < 0.01) {
		newArt = 0.01
	}
	$('#clipartScaleTextbox').val(newArt.toFixed(2));

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.scale(currentObj.scaleXStart*newArt, currentObj.scaleYStart*newArt);
	canvas.renderAll();
}

function increaseObjectRotation() {
	var currentArt =  $('#clipartAngleTextbox').val();
	var newArt = parseFloat(currentArt) + 0.10;

	if (newArt > 359.9) {
		newArt = 0;
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	} else {
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.rotate(newArt);
	canvas.renderAll();
}

function decreaseObjectRotation() {
	var currentArt =  $('#clipartAngleTextbox').val();
	var newArt = parseFloat(currentArt) - 0.10;

	if (newArt < 0) {
		newArt = 359.9;
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	} else {
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.rotate(newArt);
	canvas.renderAll();
}

function moveObjectLeft() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.left -= 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.left -= 1;
	}
	canvas.renderAll();
}

function moveObjectRight() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.left += 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.left += 1;
	}
	canvas.renderAll();
}

function moveObjectTop() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.top -= 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.top -= 1;
	}
	canvas.renderAll();
}

function moveObjectDown() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.top += 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.top += 1;
	}
	canvas.renderAll();
}



function onFontSizeTextInput()
{
  var fontSize = $('#fontSizeTextbox').val();
  if (isNumber(fontSize) && minFontScale <= fontSize)
  {
    //$('#fontSizeSlider').val(fontSize);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.scaleX = fontSize;
    updateTextSize();
    canvas.renderAll();
  }
}

function onFontSizeTextChange()
{
  var fontSizeText = $('#fontSizeTextbox');
  var fontSize = fontSizeText.val();
  if (!isNumber(fontSize))
  {
    fontSizeText.val(2.5);
  }
  else if (fontSize < minFontScale)
  {
    fontSizeText.val(minFontScale);
  }

  onFontSizeTextInput();
}

$('#fontSizeTextbox').on('input', onFontSizeTextInput);
$('#fontSizeTextbox').on('change', onFontSizeTextChange);

function onLineHeightTextInput() {
  var lineHeight = $('#lineHeightTextbox').val();
  if (isNumber(lineHeight) && minLineHeight <= lineHeight && lineHeight <= maxLineHeight)
  {
    //$('#lineHeightSlider').val(lineHeight);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.lineHeight = lineHeight;
    canvas.renderAll();
  }
}


function onLineHeightTextChange()
{
  var lineHeightText = $('#lineHeightTextbox');
  var lineHeight = lineHeightText.val();
  if (!isNumber(lineHeight))
  {
    lineHeightText.val(1);
  }
  else if (lineHeight < minLineHeight)
  {
    lineHeightText.val(minLineHeight);
  }
  else if (lineHeight > maxLineHeight)
  {
    lineHeightText.val(maxLineHeight);
  }
  onLineHeightTextInput();
}

$('#lineHeightTextbox').on('input', onLineHeightTextInput);
$('#lineHeightTextbox').on('change', onLineHeightTextChange);


function onStrokeWidthInput() {
  console.log("INPUT");
  var strokeWidth = $('#strokeWidthTextbox').val();
  var strokeWidth = parseFloat(strokeWidth);
  if (isNumber(strokeWidth) && minStrokeWidth <= strokeWidth )
  {
    $('#strokeWidthTextbox').val(strokeWidth);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.strokeWidth = strokeWidth;
	   updateTextSize();
    canvas.renderAll();
  }
}

function onStrokeWidthChange() {
  console.log("CHANGE");
  var strokeWidthText = $('#strokeWidthTextbox');
  var strokeWidth = strokeWidthText.val();
  if (!isNumber(strokeWidth))
  {
	strokeWidthText.val(12);
  }
  else if (strokeWidth < minStrokeWidth)
  {
	strokeWidthText.val(minStrokeWidth);
  }

  onStrokeWidthInput();
}

$('#strokeWidthTextbox').on('input', onStrokeWidthInput);
$('#strokeWidthTextbox').on('change', onStrokeWidthChange);



////////////////////////////////////////////////////////////////////////////////
// Not Working ATM... don't know why..      Look at it Dalton                 //
////////////////////////////////////////////////////////////////////////////////

function onClipartScaleTextInput() {
  var clipartScale = $('#clipartScaleTextbox').val();
  if (isNumber(clipartScale))
  {
    //$('#clipartScaleSlider').val(clipartScale);


    var currentObj = canvas.getActiveObject();
    if (!isClipart(currentObj))
      return;

    currentObj.scale(currentObj.scaleXStart*clipartScale, currentObj.scaleYStart*clipartScale);
    canvas.renderAll();
  }
}


function onClipartScaleTextChange()
{
  var clipartScaleText = $('#clipartScaleTextbox');
  var clipartScale = clipartScaleText.val();
  if (!isNumber(clipartScale))
  {
    clipartScaleTextbox.val = 1;
  }

  onClipartScaleTextInput();
}

$('#clipartScaleTextbox').on('input', onClipartScaleTextInput);
$('#clipartScaleTextbox').on('change', onClipartScaleTextChange);

// function onClipartScaleSliderChange() {
  // var clipartScale = $('#clipartScaleSlider');
  // var scale = clipartScale.val();
  // $('#clipartScaleTextbox').val(scale);

  // var currentObj = canvas.getActiveObject();
  // if (!isClipart(currentObj))
    // return;

  // //currentObj.scale = clipartScale.val();
  // currentObj.scale(currentObj.scaleXStart*scale, currentObj.scaleYStart*scale);
  // canvas.renderAll();
// }

// $('#clipartScaleSlider').on('input', onClipartScaleSliderChange);
// $('#clipartScaleSlider').on('change', onClipartScaleSliderChange);


// function onClipartScaleSliderChange() {
  // var scale = this.value;

  // var currentObj = canvas.getActiveObject();
  // if ( !isClipart(currentObj) )
    // return;

  // currentObj.scale(currentObj.scaleXStart*scale, currentObj.scaleYStart*scale);
  // canvas.renderAll();
// }

// $('#clipartScaleSlider').on('input', onClipartScaleSliderChange);
// $('#clipartScaleSlider').on('change', onClipartScaleSliderChange);

function updateFill(){
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  var color = document.getElementById('fill').value;
  if (isText(currentObj)) {
    currentObj.set('fill', color);
    currentObj.setCoords();
    canvas.deactivateAll().renderAll();
    canvas.setActiveObject(currentObj);
  }
  else if (isGroup(currentGroup)) {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      // if obj not text, skip it
      if (!isText(groupArr[i]))
        continue;
      groupArr[i]['fill'] = color;
    }

    currentGroup.setCoords();
    canvas.deactivateAll().renderAll();
    canvas.setActiveGroup(currentGroup);
  }
  canvas.renderAll();
}

function observeTextProperty(property) {
  document.getElementById(property).onchange = function() {
    var currentObj = canvas.getActiveObject();
    var currentGroup = canvas.getActiveGroup();

    var value = this.value;

    if (property == 'fill' && this.value.charAt(0) != '#')
      value = '#' + value;

    //MAT: Checking for curvedText as setting the fill is slightly different from regular text
    // if(/curvedText/.test(currentObj.type))
    // {
    //   currentObj.set(property, value);
    // } else
    if (isText(currentObj)) {
      currentObj.set(property, value);
      currentObj.setCoords();
      canvas.deactivateAll().renderAll();
      canvas.setActiveObject(currentObj);
    }
    else if (isGroup(currentGroup)) {
      var groupArr = currentGroup.getObjects();
      for (var i = 0; i < currentGroup.size(); i++) {
        // if obj not text, skip it
        if (!isText(groupArr[i]))
          continue;
        groupArr[i][property] = value;
      }

      currentGroup.setCoords();
      canvas.deactivateAll().renderAll();
      canvas.setActiveGroup(currentGroup);
    }
    canvas.renderAll();
  };
}

function setTextAlign(textAlign) {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  toggleTextAlign(textAlign);
  $('#text-input').css('text-align', textAlign);

  if (isText(currentObj)) {
    currentObj.textAlign = textAlign;
  }
  else if (isGroup(currentGroup)) {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].textAlign = textAlign;
    }
  }

  canvas.renderAll();
}

function setTextBold() {
  toggleTextBold();
  var object = canvas.getActiveObject();
  var currentWeight = $('#text-input').css('font-weight');

  if (currentWeight == '700' || currentWeight == 'bold') {
    $('#text-input').css('font-weight', '400');
  } else {
    $('#text-input').css('font-weight', '700');
  }

  //MAT: check if its curved text as setting the parameters is slightly different
  //than regular text
  // if(/curvedText/.test(object.type))
  // {
  //   if(object.get('fontWeight') == 'normal')
  //   {
  //       object.set('fontWeight', 'bold');
  //   } else {
  //       object.set('fontWeight', 'normal');
  //   }
  // }
  //else
  if(isText(object))
  {
    if(object.fontWeight == '700' || object.fontWeight == 'bold')
    {
      object.fontWeight = '400';
    } else {
      object.fontWeight = '700';
    }
  }

  canvas.renderAll();
}

function preloadFonts() {

}

function setTextItalic() {
  toggleTextItalic();
  var object = canvas.getActiveObject();
  var currentStyle = $('#text-input').css('font-style');

  if (currentStyle == 'normal') {
    $('#text-input').css('font-style', 'italic');
  } else {
    $('#text-input').css('font-style', 'normal');
  }

  //MAT: check if its curved text as setting the parameters is slightly different
  //than regular text
  // if(/curvedText/.test(object.type))
  // {
  //   if(object.get('fontStyle') == '')
  //   {
  //       object.set('fontStyle', 'italic');
  //   } else if(object.get('fontStyle') == 'italic'){
  //       object.set('fontStyle', '');
  //   }
  // }
  // else
  if(isText(object) )
  {
    if(object.fontStyle == 'normal')
    {
      object.fontStyle = 'italic';
    } else if(object.fontStyle == 'italic')
    {
      object.fontStyle = 'normal';
    }
  }

  canvas.renderAll();
}

function setTextUnderline() {
  toggleTextUnderline();
  var object = canvas.getActiveObject();
  var currentDecoration = $('#text-input').css('text-decoration');

  var string = currentDecoration;
  string = string.split(" ");
  if(string.length > 1) {
    currentDecoration = string[0];
  }

  if (currentDecoration == 'none') {
    $('#text-input').css('text-decoration', 'underline');
  } else {
    $('#text-input').css('text-decoration', 'none');
  }
  if(isText(object))
  {
    if(object.textDecoration == 'none')
    {
      object.setTextDecoration('underline');
      object.textDecoration = 'underline';
    } else if(object.textDecoration == 'underline')
    {
      object.setTextDecoration('none');
      object.textDecoration = 'none';
    }
  }

  canvas.renderAll();
}


function moveUp() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.bringForward(currentObj);
  //orderLayers();
}

function moveDown() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.sendBackwards(currentObj);
  //orderLayers();
}

function bringToFront() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.bringToFront(currentObj);
  //orderLayers();
}

function sendToBack() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.sendToBack(currentObj);
  //orderLayers();
}


observeTextProperty('fontFamily');
observeTextProperty('fill');
observeTextProperty('strokeWidthTextbox');
observeTextProperty('stroke');

canvas.on('object:rotating', function(e) {
	var object = e.target;
	if (isText(object))
	{
		// $('#clipartScaleSlider').val(object.scaleX/object.scaleXStart);
		$('#textAngleTextbox').val(object.angle);
	}
});

//function orderLayers()
//{
  //canvas.sendToBack(currentOutline);
  //canvas.bringToFront(outlinePath);
  //canvas.renderAll();
//}
