/*
 * Function
 */
function updateEditBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var editBox = $(".editBox");
	editBox.css("width", canvasWidth);
	editBox.css("height", canvasHeight);
	editBox.offset({top: canvasOffset.top+20, left:20});
}

function editBoxHide() {
	$('.editBox').css("display", "none");
}

function editBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.editBox').css("display", "inherit");
	updateEditBox();
}

function editBoxToggle() {
  if(document.getElementById('editBox').style.display == "none") {
    editBoxShow();
  } else {
    editBoxHide();
  }
}

$(document).ready(function() {
	updateEditBox();
  editBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateEditBox();
}

function editOnClick() {
	var object = canvas.getActiveObject();
  if(isText(object)) {
    shapeBoxHide();
    backgroundBoxHide();
    imageBoxHide();
    clipartBoxHide();
    editBoxHide();
    textBoxShow();
  } else if(isClipart(object)) {
    shapeBoxHide();
    backgroundBoxHide();
    imageBoxHide();
    clipartBoxHide();
    editBoxHide();
    imageEditBoxShow();
  }
}
