
//parameters:
//	showColumns - number of columns shown per page (4 or 6)
//	page - the page to show
function clipartShowPage(showColumns, imageSize, page)
{
	//$("#innerContainer").offset({top:$("#clipartContainer").offset().top,left:$("#clipartContainer").offset().left + -imageSize * numColumns/numRows});

	//calculate offset for width and height
	//width = outer container position + (page * columns * imageSize)
	var offsetWidth = $("#clipartContainer").offset().left + -(page * showColumns * imageSize);
	var offsetHeight = $("#clipartContainer").offset().top;

	$("#innerContainer").offset({top:offsetHeight,left:offsetWidth});

	//get all of the page elements
	var x = $("[id*='page']");

	//set the active one to bold and larger font
	for(var i = 0; i < x.length; i++)
	{
		if(i == page)
			x[i].style = "font-weight: bold; font-size: 20px;"; //("font-weight", "bold");
		else
			x[i].style = "font-weight: normal; font-size: 14px;"; //x[i].css("font-weight", "normal");*/
	}

	//create the left and right page buttons
	var backImage = "../../img/down_img.png";
	var forwardImage = "../../img/up_img.png";

	//the width of the image we're using for the back and forwards buttons
	var imageWidth = 36;

	//remove any elements that are currently on the page
	$("#back").remove();
	$("#forward").remove();

	//don't add the back button if we're on page 0
	if(page != 0)
	{
		var backImageElement = '<img id="back" class="back" src="' + backImage + '" onclick="clipartShowPage(' + showColumns + ',' + imageSize + ',' + (page-1) + ');" />';
		$('body').append(backImageElement);

		//set the absolute offset of the image to the left center of the clipart container and half the height
		var backImageOffsetLeft = $("#clipartContainer").offset().left - (imageWidth / 2);
		var backImageOffsetTop = $("#clipartContainer").offset().top + ($("#clipartContainer").height() / 2) - (imageWidth/2);

		$("#back").offset({top:backImageOffsetTop,left:backImageOffsetLeft});
	}

	//don't add the forward button if we're on the last page.
	if(page < x.length - 1)
	{
		var forwardImageElement = '<img id="forward" class="forward" src="' + forwardImage + '" onclick="clipartShowPage(' + showColumns + ',' + imageSize + ',' + (page+1) + ');" />';
		$('body').append(forwardImageElement);

		//set the absolute offset of the image to the right center of the clipart container and half the height
		var forwardImageOffsetLeft = $("#clipartContainer").offset().left + $("#clipartContainer").width() - (imageWidth / 2);
		var forwardImageOffsetTop = $("#clipartContainer").offset().top + ($("#clipartContainer").height() / 2) - (imageWidth /2);

		$("#forward").offset({top:forwardImageOffsetTop,left:forwardImageOffsetLeft});
	}
}

function addSimpleClipart(name, clipartOptions, loadFromURL)
{
	var cWidth = canvas.getWidth();
	var cHeight = canvas.getHeight();
	var topPos = cHeight/2;
	var leftPos = cWidth/4.8;
	var clipartSize = topPos;
	if($('#stickertype').val() == 7) {
		clipartSize = leftPos;
	}

	for(var i = canvas.getObjects().length-1; i >= 0; i--){
      if(canvas.item(i).isType('image') || canvas.item(i)['isSVG'] == true ) {
        canvas.remove(canvas.item(i));
      }
    }


  if (name.substring(name.length-4, name.length) == '.svg')
  {
      fabric.loadSVGFromURL(name, function(objects, options){
        var object = new fabric.util.groupSVGElements(objects, options);
        object.set({
					left: leftPos,
  		  	top: topPos,
            originX: 'center',
            originY: 'center',
			lockScalingX: true,
	  		lockScalingY: true,
	  		selectable: false,
            //scale object to 200x200
            scaleX: clipartSize / options.width,
            scaleY: clipartSize / options.height
        });
		object.set(clipartOptions);
        // ensure object is uniformly scaled
        //var scale = object.scaleX;
        //if (scale < object.scaleY) {
        //  scale = object.scaleY;
        //}
        //object.scale(scale, scale);

        object['isSVG'] = true;

		//applySimpleClipartSettings(object);

        canvas.add(object);
		addSimpleText();
        //canvas.setActiveObject(object);
        canvas.renderAll();
    });
  }
  else {
	var currentImg;

	if (loadFromURL){
		// simon fix later - mmdev
		var newName = server_path + name;
		currentImg = new fabric.Image.fromURL(newName, function(currentImg) {
			currentImg.left = leftPos;
			currentImg.top = topPos;
			currentImg.originX = 'center';
			currentImg.originY = 'center';
			currentImg.padding = 10;
			canvas.setActiveObject(currentImg);

			currentImg.set(clipartOptions);

			canvas.add(currentImg);
			canvas.setActiveObject(currentImg);
			//orderLayers();
			canvas.renderAll();
		});
	}
	else {
		var imgElement;
		if (name.length > 10 && name.substring(0,10) == 'data:image')
		{
		  imgElement = new Image();
		  imgElement.src = name;
		}
		else
		{
		  imgElement = document.getElementsByName(name)[0];
		}

		currentImg = new fabric.Image(imgElement, {
		  left: leftPos,
		  top: topPos,
		  originX: 'center',
		  originY: 'center',
		  lockScalingX: true,
		  lockScalingY: true,
		  selectable: false,
		  scaleX: 1,
		  scaleY: 1,
		  height: clipartSize,
		  width: clipartSize

		});
		// ctest = currentImg;
		if (clipartOptions != null)
			currentImg.set(clipartOptions);

		canvas.add(currentImg);
		//orderLayers();
		//canvas.setActiveObject(currentImg);
		canvas.renderAll();
		addSimpleText();
		return currentImg;
	}
  }
}

function canColor(stickerType, colorValue){
	if(colorValue == 1 && (stickerType == 6 || stickerType == 7))
		return false;
	else if(colorValue == 2 && (stickerType >= 3 && stickerType <= 7))
		return false;
	return true;
}

function loadSimpleClipart(clipart, category) {

	var stickerType = $('#stickertype').val();
	//we want to present 12 items per page
	var numItemsToPresent = 12;

	//default to 3x4 when we have a nice wide page to lay them out on
	var numRows = 2;
	var showColumns = 8;

	//get the clipartContainer and its aspect ratio
	var clipartContainer = $("#clipartContainer");
	var containerAspectRatio = $("#clipartContainer").width() / $("#clipartContainer").height();

	//we're likely working with a mobile phone if this is true
	if(containerAspectRatio < 1.75)
	{
		//so now we want to lay the images out in 4x3
		numRows = 3;
		showColumns = 4;
	}

	//calculate the size of the images based on the number of columns and the width of the div
	var imageSize = $("#clipartContainer").width() / showColumns;

	//create style with width and height definitions
	var imageStyle = $('<style>.simpleClipart {width:' + imageSize + 'px; height:' + imageSize + 'px; padding: 10px; cursor: pointer; cursor: hand;}</style>');
	imageStyle.appendTo("head");

	//set the containers height based on imageSize and numRows
	clipartContainer.height(imageSize * numRows);

	//count the number of clipart in our category
	var numClipart = 0;
	for(var i = 0; i < clipart.length; i++)
	{
		if((clipart[i].category == category || clipart[i].subcat == category || clipart[i].subcat2 == category) && canColor(stickerType, clipart[i].can_color))
			numClipart++;
	}

	//we've got all the clipart links and our current category. let's load them
	var numColumns = Math.ceil(numClipart / numRows);

	console.log("numColumns: " + numColumns);

	//set the width and height of the innerContainer
	$("#innerContainer").width(numColumns * imageSize);
	$("#innerContainer").height(numRows * imageSize);

	//clear old images
	$("#innerContainer").html("");

	//using a temporary image in leau of having cliparts from the server
	var clipartLink = "../../img/add_clipart.png";
	var onclickTag = "";

	var numAdded = 0;
	for(var i = 0; i < clipart.length; i++)
	{
		if((clipart[i].category == category || clipart[i].subcat == category || clipart[i].subcat2 == category) && canColor(stickerType, clipart[i].can_color))
		{
			clipartLink = clipart[i].name;
			onclickTag = 'addSimpleClipart(\'' + clipart[i].name + '\')';
			if(numAdded % numColumns == 0 && numAdded != 0)
				$("#innerContainer").append('<br />');

			//once we have images we'll dynamically change clipartLink to the appropriate image
			//we'll also create an onclick to add it to the canvas.
			var img = '<img src="' + clipartLink + '" name="' + clipartLink + '" class="simpleClipart" onclick="' + onclickTag + '" />';
			$("#innerContainer").append(img);

			numAdded++;
		}
	}

	//now we need to be able to paginate
	var numPages = Math.ceil(numColumns / showColumns);

	//get all of the page elements
	var x = $("[id*='page']");
	x.remove();

	//adds links to the footer to select which page to go to
	//TODO: make these links images;
	for(var i = 0; i < numPages; i++)
	{
		$("#simpleClipartFooter").append('<a id="page' + i + '" class="simpleClipartFooterPage" onclick="clipartShowPage(' + showColumns + ',' + imageSize + ',' + i + ');">' + (i+1) +' </a> ');
	}

	clipartShowPage(showColumns, imageSize, 0);
}
