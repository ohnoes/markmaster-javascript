/*
 * Function
 */
function updateShapeBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var shapeBox = $(".shapeBox");
	shapeBox.css("width", canvasWidth);
	shapeBox.css("height", canvasHeight);
	shapeBox.offset({top: canvasOffset.top+20, left:20});
}

function shapeBoxHide() {
	$('.shapeBox').css("display", "none");
}

function shapeBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.shapeBox').css("display", "inherit");
	updateShapeBox();
}

function shapeBoxToggle() {
  if(document.getElementById('shapeBox').style.display == "none") {
    shapeBoxShow();
  } else {
    shapeBoxHide();
  }
}

$(document).ready(function() {
	updateShapeBox();
  shapeBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateShapeBox();
}
