/////////////////////////////////////////////////////////
//  Function to set the price                          //
/////////////////////////////////////////////////////////
function setPrice() {

if (document.getElementById('stickertype').value == 1 || document.getElementById('stickertype').value == 2 || document.getElementById('stickertype').value == 3 ||
  document.getElementById('stickertype').value == 4 || document.getElementById('stickertype').value == 5 || document.getElementById('stickertype').value == 8  ||
  document.getElementById('stickertype').value == 9 || document.getElementById('stickertype').value == 10) {
  var startPrice    = 1,
    quantityPrice = 2,
    areaPrice     = 600;

  if(document.getElementById('stickertype').value == 10) {
    areaPrice = 300;
  }

  var width = document.getElementById('width').value;
  var height = document.getElementById('height').value;
  var quantity = document.getElementById('quantity').value;

  if(quantity == "") {
      quantity = 1;
      document.getElementById('quantity').value = 1;
  }
  if (quantity < 1) {
    document.getElementById('quantity').value = 1;
    if(document.getElementById('quantity2') != null)
      document.getElementById('quantity2').value = 1;
  }

  if(quantity == 1){
    quantityPrice = 2.8;
   } else if(quantity == 2) {
    quantityPrice = 2.65;
   } else if(quantity == 3) {
    quantityPrice = 2.49;
   } else if(quantity == 4) {
    quantityPrice = 2.34;
   } else if(quantity == 5) {
    quantityPrice = 2.19;
   } else if(quantity == 6) {
    quantityPrice = 2.04;
   } else if(quantity == 7) {
    quantityPrice = 1.89;
   } else if(quantity == 8) {
    quantityPrice = 1.74;
   } else if(quantity == 9) {
    quantityPrice = 1.59;
   } else if(quantity == 10) {
    quantityPrice = 1.44;
   } else if(quantity == 11) {
    quantityPrice = 1.37;
   } else if(quantity == 12) {
    quantityPrice = 1.30;
   } else if(quantity == 13) {
    quantityPrice = 1.23;
   } else if(quantity == 14) {
    quantityPrice = 1.16;
   } else if(quantity == 15) {
    quantityPrice = 1.09;
   } else if(quantity == 16) {
    quantityPrice = 1.05;
   } else if(quantity == 17) {
    quantityPrice = 1.01;
   } else if(quantity == 18) {
    quantityPrice = 0.97;
   } else if(quantity == 19) {
    quantityPrice = 0.93;
   } else if(quantity == 20) {
    quantityPrice = 0.89;
   } else if(quantity == 21) {
    quantityPrice = 0.87;
   } else if(quantity == 22) {
    quantityPrice = 0.85;
   } else if(quantity == 23) {
    quantityPrice = 0.83;
   } else if(quantity == 24) {
    quantityPrice = 0.81;
   } else if(quantity == 25) {
    quantityPrice = 0.79;
   } else if(quantity == 26) {
    quantityPrice = 0.77;
   } else if(quantity == 27) {
    quantityPrice = 0.75;
   } else if(quantity == 28) {
    quantityPrice = 0.73;
   } else if(quantity == 29) {
    quantityPrice = 0.71;
   } else if(quantity == 30) {
    quantityPrice = 0.69;
   } else if(quantity == 31) {
    quantityPrice = 0.68;
   } else if(quantity == 32) {
    quantityPrice = 0.67;
   } else if(quantity == 33) {
    quantityPrice = 0.66;
   } else if(quantity == 34) {
    quantityPrice = 0.65;
   } else if(quantity == 35) {
    quantityPrice = 0.64;
   } else if(quantity == 36) {
    quantityPrice = 0.63;
   } else if(quantity == 37) {
    quantityPrice = 0.62;
   } else if(quantity == 38) {
    quantityPrice = 0.61;
   } else if(quantity == 39) {
    quantityPrice = 0.6;
   } else if(quantity == 40) {
    quantityPrice = 0.59;
   } else if(quantity == 41) {
    quantityPrice = 0.58;
   } else if(quantity == 42) {
    quantityPrice = 0.57;
   } else if(quantity == 43) {
    quantityPrice = 0.56;
   } else if(quantity == 44) {
    quantityPrice = 0.55;
   } else if(quantity == 45) {
    quantityPrice = 0.54;
   } else if(quantity == 46) {
    quantityPrice = 0.53;
   } else if(quantity == 47) {
    quantityPrice = 0.52;
   } else if(quantity == 48) {
    quantityPrice = 0.51;
   } else if(quantity == 49) {
    quantityPrice = 0.50;
   } else if(quantity == 50) {
    quantityPrice = 0.49;
   } else if(quantity == 51) {
    quantityPrice = 0.485;
   } else if(quantity == 52) {
    quantityPrice = 0.48;
   } else if(quantity == 53) {
    quantityPrice = 0.475;
   } else if(quantity == 54) {
    quantityPrice = 0.47;
   } else if(quantity == 55) {
    quantityPrice = 0.465;
   } else if(quantity == 56) {
    quantityPrice = 0.46;
   } else if(quantity == 57) {
    quantityPrice = 0.455;
   } else if(quantity == 58) {
    quantityPrice = 0.45;
   } else if(quantity == 59) {
    quantityPrice = 0.445;
   } else if(quantity == 60) {
    quantityPrice = 0.44;
   } else if(quantity == 61) {
    quantityPrice = 0.435;
   } else if(quantity == 62) {
    quantityPrice = 0.43;
   } else if(quantity == 63) {
    quantityPrice = 0.425;
   } else if(quantity == 64) {
    quantityPrice = 0.42;
   } else if(quantity == 65) {
    quantityPrice = 0.415;
   } else if(quantity == 66) {
    quantityPrice = 0.41;
   } else if(quantity == 67) {
    quantityPrice = 0.405;
   } else if(quantity == 68) {
    quantityPrice = 0.4;
   } else if(quantity == 69) {
    quantityPrice = 0.395;
   } else if(quantity == 70) {
    quantityPrice = 0.39;
   } else if(quantity == 71) {
    quantityPrice = 0.386;
   } else if(quantity == 72) {
    quantityPrice = 0.382;
   } else if(quantity == 73) {
    quantityPrice = 0.378;
   } else if(quantity == 74) {
    quantityPrice = 0.374;
   } else if(quantity == 75) {
    quantityPrice = 0.37;
   } else if(quantity == 76) {
    quantityPrice = 0.366;
   } else if(quantity == 77) {
    quantityPrice = 0.362;
   } else if(quantity == 78) {
    quantityPrice = 0.358;
   } else if(quantity == 79) {
    quantityPrice = 0.354;
   } else if(quantity == 80) {
    quantityPrice = 0.35;
   } else if(quantity == 81) {
    quantityPrice = 0.347;
   } else if(quantity == 82) {
    quantityPrice = 0.344;
   } else if(quantity == 83) {
    quantityPrice = 0.341;
   } else if(quantity == 84) {
    quantityPrice = 0.338;
   } else if(quantity == 85) {
    quantityPrice = 0.335;
   } else if(quantity == 86) {
    quantityPrice = 0.332;
   } else if(quantity == 87) {
    quantityPrice = 0.329;
   } else if(quantity == 88) {
    quantityPrice = 0.326;
   } else if(quantity == 89) {
    quantityPrice = 0.323;
   } else if(quantity == 90) {
    quantityPrice = 0.32;
   } else if(quantity == 91) {
    quantityPrice = 0.318;
   } else if(quantity == 92) {
    quantityPrice = 0.316;
   } else if(quantity == 93) {
    quantityPrice = 0.314;
   } else if(quantity == 94) {
    quantityPrice = 0.312;
   } else if(quantity == 95) {
    quantityPrice = 0.31;
   } else if(quantity == 96) {
    quantityPrice = 0.308;
   } else if(quantity == 97) {
    quantityPrice = 0.306;
   } else if(quantity == 98) {
    quantityPrice = 0.304;
   } else if(quantity == 99) {
    quantityPrice = 0.302;
   } else {
    quantityPrice = 0.3;
   }

  var materialPrice = width * height * quantity * areaPrice / 1000000;

  var price = startPrice + (quantity * quantityPrice) + materialPrice;
  price = price * 1.25;
  price = Math.round((price + 0.00001) * 100) / 100;
  price = price.toFixed(2);

  if (stickerType == 9) {
    price = price * 1.5;
    price = price.toFixed(2);
  }

  document.getElementById('priceHolder').value = price;
  document.getElementById('postprice').value = price;
  $('#stickerprice').val(price);
} else {
  var quantity = document.getElementById('quantity').value;
  var width = document.getElementById('width').value;

  if (quantity == 1) {
    if (width == 37) {
      price = 1.95;
    } else if (width == 60) {
      price = 2.85;
    } else if (width == 30) {
      price = 2.10;
    }
  } else if (quantity == 5) {
    if (width == 37) {
      price = 9.75;
    } else if (width == 60) {
      price = 14.00;
    } else if (width == 30) {
      price = 10.40;
    }
  } else if (quantity == 10) {
    if (width == 37) {
      price = 19.00;
    } else if (width == 60) {
      price = 28.00;
    } else if (width == 30) {
      price = 20.00;
    }
  } else if (quantity == 20) {
    if (width == 37) {
      price = 35.00;
    } else if (width == 60) {
      price = 57.00;
    } else if (width == 30) {
      price = 37.00;
    }
  } else if (quantity == 50) {
    if (width == 37) {
      price = 55.00;
    } else if (width == 60) {
      price = 104.00;
    } else if (width == 30) {
      price = 54.00;
    }
  } else if (quantity == 75) {
    if (width == 37) {
      price = 64.00;
    } else if (width == 60) {
      price = 143.00;
    } else if (width == 30) {
      price = 73.00;
    }
  } else if (quantity == 100) {
    if (width == 37) {
      price = 73.00;
    } else if (width == 60) {
      price = 169.00;
    } else if (width == 30) {
      price = 88.00;
    }
  } else if (quantity == 250) {
    if (width == 37) {
      price = 120.00;
    } else if (width == 60) {
      price = 260.00;
    } else if (width == 30) {
      price = 138.00;
    }
  } else if (quantity == 500) {
    if (width == 37) {
      price = 182.00;
    } else if (width == 60) {
      price = 396.00;
    } else if (width == 30) {
      price = 247.00;
    }
  } else if (quantity == 1000) {
    if (width == 37) {
      price = 286.00;
    } else if (width == 60) {
      price = 546.00;
    } else if (width == 30) {
      price = 409.00;
    }
  }


  document.getElementById('priceHolder').value = price;
  document.getElementById('postprice').value = price;
  $('#stickerprice').val(price);

  }
  currencyPrice();
}

function currencyPrice(){

    var currencyType = document.getElementById('thecurrency').value; // holds the currency type
    var newcurr = document.getElementById('currencycheck').value; // holds the currency type value
    var oldPrice = document.getElementById('priceHolder').value;
    var stickerpholder = document.getElementById('stickerpriceholder').value;

    var price = oldPrice/newcurr;

    if(currencyType == 0) {
      document.getElementById('thePrice').innerHTML = price.toFixed(2) + ' kr';
      document.getElementById('thePrice2').innerHTML = price.toFixed(2) + ' kr';
    } else if (currencyType == 1)  {
      document.getElementById('thePrice').innerHTML =  '$' + price.toFixed(2);
      document.getElementById('thePrice2').innerHTML = '$' + price.toFixed(2);
    } else if (currencyType == 2)  {
      document.getElementById('thePrice').innerHTML =  price.toFixed(2) + ' kr';
      document.getElementById('thePrice2').innerHTML = price.toFixed(2) + ' kr';
    } else if (currencyType == 3)  {
      document.getElementById('thePrice').innerHTML = price.toFixed(2) + ' kr';
      document.getElementById('thePrice2').innerHTML = price.toFixed(2) + ' kr';
    } else if (currencyType == 4)  {
      document.getElementById('thePrice').innerHTML = '€' + price.toFixed(2);
      document.getElementById('thePrice2').innerHTML = '€' + price.toFixed(2);
    }


}

$('#Antall').on('change', function(){
  if(this.value == 1) {
	index = 0;
  } else if(this.value == 5) {
	index = 1;
  } else if(this.value == 10) {
	index = 2;
  } else if(this.value == 20) {
	index = 3;
  } else if(this.value == 50) {
	index = 4;
  } else if(this.value == 75) {
	index = 5;
  } else if(this.value == 100) {
	index = 6;
  } else if(this.value == 250) {
	index = 7;
  } else if(this.value == 500) {
	index = 8;
  } else if(this.value == 1000) {
	index = 9;
  }
  document.getElementById("quantity").value = this.value;
  document.getElementById("postquantity").value = this.value;
  setPrice();
});
