function canvasToImage(source){
  //original height and width of the background
  var width = source.backgroundImage.width;
  var height = source.backgroundImage.height;

  source.backgroundImage.strokeWidth = 5;
  source.overlayImage.opacity = 1;
  source.deactivateAll().renderAll();

  var extraPadding = 0;

  //The location of the sticker in the canvas
  var pathStartX = 900 - (450 + ((width+extraPadding) / 2));
  var pathStartY = 450 - (225 + ((height+extraPadding) / 2));

  //get the image data from the canvas and crop it so that its only the sticker
  var imgData = source.toDataURL({
    format: 'png',
    multiplier: 0.2,
    width: width+5,
    height: height+5,
    left: pathStartX,
    top: pathStartY
  });

  return imgData;
}

function createSticker(event) {
   console.log('Sticker called');
   var objects = canvas.getObjects();
   for(var i = objects.length-1; i >= 0; i--) {
     // text
     if (isText(objects[i])) {
       objects[i].selectable = true;
     } else if (isImg(objects[i])) {
       objects[i].selectable = true;
     } else if(isSVG(objects[i])) {
       objects[i].selectable = true;
     }
   }

    event.preventDefault();
    canvas.deactivateAll();

    updatePostValues();
    var postjson = document.getElementById('postjson').value;
    var postquantity = document.getElementById('postquantity').value;
    var postwidth = document.getElementById('postwidth').value;
    var postheight = document.getElementById('postheight').value;
    var postprice = document.getElementById('postprice').value;
    var postuserid = 0;
    var stickerUpdate = document.getElementById('edit').value;
    var stickerid = document.getElementById('stickerid').value;
    var postthumbnail = canvasToImage(canvas);
    var output=postthumbnail.replace(/^data:image\/(png|jpg);base64,/, "");
    var test1;

    console.log("THIS IS THE STICKERTYPE: " + stickerType);
    var THE_TOKEN = $('meta[name="csrf-token"]').attr('content');
    console.log("This is the token here: " + THE_TOKEN);
    var _TOKEN = $('[name="_token"]').val();;
    console.log("The _TOKEN: " + _TOKEN);

    var formData = new FormData($('#addToCart')[0]);
    formData.append('thumbnail', output);
	  formData.append('stickerType', stickerType);
    formData.append('json', postjson);
    formData.append('quantity', postquantity);
    formData.append('width', postwidth);
    formData.append('height', postheight);
    formData.append('price', postprice);
    formData.append('update', stickerUpdate);
    formData.append('userid', postuserid);
    formData.append('stickerid', stickerid);
    $.ajax({
        type: 'post',
        url: 'addtocart',
        beforeSend: function (xhr) {
          var token = $('meta[name="csrf-token"]').attr('content');

          if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token);
          }
         },
        cache: false,
        contentType: false,
        processData: false,
        dataType: 'html',
        data: formData,
        beforeSend: function() {
			      showPopup();
            $("#validation-errors").hide().empty();
			// console.log("sending clipart");
        },
        success: function(data) {
			moreToLoad = true;
			hidePopup();
			// console.log(data);
			test1=data;
			// console.log(data.length);
		  if (data.search("failed") > -1)
		  {
        var objects = canvas.getObjects();
        for(var i = objects.length-1; i >= 0; i--) {
          // text
          if (isText(objects[i])) {
            objects[i].selectable = false;
          } else if (isImg(objects[i])) {
            objects[i].selectable = false;
          } else if(isSVG(objects[i])) {
            objects[i].selectable = false;
          }
        }
			putMessage(data.split("failed")[1]);
		  }
		  else {
			  // SUCCESS MOVE TO CART
        var redir = document.getElementById('redirect').value;
        window.location.replace(server_path + redir);
		  }
        },
        error: function(xhr, textStatus, thrownError) {
			hidePopup();
            console.error('AJAX error Something went to wrong.Please Try again later...');
            console.error(xhr);
            console.error(textStatus);
            console.error(thrownError);
        }
    });
}

//$('#cilpartForm').submit(uploadClipart(clipartType));
$('#addToCart').submit(createSticker);

function updatePostValues() {

  jsonify();
  setPrice();
  var postquantity = document.getElementById('postquantity');
  postquantity.value = document.getElementById('quantity').value;
  var postwidth = document.getElementById('postwidth');
  postwidth.value = document.getElementById('width').value;
  var postheight = document.getElementById('postheight');
  postheight.value = document.getElementById('height').value;



}
