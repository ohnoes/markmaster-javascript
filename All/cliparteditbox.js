/*
 * Function
 */
function updateImageEditBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var imageEditBox = $(".imageEditBox");
	imageEditBox.css("width", canvasWidth);
	imageEditBox.css("height", canvasHeight);
	imageEditBox.offset({top: canvasOffset.top+20, left:20});
}

function imageEditBoxHide() {
	$('.imageEditBox').css("display", "none");
}

function imageEditBoxShow() {
	shapeBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	clipartBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.imageEditBox').css("display", "inherit");
	updateImageEditBox();
}

function imageEditBoxToggle() {
  if(document.getElementById('imageEditBox').style.display == "none") {
    imageEditBoxShow();
  } else {
    imageEditBoxHide();
  }
}

$(document).ready(function() {
	updateImageEditBox();
  imageEditBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateImageEditBox();
}
