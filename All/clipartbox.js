/*
 * Function
 */
function updateImageBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var imageBox = $(".imageBox");
	imageBox.css("width", canvasWidth);
	imageBox.css("height", canvasHeight);
	imageBox.offset({top: canvasOffset.top+20, left:20});
}

function imageBoxHide() {
	$('.imageBox').css("display", "none");
}

function imageBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	$('.imageBox').css("display", "inherit");
	updateImageBox();
}

function imageBoxToggle() {
  if(document.getElementById('imageBox').style.display == "none") {
    imageBoxShow();
  } else {
    imageBoxHide();
  }
}

$(document).ready(function() {
	updateImageBox();
  imageBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateImageBox();
}
