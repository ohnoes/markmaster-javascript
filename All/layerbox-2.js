/*
 * Function
 */
function updateLayerBox() {
	//get the tools-design-canvas element's width, height, top and left
	var canvas = $(".design-tools-canvas");
	var canvasWidth = canvas.width();
	var canvasHeight = canvas.height();
	var canvasOffset = canvas.offset();

	//then we'll set its properties
	var layerBox = $(".layerBox");
	layerBox.css("width", canvasWidth);
	layerBox.css("height", canvasHeight);
	layerBox.offset({top: canvasOffset.top+20, left:20});
}

function layerBoxHide() {
	$('.layerBox').css("display", "none");
}

function layerBoxShow() {
	shapeBoxHide();
	nbackgroundBoxHide();
	backgroundBoxHide();
	textBoxHide();
	imageBoxHide();
	imageEditBoxHide();
	editBoxHide();
	layerBoxHide();
	function1();
	$('.layerBox').css("display", "inherit");
	updateLayerBox();
}

function layerBoxToggle() {
  if(document.getElementById('layerBox').style.display == "none") {
    layerBoxShow();
  } else {
    layerBoxHide();
  }
}

$(document).ready(function() {
	updateLayerBox();
  layerBoxHide();
});

var id;
$(window).resize(function() {
	clearTimeout(id);
	id = setTimeout(doneResizing, 500);
});

function doneResizing() {
	updateLayerBox();
}

function function1() {
	var ul = document.getElementById("sortable");
	// clear the old list
	$(ul).empty();

	canvas.deactivateAll();
	canvas.renderAll();
	var object;
	var counter = 0;

  var objects = canvas.getObjects();
	for(var i = objects.length-1; i >= 0; i--) {
		// text
		if (isText(objects[i])) {
			if (objects[i].text.length > 10) {
				var layerText = objects[i].text.substr(0,9) + "...";
			} else {
				var layerText = objects[i].text;
			}
			var li = document.createElement("li");
			var bup = document.createElement("b");
			var bdown = document.createElement("b");
			var btext = document.createElement("b");
			var block = document.createElement("b");
			bup.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-up");
			bdown.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-down");
			bup.setAttribute("style", "float: left; line-height: 45px;");
			bdown.setAttribute("style", "float: left; line-height: 45px;");
			bup.setAttribute("id", "up"+counter);
			bup.setAttribute("onclick", "layerUp('up" + counter + "')");
			bdown.setAttribute("id", "down"+counter);
			bdown.setAttribute("onclick", "layerDown('down" + counter + "')");
			btext.setAttribute("style", "float: left; line-height: 45px;");
			btext.appendChild(document.createTextNode(layerText));
			//barrow.setAttribute("class", "fa fa-fw fa-lg fa-arrows-v");
			//barrow.setAttribute("style", "float: left;");
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right; line-height: 45px;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(bup);
			li.appendChild(bdown);
			li.appendChild(btext);
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		} else if (isImg(objects[i])) { // clipart
			var layerImg = objects[i]._element.currentSrc;
			var oImg = document.createElement("img");
			oImg.setAttribute('src', layerImg);
			oImg.setAttribute('alt', 'na');
			oImg.setAttribute('height', '40px');
			oImg.setAttribute("style", "padding-top: 2.5px;");
			var li = document.createElement("li");
			var bup = document.createElement("b");
			var bdown = document.createElement("b");
			var block = document.createElement("b");
			bup.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-up");
			bdown.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-down");
			bup.setAttribute("style", "float: left; line-height: 45px;");
			bdown.setAttribute("style", "float: left; line-height: 45px;");
			bup.setAttribute("id", "up"+counter);
			bup.setAttribute("onclick", "layerUp('up" + counter + "')");
			bdown.setAttribute("id", "down"+counter);
			bdown.setAttribute("onclick", "layerDown('down" + counter + "')");
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right; line-height: 45px;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(bup);
			li.appendChild(bdown);
			li.appendChild(oImg);
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		} else if(isSVG(objects[i])) {
			var layerText = 'SVG';
			if(objects[i].currentSrc) {
				var layerImg = objects[i].currentSrc;
				var oImg = document.createElement("img");
				oImg.setAttribute('src', layerImg);
				oImg.setAttribute('alt', 'na');
				oImg.setAttribute('height', '40px');
				oImg.setAttribute("style", "padding-top: 2.5px;");
			}
			var li = document.createElement("li");
			var bup = document.createElement("b");
			var bdown = document.createElement("b");
			var btext = document.createElement("b");
			var block = document.createElement("b");
			bup.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-up");
			bdown.setAttribute("class", "fa fa-fw fa-lg fa-arrow-circle-down");
			bup.setAttribute("style", "float: left; line-height: 45px;");
			bdown.setAttribute("style", "float: left; line-height: 45px;");
			bup.setAttribute("id", "up"+counter);
			bup.setAttribute("onclick", "layerUp('up" + counter + "')");
			bdown.setAttribute("id", "down"+counter);
			bdown.setAttribute("onclick", "layerDown('down" + counter + "')");
			btext.setAttribute("style", "float: left; line-height: 45px;");
			btext.appendChild(document.createTextNode(layerText));
			if(objects[i].selectable == true) {
				block.setAttribute("class", "fa fa-fw fa-lg fa-unlock-alt");
			} else {
				block.setAttribute("class", "fa fa-fw fa-lg fa-lock");
			}
			block.setAttribute("style", "float: right; line-height: 45px;");
			block.setAttribute("id", "lock"+counter);
			block.setAttribute("onclick", "toggleLock('lock" + counter + "')");
			li.appendChild(bup);
			li.appendChild(bdown);33
			if(objects[i].currentSrc) {
				li.appendChild(oImg);
			} else {
				li.appendChild(btext);
			}
			li.appendChild(block);
			li.setAttribute("class", "ui-state-default");
			li.setAttribute("id", counter);
			ul.appendChild(li);
		}
		counter++;
	}

}
