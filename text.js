/////////////////////////////////////////////////////////
//        tab   3 - add text
/////////////////////////////////////////////////////////

var minFontScale = 1.5;

canvas.on('object:scaling', function(event){

    // lower limit = 6 * 5 = 30;
    // setting fontSize to 20, lower scale limit is now 1.5
    var obj = event.target;
    if (isText(obj)) {
        if(obj.scaleX < minFontScale || obj.scaleY < minFontScale) {
            obj.scaleX = minFontScale;
            obj.scaleY = minFontScale;
        }
    }
});

// scales text to match proportions of sticker
function updateTextSize()
{
	var cWidth = canvas.getWidth();
	var cHeight = canvas.getHeight();
	
	var width = validateWidth(document.getElementById('width').value);
	document.getElementById('width').value = width;
	var height = validateWidth(document.getElementById('height').value);
	document.getElementById('height').value = height;
	
	var mmLimit;
	
	if (width > height)
	  {
		mmLimit = width/cWidth;
	  }
	  else
	  {
		mmLimit = height/cHeight;
	  }
	
	minFontScale = 2.47 / (26 * mmLimit);
	
  canvas.forEachObject(function(obj){
      if (isText(obj)) {

        var fontSize = obj.scaleX;
        if (fontSize < minFontScale) {
          fontSize = minFontScale;
          // alert('too small');
        }
        obj.scaleX = fontSize;
        obj.scaleY = fontSize;

        if (canvas.getActiveObject() == obj)
        {
          // update text controls
         // $('#fontSizeSlider').val(obj.fontSize);
          $('#fontSizeTextbox').html(obj.scaleX);
        }
        // obj.fontSize

        // obj.scale(pt2mm * mm2px, pt2mm * mm2px);
      }
  });
}


function setText() {

  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  var text = document.getElementById('stickerText').value;

  if (isText(currentObj))
  {
    currentObj.setText(text);
  }
  else if (isGroup(currentGroup)) {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      if (isText(groupArr[i])) {
        groupArr[i].text = text;
      }
    }
  }
  canvas.renderAll();
}


// add text
function setTextInput() {
  var text = document.getElementById('stickerText').value;
  console.log('Text: ' + text);
  var currentText = new fabric.IText(text,
    { left: 100,
      top: 100,
      originX: 'center',
      originY: 'center',
      textAlign: 'center',
      fontSize: 20
    });

  // if(document.getElementById('strokeWidth').value == 0) { // to remove the outline if the slider is at 0
  //   currentText.stroke = document.getElementById('fill').value;
  // }
  canvas.add(currentText);
  canvas.renderAll();
  canvas.calcOffset();
  updateTextInput(currentText);
  //orderLayers();
}

// add text
function updateTextInput(textObj) {
  var currentText = textObj;

  currentText.padding = 10;

  applyTextSettings(currentText);

  var firstFont = $('#fontSelect').css('font-family');
  var testFont = firstFont.split(", ");
  var theFont = testFont[1];

  currentText.fontFamily = theFont;
  currentText.scaleX = document.getElementById('fontSizeTextbox').value;
  currentText.scaleY = document.getElementById('fontSizeTextbox').value;
  currentText.lineHeight = document.getElementById('lineHeightTextbox').value;
  currentText.fill = document.getElementById('fill').value;
  currentText.stroke = document.getElementById('stroke').value;
  currentText.strokeWidth = parseFloat(document.getElementById('strokeWidthTextbox').value);
  // if(document.getElementById('strokeWidth').value == 0) { // to remove the outline if the slider is at 0
  //   currentText.stroke = document.getElementById('fill').value;
  // }

  currentText.center();
  currentText.setCoords();
  // canvas.on('object:selected', objectSelected);
  //updateTextSize();
  canvas.setActiveObject(currentText);
  canvas.renderAll();
  canvas.calcOffset();

  //orderLayers();
}

function changeTextFont(selectedFont) {

    var testFont = selectedFont.split(", ");
    var theFont = testFont[1];

    var obj = canvas.getActiveObject();
    if(isText(obj)) {
        obj.fontFamily = theFont;
        // canvas.on('object:selected', objectSelected);
        //updateTextSize();
        canvas.setActiveObject(obj);
        canvas.renderAll();
        canvas.calcOffset();
    }

}

function applyTextSettings(textObj)
{

	textObj.setControlsVisibility({
        mr: false,
        ml: false,
        mt: false,
        mb: false
  });

	textObj.set({
	  borderColor: 'black',
	  cornerColor: 'black',
	  transparentCorners: false
	});

	if(outlineOptions.fill == 'black' || outlineOptions.fill == '#000000')
	{
		textObj.set({
		  borderColor: 'red',
		  cornerColor: 'red',
		  transparentCorners: false
		});
	}
}

function setTextColor(textColor) {

  var currentObj = canvas.getActiveObject();
  if (isText(currentObj))
  {
    currentObj.fill = textColor;
    canvas.renderAll();
  }
}


function setTextColorInput() {

  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS
  var textColor = document.getElementById('textColorInput').value;
  if (textColor[0] != '#')
    textColor = '#' + textColor;
  var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(textColor);
  if (!isOk)
    return;
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS

  var styleString = "background:" + textColor;
  var customColor = document.getElementById('customTextColor');
  customColor.style.cssText = 'background:'+ textColor;
  setTextColor(textColor);
  canvas.renderAll();
}

function setStrokeColor(strokeColor) {

  var currentObj = canvas.getActiveObject();
  if (isText(currentObj))
  {
    currentObj.stroke = strokeColor;
    canvas.renderAll();
  }
}


function setStrokeColorInput() {

  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS
  var strokeColor = document.getElementById('strokeColorInput').value;
  if (strokeColor[0] != '#')
    strokeColor = '#' + strokeColor;
  var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(strokeColor);
  if (!isOk)
    return;
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS


  var styleString = "background:" + strokeColor;
  var customColor = document.getElementById('customStrokeColor');
  customColor.style.cssText = 'background:'+ strokeColor;
  setStrokeColor(strokeColor);
  canvas.renderAll();
}


function deleteText() {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();
  if (isText(currentObj))
  {
    var yes = confirm("Are you sure you want to delete the selected text?");
    if (yes)
    {
      currentObj.remove();
    }
  }
  else if (isGroup(currentGroup))
  {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].remove();
    }
    canvas.discardActiveGroup();
  }
  //orderLayers();
  canvas.renderAll();
  canvas.calcOffset();
}

function isText(obj)
{
  return obj != null && obj.text != null;
}

function isGroup(obj)
{
  return obj != null && obj._objects != null;
}

function isImg(obj)
{
  return obj != null && obj._element != null;
}

function isSVG(obj)
{
  return obj != null && obj['isSVG'] == true;
}

function isClipart(obj)
{
	return isImg(obj) || isSVG(obj);
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// function onFontSizeChange()
// {
  // var fontSizeSlider = $('#fontSizeSlider');
  // $('#fontSizeTextbox').val(fontSizeSlider.val());

  // var currentObj = canvas.getActiveObject();
  // if (!isText(currentObj))
    // return;

  // currentObj.fontSize = fontSizeSlider.val();
  // updateTextSize();
  // canvas.renderAll();
// }

// $('#fontSizeSlider').on('input', onFontSizeChange);
// $('#fontSizeSlider').on('change', onFontSizeChange);

var inter = null;

function increaseFont()
{
	var currentFont = $('#fontSizeTextbox').val();
	var newFont = parseFloat(currentFont) + 0.1;
	$('#fontSizeTextbox').val(newFont.toFixed(1));

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.scaleX = newFont;
    currentObj.scaley = newFont;
	updateTextSize();
	canvas.renderAll();
}

function decreaseFont()
{
	var currentFont = $('#fontSizeTextbox').val();
	var newFont = parseFloat(currentFont) - 0.1;
	if (currentFont <= minFontSize){
		newFont = currentFont;
	} else {
		$('#fontSizeTextbox').val(newFont.toFixed(1));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

    currentObj.scaleX = newFont;
    currentObj.scaley = newFont;
	updateTextSize();
	canvas.renderAll();
}

function increaseTextStroke()
{
	var currentFont = $('#strokeWidthTextbox').val();
	var newFont = parseFloat(currentFont) + 0.01;
	if (newFont > 1.0) {
		newFont = 1.0;
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	} else {
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.strokeWidth = newFont;
	updateTextSize();
	canvas.renderAll();
}

function decreaseTextStroke()
{
	var currentFont = $('#strokeWidthTextbox').val();
	var newFont = parseFloat(currentFont) - 0.01;
	if (newFont < 0.01) {
		newFont = 0.01;
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	} else {
		$('#strokeWidthTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.strokeWidth = newFont;
	updateTextSize();
	canvas.renderAll();
}

function increaseLineHeight() // max value 2
{
	var currentFont = $('#lineHeightTextbox').val();
	var newFont = parseFloat(currentFont) + 0.01;
	if (newFont > 2){
		newFont = currentFont;
	} else {
		$('#lineHeightTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.lineHeight = newFont;
	updateTextSize();
	canvas.renderAll();
}

function decreaseLineHeight() // min value 0
{
	var currentFont = $('#lineHeightTextbox').val();
	var newFont = parseFloat(currentFont) - 0.01;
	if (newFont < 0){
		newFont = currentFont;
	} else {
		$('#lineHeightTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.lineHeight = newFont;
	updateTextSize();
	canvas.renderAll();
}

function increaseTextRotation() { // max value 359.9, at 360 we set it to 0
	var currentFont = $('#textAngleTextbox').val();
	var newFont = parseFloat(currentFont) + 0.10;

	if (newFont > 359.90) {
		newFont = 0;
		$('#textAngleTextbox').val(newFont.toFixed(2));
	} else {
		$('#textAngleTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.rotate(newFont);
	updateTextSize();
	canvas.renderAll();
}

function decreaseTextRotation() { // min value 0, if we go any lower, we set it to 359.9
	var currentFont = $('#textAngleTextbox').val();
	var newFont = parseFloat(currentFont) - 0.10;

	if (newFont < 0.00) {
		newFont = 359.9;
		$('#textAngleTextbox').val(newFont.toFixed(2));
	} else {
		$('#textAngleTextbox').val(newFont.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (!isText(currentObj))
		return;

	currentObj.rotate(newFont);
	updateTextSize();
	canvas.renderAll();
}

function increaseObjectScale() {
	var currentArt =  $('#clipartScaleTextbox').val();
	var newArt = parseFloat(currentArt) + 0.01;

	$('#clipartScaleTextbox').val(newArt.toFixed(2));

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.scale(currentObj.scaleXStart*newArt, currentObj.scaleYStart*newArt);
	canvas.renderAll();
}

function decreaseObjectScale() {
	var currentArt =  $('#clipartScaleTextbox').val();
	var newArt = parseFloat(currentArt) - 0.01;
	if (newArt < 0.01) {
		newArt = 0.01
	}
	$('#clipartScaleTextbox').val(newArt.toFixed(2));

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.scale(currentObj.scaleXStart*newArt, currentObj.scaleYStart*newArt);
	canvas.renderAll();
}

function increaseObjectRotation() {
	var currentArt =  $('#clipartAngleTextbox').val();
	var newArt = parseFloat(currentArt) + 0.10;

	if (newArt > 359.9) {
		newArt = 0;
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	} else {
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.rotate(newArt);
	canvas.renderAll();
}

function decreaseObjectRotation() {
	var currentArt =  $('#clipartAngleTextbox').val();
	var newArt = parseFloat(currentArt) - 0.10;

	if (newArt < 0) {
		newArt = 359.9;
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	} else {
		$('#clipartAngleTextbox').val(newArt.toFixed(2));
	}

	var currentObj = canvas.getActiveObject();
	if (currentObj == null)
		return;

	currentObj.rotate(newArt);
	canvas.renderAll();
}

function moveObjectLeft() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.left -= 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.left -= 1;
	}
	canvas.renderAll();
}

function moveObjectRight() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.left += 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.left += 1;
	}
	canvas.renderAll();
}

function moveObjectTop() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.top -= 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.top -= 1;
	}
	canvas.renderAll();
}

function moveObjectDown() {
	var currentObj = canvas.getActiveObject();
	var currentGroup = canvas.getActiveGroup();
	if (currentObj != null)
	{
		currentObj.top += 1;
	}
	else if (isGroup(currentGroup))
	{
		currentGroup.top += 1;
	}
	canvas.renderAll();
}



function onFontSizeTextInput()
{
  var fontSize = $('#fontSizeTextbox').val();
  if (isNumber(fontSize) && minFontScale <= fontSize)
  {
    //$('#fontSizeSlider').val(fontSize);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.scaleX = fontSize;
    updateTextSize();
    canvas.renderAll();
  }
}

function onFontSizeTextChange()
{
  var fontSizeText = $('#fontSizeTextbox');
  var fontSize = fontSizeText.val();
  if (!isNumber(fontSize))
  {
    fontSizeText.val(2.5);
  }
  else if (fontSize < minFontSize)
  {
    fontSizeText.val(minFontSize);
  }

  onFontSizeTextInput();
}

$('#fontSizeTextbox').on('input', onFontSizeTextInput);
$('#fontSizeTextbox').on('change', onFontSizeTextChange);

function onLineHeightTextInput() {
  var lineHeight = $('#lineHeightTextbox').val();
  if (isNumber(lineHeight) && minLineHeight <= lineHeight && lineHeight <= maxLineHeight)
  {
    //$('#lineHeightSlider').val(lineHeight);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.lineHeight = lineHeight;
    canvas.renderAll();
  }
}


function onLineHeightTextChange()
{
  var lineHeightText = $('#lineHeightTextbox');
  var lineHeight = lineHeightText.val();
  if (!isNumber(lineHeight))
  {
    lineHeightText.val(1);
  }
  else if (lineHeight < minLineHeight)
  {
    lineHeightText.val(minLineHeight);
  }
  else if (lineHeight > maxLineHeight)
  {
    lineHeightText.val(maxLineHeight);
  }
  onLineHeightTextInput();
}

$('#lineHeightTextbox').on('input', onLineHeightTextInput);
$('#lineHeightTextbox').on('change', onLineHeightTextChange);


function onStrokeWidthInput() {
  var strokeWidth = $('#strokeWidthTextBox').val();
  if (isNumber(strokeWidth) && minStrokeWidth <= strokeWidth )
  {
    $('#strokeWidthTextbox').val(strokeWidth);


    var currentObj = canvas.getActiveObject();
    if (!isText(currentObj))
      return;

    currentObj.strokeWidth = strokeWidth;
	//updateTextSize();
    canvas.renderAll();
  }
}

function onStrokeWidthChange() {
  var strokeWidthText = $('#strokeWidthTextbox');
  var strokeWidth = strokeWidthText.val();
  if (!isNumber(strokeWidth))
  {
	strokeWidthText.val(12);
  }
  else if (strokeWidth < minStrokeWidth)
  {
	strokeWidthText.val(minStrokeWidth);
  }

  onStrokeWidthInput();
}

$('#strokeWidthTextbox').on('input', onStrokeWidthInput);
$('#strokeWidthTextbox').on('change', onStrokeWidthChange);



////////////////////////////////////////////////////////////////////////////////
// Not Working ATM... don't know why..      Look at it Dalton                 //
////////////////////////////////////////////////////////////////////////////////

function onClipartScaleTextInput() {
  var clipartScale = $('#clipartScaleTextbox').val();
  if (isNumber(clipartScale) && minClipartScale <= clipartScale && clipartScale <= maxClipartScale)
  {
    //$('#clipartScaleSlider').val(clipartScale);


    var currentObj = canvas.getActiveObject();
    if (!isClipart(currentObj))
      return;

    currentObj.scale(currentObj.scaleXStart*clipartScale, currentObj.scaleYStart*clipartScale);
    canvas.renderAll();
  }
}


function onClipartScaleTextChange()
{
  var clipartScaleText = $('#clipartScaleTextbox');
  var clipartScale = clipartScaleText.val();
  if (!isNumber(clipartScale))
  {
    clipartScaleTextbox.val(1);
  }
  else if (clipartScale < minClipartScale)
  {
    clipartScaleTextbox.val(minClipartScale);
  }
  else if (clipartScale > maxClipartScale)
  {
    clipartScaleTextbox.val(maxClipartScale);
  }
  onClipartScaleTextInput();
}

$('#clipartScaleTextbox').on('input', onClipartScaleTextInput);
$('#clipartScaleTextbox').on('change', onClipartScaleTextChange);

// function onClipartScaleSliderChange() {
  // var clipartScale = $('#clipartScaleSlider');
  // var scale = clipartScale.val();
  // $('#clipartScaleTextbox').val(scale);

  // var currentObj = canvas.getActiveObject();
  // if (!isClipart(currentObj))
    // return;

  // //currentObj.scale = clipartScale.val();
  // currentObj.scale(currentObj.scaleXStart*scale, currentObj.scaleYStart*scale);
  // canvas.renderAll();
// }

// $('#clipartScaleSlider').on('input', onClipartScaleSliderChange);
// $('#clipartScaleSlider').on('change', onClipartScaleSliderChange);


// function onClipartScaleSliderChange() {
  // var scale = this.value;

  // var currentObj = canvas.getActiveObject();
  // if ( !isClipart(currentObj) )
    // return;

  // currentObj.scale(currentObj.scaleXStart*scale, currentObj.scaleYStart*scale);
  // canvas.renderAll();
// }

// $('#clipartScaleSlider').on('input', onClipartScaleSliderChange);
// $('#clipartScaleSlider').on('change', onClipartScaleSliderChange);

function observeTextProperty(property) {
  document.getElementById(property).onchange = function() {
    var currentObj = canvas.getActiveObject();
    var currentGroup = canvas.getActiveGroup();

    var value = this.value;

    if (property == 'fill' && this.value.charAt(0) != '#')
      value = '#' + value;

    if (isText(currentObj)) {
      currentObj[property] = value;
      currentObj.setCoords();
      canvas.deactivateAll().renderAll();
      canvas.setActiveObject(currentObj);
    }
    else if (isGroup(currentGroup)) {
      var groupArr = currentGroup.getObjects();
      for (var i = 0; i < currentGroup.size(); i++) {
        // if obj not text, skip it
        if (!isText(groupArr[i]))
          continue;
        groupArr[i][property] = value;
      }

      currentGroup.setCoords();
      canvas.deactivateAll().renderAll();
      canvas.setActiveGroup(currentGroup);
    }
    canvas.renderAll();
  };
}

function setTextAlign(textAlign) {
  var currentObj = canvas.getActiveObject();
  var currentGroup = canvas.getActiveGroup();


  if (isText(currentObj)) {
    currentObj.textAlign= textAlign;
  }
  else if (isGroup(currentGroup)) {
    var groupArr = currentGroup.getObjects();
    for (var i = 0; i < currentGroup.size(); i++) {
      groupArr[i].textAlign = textAlign;
    }
  }

  canvas.renderAll();
}

function setTextBold() {
  var object = canvas.getActiveObject();
  var currentWeight = $('#stickerText').css('font-weight');

  if (currentWeight == 400) {
    $('#stickerText').css('font-weight', 'bold');
  } else {
    $('#stickerText').css('font-weight', 400);
  }
  if(isText(object))
  {
    if(object.fontWeight == 400)
    {
      object.fontWeight = 'bold';
    } else {
      object.fontWeight = 400;
    }
  }

  canvas.renderAll();
}

function preloadFonts() {

}

function setTextItalic() {
  var object = canvas.getActiveObject();
  var currentStyle = $('#stickerText').css('font-style');

  if (currentStyle == 'normal') {
    $('#stickerText').css('font-style', 'italic');
  } else {
    $('#stickerText').css('font-style', 'normal');
  }

  if(isText(object))
  {
    if(object.fontStyle == 'normal')
    {
      object.fontStyle = 'italic';
    } else if(object.fontStyle == 'italic')
    {
      object.fontStyle = 'normal';
    }
  }

  canvas.renderAll();
}

function setTextUnderline() {
  var object = canvas.getActiveObject();
  var currentDecoration = $('#stickerText').css('text-decoration');

  if (currentDecoration == 'none') {
    $('#stickerText').css('text-decoration', 'underline');
  } else {
    $('#stickerText').css('text-decoration', 'none');
  }
  if(isText(object))
  {
    if(object.textDecoration == 'none')
    {
      object.textDecoration = 'underline';
    } else if(object.textDecoration == 'underline')
    {
      object.textDecoration = 'none';
    }
  }

  canvas.renderAll();
}


function moveUp() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.bringForward(currentObj);
  //orderLayers();
}

function moveDown() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.sendBackwards(currentObj);
  //orderLayers();
}

function bringToFront() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.bringToFront(currentObj);
  //orderLayers();
}

function sendToBack() {
  var currentObj = canvas.getActiveObject();
  if (currentObj == null)
    return;

  canvas.sendToBack(currentObj);
  //orderLayers();
}


observeTextProperty('fontFamily');
observeTextProperty('fill');
observeTextProperty('strokeWidthTextbox');
observeTextProperty('stroke');

//function orderLayers()
//{
  //canvas.sendToBack(currentOutline);
  //canvas.bringToFront(outlinePath);
  //canvas.renderAll();
//}