/////////////////////////////////////////////////////////
//        tab   2 - background color
/////////////////////////////////////////////////////////

  var backImgInfo = {
  src: '',
  beingUsed: false,
  isLocal: true,
  width: 0,
  height: 0
 }

function setBackgroundColor(bgColor) {

  backImgInfo.beingUsed = false;
  outlineOptions.fill = bgColor;
  updateOutlinePath();
  setBorderColor();
  canvas.renderAll();
}

function setBackgroundColorInput() {

  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS
  var bgColor = document.getElementById('bgColorInput').value;
  if (bgColor[0] != '#')
    bgColor = '#' + bgColor;
  var isOk  = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(bgColor);
  if (!isOk)
    return;
  // VALIDATE TO AVOID BLACK WHILE CHANGING COLORS


  var styleString = "background:" + bgColor;
  var customColor = document.getElementById('customBackgroundColor');
  customColor.style.cssText = 'background:'+ bgColor;
  setBackgroundColor(bgColor);
  canvas.renderAll();
}


 function addBackground(name, backOptions, loadFromURL)
{

  var imgElement;
	if (loadFromURL){
		// simon fix later - mmdev
		var newName = server_path + name;
		imgElement = new Image();
		imgElement.src = newName;

		backImgInfo.isLocal = false;
	}
  else {
	  if (name.length > 10 && name.substring(0,10) == 'data:image')
	  {
		imgElement = new Image();
		imgElement.src = name;
		backImgInfo.isLocal = true;
	  }
	  else
	  {
		imgElement = document.getElementsByName(name)[0];
		backImgInfo.isLocal = false;
	  }
  }

  addBackgroundFromImgElement(imgElement);
  canvas.renderAll();
 }
 function addBackgroundFromImgElement(imgElement)
 {
  backImgInfo.src = imgElement.src;
  backImgInfo.beingUsed = true;

  var currentImg = new fabric.Image(imgElement, {
    left: currentOutline.left,
    top: currentOutline.top,
    originX: 'center',
    originY: 'center'
  });

  var width, height;
  if (currentOutline == rectOutlineInfo)
  {
    width = currentOutline.width;
    height = currentOutline.height;
  }
  else// if (currentOutline == ellipseOutlineInfo)
  {
    width = 2*currentOutline.rx;
    height = 2*currentOutline.ry;
  }

  currentImg.width = width + (1 * mm2px);
  currentImg.height = height + (1 * mm2px);
  currentImg.centeredScaling = true;

  backImgInfo.left = currentImg.left;
  backImgInfo.top = currentImg.top;
  backImgInfo.width = currentImg.width;
  backImgInfo.height = currentImg.height;

  canvas.setBackgroundImage(currentImg);
  //orderLayers();
  canvas.renderAll();
}
