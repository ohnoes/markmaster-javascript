/////////////////////////////////////////////////////////
//        Setup canvas stuff
/////////////////////////////////////////////////////////
var stickerType = document.getElementById('stickertype').value;

var minStrokeWidth = 0.01;


var minFontSize = 1.5;
var maxFontSize = 140;

var minLineHeight = 0;
var maxLineHeight = 2;

var minClipartScale = 0.0001;
var maxClipartScale = 6;


// added
// conversion ratio from pt. for font size to mm
// 1 pt = 1/72 inch.  1/72 inch = ~0.352mm
// exact after 3rd decimal apparently varies from program to program http://en.wikipedia.org/wiki/Point_(typography)
var pt2mm = 0.352;
var mm2px = 1;

var canvas = this.__canvas = new fabric.Canvas('myCanvas', {width: $('#myCanvas').prop('width'), height: $('#myCanvas').prop('height')});
setInterval(function(){canvas.calcOffset();}, 5000);
var currentOutline;
var outlinePath;
// outlinePath style and other options




if (stickerType == 6 || stickerType == 7) {
    var outlinePathOptions = {
      visible: true,
      selectable: false,
      fill: "#BDBDBD",
      originX: 'center',
      originY: 'center',
      opacity: 0.8,
    };
} else {
    var outlinePathOptions = {
      visible: true,
      selectable: false,
      fill: "#BDBDBD",
      originX: 'center',
      originY: 'center',
      opacity: 0.8,
    };
}
var outlinePathWhiteOptions = {
  visible: true,
  selectable: false,
  fill: "#FFFFFF",
  originX: 'center',
  originY: 'center',
  opacity: 1,

};


var rectOutlineInfo = new fabric.Rect({
  left: 150,
  top: 120,
  originX: 'center',
  originY: 'center',
  width: 150,
  height: 120,
  angle: 0,
  fill: 'rgba(255,255,255,1)',
  transparentCorners: false,
  selectable: false
});


var outlineOptions = {
  visible: true,
  selectable: false,
  fill: "#FFFFFF",
  originX: 'center',
  originY: 'center',
  opacity: 1,
  stroke: true,
  strokeStyle: 'black'
}

// set all variables for a rectangle path

var ellipseOutlineInfo = new fabric.Ellipse({
  left: 0,
  top: 0,
  originX: 'center',
  originY: 'center',
  rx: 150,
  ry: 120,
  fill: 'rgba(255,255,255,1)',
  transparentCorners: false,
  selectable: false
});

currentOutline = rectOutlineInfo;

/////////////////////////////////////////////////////////
//        tab   1 - size and shape
/////////////////////////////////////////////////////////

var minWidth = 6;
var maxWidth = 300;

var minHeight = 6;
var maxHeight = 300;
var outlinePadding = 40; // how much to pad largest dimension: width/height

if (stickerType == 9) {
  maxHeight = 50;
  maxWidth = 50;
}

// clamps number between min/maxWidth
// takes number, checks that it is within min/max width range
// if it is, return the number
// if not, return closest boundary

function validateWidth(numValue) {
  if (numValue == '')
    return minWidth;
  numValue = parseInt(numValue);
  if (numValue < minWidth)
    return minWidth;
  if (numValue > maxWidth)
    return maxWidth;
  return numValue;
}

// gets width and height from textbox, then resizes all outlines with padding using width/height ratio
function resizeOutline() {
  // get width and height from textbox
  var width = validateWidth(document.getElementById('width').value);
  document.getElementById('width').value = width;
  var height = validateWidth(document.getElementById('height').value);
  document.getElementById('height').value = height;


  var ratio = width / height;

  // canvas width, height, center
  var cWidth = canvas.getWidth();
  var cHeight = canvas.getHeight();
  var centerX = cWidth / 2;
  var centerY = cHeight / 2;

  // aspect width/height
  var aWidth;
  var aHeight;

  // pads dimension with higher value, then calculates other
  // ex: if width is more than height, then pad width and calculate height, else vice versa
  var cornerRadius = 2;
  if (width > height)
  {
    aWidth = cWidth - 2*outlinePadding;
    aHeight = aWidth / ratio;
    mm2px = aWidth / width;
  }
  else
  {
    aHeight = cHeight - 2*outlinePadding;
    aWidth = ratio * aHeight;
    mm2px = aHeight/height;
  }

  cornerRadius *= mm2px;

  // set width and height of outlines and center them
  rectOutlineInfo.width = aWidth;
  rectOutlineInfo.height = aHeight;
  rectOutlineInfo.left = centerX;
  rectOutlineInfo.top = centerY;
  rectOutlineInfo.rx = cornerRadius;
  rectOutlineInfo.ry = cornerRadius;



  ellipseOutlineInfo.rx = aWidth/2;
  ellipseOutlineInfo.ry = aHeight/2;
  ellipseOutlineInfo.left = centerX;
  ellipseOutlineInfo.top = centerY;



  //orderLayers();

  //canvas.setBackgroundImage(currentOutline); // added
  updateTextSize();
  canvas.renderAll();
}


// creates and returns non-sticker area based on rect outline
// gray area
function getRectClipPath()
{
  var rWidth = rectOutlineInfo.width;
  var rHeight = rectOutlineInfo.height;

  // setBarWidth(rWidth, $('#width').val());
  // setBarHeight(rHeight, $('#height').val());


  var width = canvas.getWidth();
  var height = canvas.getHeight();

  var cRadius = rectOutlineInfo.rx;

  var top = 0;
  var left = 0;
  var bottom = top+height;
  var right = left+width;

  var centerX = (left+right)/2;
  var centerY = (top+bottom)/2;

  var rTop = centerY -rHeight/2;
  var rLeft = centerX - rWidth/2;
  var rBottom = centerY +rHeight/2;
  var rRight = centerX + rWidth/2;

  var cTop = rTop + cRadius;
  var cLeft = rLeft + cRadius;
  var cBottom = rBottom - cRadius;
  var cRight = rRight - cRadius;

  var ePath, rectOutline;
  if (stickerType == 7) {
	 	var borderWidth = mm2px * 1.5;
		cTop = rTop + borderWidth;
		cLeft = rLeft + borderWidth;
		cBottom = rBottom - borderWidth;
		cRight = rRight - borderWidth;
		//in rectCutoff change cLeft, cRight, cTop, and cBottom
		canvas.setBackgroundImage(rectOutline);
		var rectCutoff = new fabric.Path(
		'M' + rLeft    + ' ' + rTop     +
		'L' + centerX + ' ' + rTop     +
		'L' + centerX + ' ' + cTop    +
		'L' + cLeft   + ' ' + cTop    +
		'L' + cLeft   + ' ' + cBottom +
		'L' + cRight  + ' ' + cBottom +
		'L' + cRight  + ' ' + cTop    +
		'L' + centerX + ' ' + cTop    +
		'L' + centerX + ' ' + rTop     +
		'L' + rRight   + ' ' + rTop     +
		'L' + rRight   + ' ' + rBottom  +
		'L' + rLeft + ' ' + rBottom  +
		'Z', outlinePathWhiteOptions);
		rectCutoff.setFill('#FFFFFF');

		ePath = new fabric.Path(
		'M' + left    + ' ' + top     +
		'L' + centerX + ' ' + top     +
		'L' + centerX + ' ' + rTop    +
		'L' + rLeft   + ' ' + rTop    +
		'L' + rLeft   + ' ' + rBottom +
		'L' + rRight  + ' ' + rBottom +
		'L' + rRight  + ' ' + rTop    +
		'L' + centerX + ' ' + rTop    +
		'L' + centerX + ' ' + top     +
		'L' + right   + ' ' + top     +
		'L' + right   + ' ' + bottom  +
		'L' + left + ' ' + bottom  +
		'Z', outlinePathOptions);


	  rectOutline = new fabric.Path(
		'M' + centerX + ' ' + rTop    +
		'L' + rLeft   + ' ' + rTop    +
		'L' + rLeft   + ' ' + rBottom +
		'L' + rRight  + ' ' + rBottom +
		'L' + rRight  + ' ' + rTop    +
		'L' + centerX + ' ' + rTop    +
		'Z', outlineOptions);


		var group = new fabric.Group([ rectCutoff, ePath ], { left: 0, top: 0 });
		return group;
  }
  else {

		if(stickerType == 6) {
			var borderWidth = mm2px * 1.5;
			cTop = rTop + borderWidth;
			cLeft = rLeft + borderWidth;
			cBottom = rBottom - borderWidth;
			cRight = rRight - borderWidth;
			//in rectCutoff change cLeft, cRight, cTop, and cBottom
			canvas.setBackgroundImage(rectOutline);
			var rectCutoff = new fabric.Path(
			'M' + rLeft    + ' ' + rTop     +
			'L' + centerX + ' ' + rTop     +
			'L' + centerX + ' ' + cTop    +
			'L' + cLeft   + ' ' + cTop    +
			'L' + cLeft   + ' ' + cBottom +
			'L' + cRight  + ' ' + cBottom +
			'L' + cRight  + ' ' + cTop    +
			'L' + centerX + ' ' + cTop    +
			'L' + centerX + ' ' + rTop     +
			'L' + rRight   + ' ' + rTop     +
			'L' + rRight   + ' ' + rBottom  +
			'L' + rLeft + ' ' + rBottom  +
			'Z', outlinePathWhiteOptions);
			rectCutoff.setFill('#FFFFFF');

		}

	  ePath = new fabric.Path(
		'M' + left    + ' ' + top     +
		'L' + centerX + ' ' + top     +
		'L' + centerX + ' ' + rTop    +
		'L' + cLeft   + ' ' + rTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rLeft   + ' ' + cTop +
		'L' + rLeft   + ' ' + cBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cLeft   + ' ' + rBottom +
		'L' + cRight  + ' ' + rBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rRight   + ' ' + cBottom +
		'L' + rRight  + ' ' + cTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cRight   + ' ' + rTop +
		'L' + centerX + ' ' + rTop    +
		'L' + centerX + ' ' + top     +
		'L' + right   + ' ' + top     +
		'L' + right   + ' ' + bottom  +
		'L' + left + ' ' + bottom  +
		'Z', outlinePathOptions);


	  rectOutline = new fabric.Path(
		'M' + centerX + ' ' + rTop    +
		'L' + cLeft   + ' ' + rTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rLeft   + ' ' + cTop +
		'L' + rLeft   + ' ' + cBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cLeft   + ' ' + rBottom +
		'L' + cRight  + ' ' + rBottom +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + rRight   + ' ' + cBottom +
		'L' + rRight  + ' ' + cTop    +
		'A' + cRadius + ' ' + cRadius + ' 0, 0 0,' + cRight   + ' ' + rTop +
		'L' + centerX + ' ' + rTop    +
		'Z', outlineOptions);
  }
	canvas.setBackgroundImage(rectOutline);

	if(stickerType == 6) {
		var group = new fabric.Group([ rectCutoff, ePath ], { left: 0, top: 0 });
		return group;
	}

  return ePath;
}

// creates and returns non-sticker area based on ellipse outline
// gray outline
function getEllipseClipPath()
{
  var rx = ellipseOutlineInfo.rx;
  var ry = ellipseOutlineInfo.ry;

  // setBarWidth(rx*2, $('#width').val());
  // setBarHeight(ry*2, $('#height').val());

  var width = canvas.getWidth();
  var height = canvas.getHeight();

  var top = 0;
  var left = 0;
  var bottom = top+height;
  var right = left+width;

  var centerX = (left+right)/2;
  var centerY = (top+bottom)/2;

  var eTop = centerY - ry;
  var eLeft = centerX - rx;
  var eBottom = centerY + ry;
  var eRight = centerX + rx;

  var ePath = new fabric.Path(
    'M' + left    + ' ' + top     +
    'L' + centerX + ' ' + '0'     +
    'L' + centerX + ' ' + eTop    +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eLeft   + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eBottom +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eRight  + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eTop +
    'L' + centerX + ' ' + '0'     +
    'L' + right   + ' ' + '0'     +
    'L' + right   + ' ' + bottom  +
    'L' + left + ' ' + bottom  +
    'Z', outlinePathOptions);

  var ellipseOutline = new fabric.Path(
    'M' + centerX + ' ' + eTop    +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eLeft   + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eBottom +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + eRight  + ' ' + centerY +
    'A' + rx      + ' ' + ry      + ' 0, 0 0,' + centerX + ' ' + eTop +
    'Z', outlineOptions);
  canvas.setBackgroundImage(ellipseOutline);

  return ePath;
}

// updates greyed-out non-sticker area to match outline
function updateOutlinePath() {

    if (currentOutline == rectOutlineInfo)
    {
      outlinePath = getRectClipPath();
    }
    else// if (currentOutline == ellipseOutlineInfo)
    {
      outlinePath = getEllipseClipPath();
    }

    var overlay = outlinePath.cloneAsImage();
    canvas.setOverlayImage(overlay);
    canvas.renderAll();
}

//
function observeShape(property) {
  document.getElementById(property).onclick = function() {
    if (property == 'isRect')
    {
      currentOutline = rectOutlineInfo;
    }
    else if (property == 'isEllipse')
    {
      currentOutline = ellipseOutlineInfo;
    }

    resizeOutline();
    updateOutlinePath();
    canvas.renderAll();
  };
}


function observeNumeric(property) {
  document.getElementById(property).onchange = function() {
    resizeOutline();
    updateOutlinePath();
    canvas.renderAll();
    };
    document.getElementById(property).onkeypress = function() {
      x = event.which;
      if (x == 13)
      {
        document.getElementById(property).onchange();
      }
    }

    updateOutlinePath();
  }

function observeSize(property) {
  document.getElementById(property).onclick = function() {
    if (property == '3716')
    {
      document.getElementById('width').value = 37;
	  document.getElementById('height').value = 16;
    }
    else if (property == '6026')
    {
      document.getElementById('width').value = 60;
	  document.getElementById('height').value = 26;
    } else if (property == '3010')
	{
	  document.getElementById('width').value = 30;
	  document.getElementById('height').value = 10;
	}

	setPrice();
    resizeOutline();
    updateOutlinePath();
    canvas.renderAll();
  };
}

observeShape('isRect');
observeShape('isEllipse');

observeNumeric('width');
observeNumeric('height');

observeSize('3010');
observeSize('3716');
observeSize('6026');
