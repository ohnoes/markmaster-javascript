// Add simple text (check how much text is input, and if there's a clipart)
function addSimpleText() {
    // Clear all text objects before adding new, also check for clipart
    var clipartAdded = false;
    canvas.getObjects();
    for(var i = canvas.getObjects().length-1; i >= 0; i--){
      if(canvas.item(i).isType('i-text')) {
        canvas.remove(canvas.item(i));
      } else if (canvas.item(i).isType('image') || canvas.item(i)['isSVG'] == true) {
        clipartAdded = true;
      }
    }

    // get the number of lines being added (objects)
    var textOne = document.getElementById('text1').value;
    var textTwo = document.getElementById('text2').value;
    var textThree = document.getElementById('text3').value;

    var firstFont = $('#fontSelect').css('font-family');
    var testFont = firstFont.split(", ");
    var theFont = testFont[1];

    var numLines = 0;
    if(textOne.length > 0) {
        numLines++;
    }
    if(textTwo.length > 0) {
        numLines++;
    }
    if(textThree.length > 0) {
        numLines++;
    }

    // We got clipart, and need to make the text smaller to make room for the clipart
    if(clipartAdded) {
      var leftPos = 380;
      var maxWidth = 320;
      if($('#stickertype').val() == 7) {
          leftPos = 375;
      }

    // There is no clipart, make text take majority of the space
    } else {
      var leftPos = 300;
      var maxWidth = 400;
    }
    var fontSize = 20;
    var firstText, secondText;
    if(numLines == 1) {
        var oneScale = 8;
        if($('#stickertype').val() == 7) {
            oneScale = 7.5;
        }

      if(textOne.length > 0) {
        firstText = textOne;
      } else if (textTwo.length > 0) {
        firstText = textTwo;
      } else {
        firstText = textThree;
      }
        var theFill = document.getElementById('fill').value;
        var currentText = new fabric.IText(firstText,
          { left: leftPos,
            top: 315,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: oneScale,
            scaleY: oneScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);
        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        var moveTOP = 1;
        console.log("Object width: " + objectWidth);
        console.log("Object scale: " + objectScale);
        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*oneScale;
            currentText.scaleY = (maxWidth/objectWidth)*oneScale;
            moveTOP = 1 - (currentText.scaleX / oneScale);
            objectScale = currentText.scaleX;
            currentText.top = 315 - (moveTOP * 10 );
        }
        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);
    } else if (numLines == 2) {
        var textScale = 5;
        var firstTextTop = 270;
        var secondTextTop = 360;
        if($('#stickertype').val() == 7) {
            textScale = 3.5;
            firstTextTop = 273;
            secondTextTop = 340;
        }

      if(textOne.length > 0) {
        firstText = textOne;
        if(textTwo.length > 0) {
          secondText = textTwo;
        } else {
          secondText = textThree;
        }
      } else {
        firstText = textTwo;
        secondText = textThree;
      }
        var currentText = new fabric.IText(firstText,
          { left: leftPos,
            top: firstTextTop,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: textScale,
            scaleY: textScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);
        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*textScale;
            currentText.scaleY = (maxWidth/objectWidth)*textScale;
            objectScale = currentText.scaleX;
            var firstScale = objectScale;
        }
        console.log("Object width: " + objectWidth);
        console.log("Object scale: " + objectScale);
        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);

        var currentText = new fabric.IText(secondText,
          { left: leftPos,
            top: secondTextTop,
            originX: 'center',
            originY: 'center',
            lockScalingX: true,
            lockScalingY: true,
            textAlign: 'center',
            fontSize: 20,
            scaleX: textScale,
            scaleY: textScale,
            selectable: false,
            fontFamily: theFont,
            type: 'i-text',
            strokeWidth: 0.01
          });

        // Use width and x scale to make sure it fits (if the width extends the size)
        //canvas.setActiveObject(currentText);

        var objectScale = currentText.scaleX;
        var objectWidth = currentText.width*objectScale;
        var moveTOP = 1;

        if (objectWidth > maxWidth) {
            currentText.scaleX = (maxWidth/objectWidth)*textScale;
            currentText.scaleY = (maxWidth/objectWidth)*textScale;
            moveTOP = 1 - (currentText.scaleX / textScale);
            objectScale = currentText.scaleX;
            currentText.top = secondTextTop - (moveTOP * 10);
        }
        if (firstScale < objectScale) {
            console.log("WENEEDCHANGE")
            currentText.scaleX = firstScale;
            currentText.scaleY = firstScale;
            moveTOP = 1 - (currentText.scaleX / textScale);
            objectScale = currentText.scaleX;
            currentText.top = secondTextTop - (moveTOP * 10);
        }
        console.log("Object width: " + objectWidth);
        console.log("Object scale: " + objectScale);
        currentText.fill = document.getElementById('fill').value;
        canvas.add(currentText);

    } else if (numLines == 3) {
      var firstTextLine = new fabric.IText(textOne,
        { left: leftPos,
          top: 240,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          textAlign: 'center',
          fontSize: 20,
          scaleX: 3,
          scaleY: 3,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });

      // Use width and x scale to make sure it fits (if the width extends the size)
      //canvas.setActiveObject(currentText);
      var firstWidth = firstTextLine.width;
      firstTextLine.fill = document.getElementById('fill').value;

      var secondTextLine = new fabric.IText(textTwo,
        { left: leftPos,
          top: 310,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          textAlign: 'center',
          fontSize: 20,
          scaleX: 3,
          scaleY: 3,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });
      //canvas.setActiveObject(currentText);
      var secondWidth = secondTextLine.width;
      secondTextLine.fill = document.getElementById('fill').value;

      var thirdTextLine = new fabric.IText(textThree,
        { left: leftPos,
          top: 380,
          originX: 'center',
          originY: 'center',
          lockScalingX: true,
          lockScalingY: true,
          scaleX: 3,
          scaleY: 3,
          textAlign: 'center',
          fontSize: 20,
          selectable: false,
          fontFamily: theFont,
          type: 'i-text',
          strokeWidth: 0.01
        });

      // Use width and x scale to make sure it fits (if the width extends the size)
      //canvas.setActiveObject(currentText);
      var thirdWidth = thirdTextLine.width;
      thirdTextLine.fill = document.getElementById('fill').value;


    var largestWidth = firstWidth;
    // find the greatest width text of the three
    if(firstWidth >= secondWidth) {
        if(firstWidth >= thirdWidth) {
            largestWidth = firstWidth;
        } else {
            largestWidth = thirdWidth;
        }
    } else {
        if (secondWidth >= thirdWidth) {
            largestWidth = secondWidth;
        } else {
            largestWidth = thirdWidth;
        }
    }

    if ((largestWidth*3) > maxWidth) {
        firstTextLine.scaleX    = (maxWidth/largestWidth);
        firstTextLine.scaleY    = (maxWidth/largestWidth);
        secondTextLine.scaleX   = (maxWidth/largestWidth);
        secondTextLine.scaleY   = (maxWidth/largestWidth);
        thirdTextLine.scaleX    = (maxWidth/largestWidth);
        thirdTextLine.scaleY    = (maxWidth/largestWidth);
    }

    canvas.add(firstTextLine);
    canvas.add(secondTextLine);
    canvas.add(thirdTextLine);
    // we have width, height, left and top
    // Center: left 150, top 55
}

}

function orderSticker(){
    var value = $('#stickertype').val();
    rdycheckout(value);
    document.getElementById("cartForm").submit();
}

function resizeTopMargin()
{
    //get the canvas div
    if($(window).width() < 768)
        $('#textBox').css({'padding-top':$("#canvasDiv").height() + 10});
    else {
        $('#textBox').css({'padding-top': 10});
    }
}

function clearTheSticker(){
    canvas.clear();
}
